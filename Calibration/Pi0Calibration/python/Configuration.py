#!/usr/bin/env python
# =============================================================================
# $Id$
# =============================================================================
# @file  Pi0Calibration/Configuration.py
#
# The basic configuration for Calorimeter Calibration
# The application produces ROOT NTuple for analysis
#
# @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
# @date   2009-09-28
#
#                   $Revision$
# Last modification $Date$
#                by $Author$
#
# =============================================================================
"""
The basic configuration to (re)run Ecal pi0-calibration
The application produces ROOT NTuple for analysis
"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@nikhef.nl "
__date__    = " 2009-09-28 "
__version__ = " version $Revision$ "
# =============================================================================
# the only one  "vizible" symbol 
__all__  = (
    'KaliPi0Conf' ,                         ## the only one vizible symbol
    )
# =============================================================================
from Gaudi.Configuration       import *
from LHCbKernel.Configuration  import *
from GaudiKernel.SystemOfUnits import GeV,MeV 
import logging

_log = logging.getLogger('KaliCalo')

# =============================================================================
## @class KaliPi0Conf
#  Configurable for Calorimeter Iterative Calibration with pi0
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-09-28
class  KaliPi0Conf(LHCbConfigurableUser):
    """
    The main Configurable for Calorimeter Iterative Calibration with pi0

    The application produces:
    
    - ROOT NTuple for analysis
    
    """
    
    ## Explicitly Used Configurables
    __used_configurables__ = [
        'OffLineCaloRecoConf' ,
        'OffLineCaloPIDsConf' ,
        'GlobalRecoConf',
        ('DaVinci', None)
        ]

    ## the own slots 
    __slots__ = {
        ## Own flags:
        'FirstPass'             : False ## The first (specific) pass on (x)DST ?
        , 'DestroyTES'          : True  ## Destroy TES containers : List of Input Partcle Containers
        , 'DestroyList'         : ['KaliPi0' ]  ## The list of input TES-location for Destroyer
        , 'OtherAlgs'           : []    ## List of "other" algorithms to be run, e.g. electorn calibration
        , 'Mirror'              : False ## Use Albert's trick for combinatorial background evaluation
        , 'Histograms'          : False ## Create monitoring histograms
        , 'RecoAll'             : False ## Global Recontruction ?
        ## for first pass only
        , 'Pi0VetoDeltaMass'    : -1    ## mass-window for pi0-veto 
        , 'Pi0VetoChi2'         : -1    ## chi2        for pi0-veto
        , 'Filter'              : ''    ## event filter
        ## mis/re-calibration
        , 'KaliDB'              : {}    ## the map of { 'dbase' : 'bbase_name' , 'ecal' : 'key for Ecal' , 'prs' : 'key for Prs'}
        , 'Coefficients'        : {}    ## The map of (mis)calibration coefficients
        , 'PrsCoefficients'     : {}    ## The map of (mis)calibration coefficients for Prs 
        ## ``Physics''
        , 'PtGamma'             : 300 * MeV ## Pt-cut for photons 
        , 'Pi0Cut'              : " PT > 200 * MeV * ( 7 - ETA ) " ## Cut for pi0  
        , 'SpdCut'              : 0.1 * MeV ## Spd-cuts for photons 
        ## CaloReco Flags:
        , 'UseTracks'           : True  ## Use Tracks for the first pass ?
        , 'UseSpd'              : True  ## Use Spd as neutrality criteria ?
        , 'UsePrs'              : False ## Use Prs for photon selection ?
        , 'ForceDigits'         : False ## Force Digits witgh Cluster Recontruction
        ## IO-related 
        , 'NTuple'              : 'KaliPi0_Tuples.root' ## The output NTuple-file
        , 'Histos'              : 'KaliPi0_Histos.root' ## The output Histo-file
        , 'FemtoDST'            : 'KaliPi0.fmDST'       ## The output femto-DST
        ## forwarded to DaVinci & other configurables 
        , 'EvtMax'              :  -1    ## Number of events to run (DaVinci)             
        , 'DataType'            : '2009' ## Data type               (DaVinci)
        , 'Simulation'          : False  ## Simulation              (DaVinci) 
        , 'MeasureTime'         : True   ## Measure the time for sequencers
        , 'OutputLevel'         : INFO   ## The global output level
        , 'PrintFreq'           : 100000 ## The print frequency
        , 'NTupleProduce'       : True   ## Produce NTuples
        }
    ## documentation lines 
    _propertyDocDct = {
        ## Own flags 
        'FirstPass'             : """ The first (specific) pass on (x)DST ?"""
        , 'DestroyTES'          : """ Destroy TES containers """
        , 'DestroyList'         : """ The list of input TES-locations for Destroyer """
        , 'OtherAlgs'           : """ The list of 'other' algorithm to run, e.g. electron calibration """
        , 'Mirror'              : """ Use Albert's trick for combinatorial background evaluation """ 
        , 'Histograms'          : """ Activate monitoring histograms creation """
        , 'RecoAll'             : """ Global Reconstruction? """
        ## the first pass only 
        , 'Pi0VetoDeltaMass'    : """ Mass-window for pi0-veto """
        , 'Pi0VetoChi2'         : """ Chi2        for pi0-veto """ 
        , 'Filter'              : """ Void-filter to be used   """ 
        ## mis/re-calibration        
        , 'KaliDB'              : """ The map of { 'name' : 'bbase_name' , 'ecal' : 'key for Ecal' , 'prs' : 'key for Prs'} """
        , 'Coefficients'        : """ The map of (mis)calibration coefficients """
        , 'PrsCoefficients'     : """ The map of (mis)calibration coefficients for Prs """
        ## ``Physics''
        , 'PtGamma'             : """ Pt-cut for photons """
        , 'Pi0Cut'              : """ Cut for pi0 (LoKi/Bender expression)""" 
        , 'SpdCut'              : """ Spd-cuts for photons """ 
        ## CaloReco flags 
        , 'UseTracks'           : """ Use Tracks for the first pass ? """
        , 'UseSpd'              : """ Use Spd as neutrality criteria ? """
        , 'UsePrs'              : """ Use Prs for photon selection ? """
        , 'ForceDigits'         : """ Force Digits witgh Cluster Recontruction """
        ## IO-related 
        , 'NTuple'              : """ The output NTuple-file """ 
        , 'Histos'              : """ The output Histo-file """
        , 'FemtoDST'            : """ The output femto-DST """
        ## DaVinci & Co configuration: 
        , 'EvtMax'              : """ Number of events to run (DaVinci) """
        , 'DataType'            : """ Data type               (DaVinci) """
        , 'Simulation'          : """ Simulation              (DaVinci) """
        , 'MeasureTime'         : """ Measure the time for sequencers """
        , 'OutputLevel'         : """ The global output level """
        , 'NTupleProduce'       : """ Produce calibration NTuples """
        }
 
        

    ## 5. The configuration for Kali-Pi0 algorithm
    def kaliPi0 ( self, photon ) :
        """
        The configuration for Kali-Pi0 algorithm
        """
        from Configurables import Kali__Pi0
        kali = Kali__Pi0 (
            "KaliPi0"                                       ,
            ## specific cuts :
            Cuts = { 'PtGamma' : self.getProp ( 'PtGamma' ) ,
                     'SpdCut'  : self.getProp ( 'SpdCut'  ) } ,
            ## cut for pi0 :
            Pi0Cut         = self.getProp ( 'Pi0Cut' )      ,
            ## general configuration 
            NTupleLUN      = "KALIPI0"                      ,
            HistoPrint     = True                           ,
            NTuplePrint    = True                           ,
            Inputs         = [ 'Phys/%s/Particles' % photon.name() ]  ,
            OutputLevel    = self.getProp ( 'OutputLevel' ) ,
            Mirror         = self.getProp ( 'Mirror'      ) , 
            HistoProduce   = self.getProp ( 'Histograms'  ) ,
            NTupleProduce  = self.getProp ( 'NTupleProduce' )
            )
        
        if self.getProp ('Mirror' ) :
            _log.warning ("KaliPi0: Albert's trick is   activated") 
        else :
            _log.warning ("KaliPi0: Albert's trick is deactivated")
            
        if self.getProp('FemtoDST'):
            _log.warning ("KaliPi0: FemtoDSTs will be produced")
        else:
            _log.warning ("KaliPi0: NO FemtoDSTs will be produced!!!")

        if self.getProp ( 'Histograms' ) :
            _log.warning ( "KaliPi0: Monitoring histograms are   activated") 
        else :
            _log.warning ( "KaliPi0: Monitoring histograms are deactivated")

        if self.getProp ( 'NTupleProduce' ) :
            _log.warning ( "KaliPi0: Creation of calibration NTuples is   activated")
        else :
            _log.warning ( "KaliPi0: Creation of calibration NTuples is deactivated")
            
        if self.getProp ( 'FirstPass' ) :
            
            if 0 <= self.getProp ('Pi0VetoDeltaMass') :
                _dm   = self.getProp ( 'Pi0VetoDeltaMass' ) 
                _log.warning ("KaliPi0: Pi0Veto is activated DM   =%s" % _dm   ) 
                kali.Pi0VetoDeltaMass = _dm
                
            if 0 <= self.getProp ('Pi0VetoChi2') :
                _chi2 = self.getProp ( 'Pi0VeloChi2' ) 
                _log.warning ("KaliPi0: Pi0Veto is activated CHI2 =%s" % _chi2 ) 
                kali.Pi0VetoChi2      = _chi2
                
        kali.InputPrimaryVertices = 'None'  ## NB: it saves a lot of CPU time!
        _log.warning("KaliPi0: Primary Vertices are disabled for Kali") 
        
        return kali 

                    

# import atexit
# atexit.register ( _KaliAtExit_ )

# =============================================================================
if '__main__' == __name__ :
    
    print '*'*120
    print                      __doc__
    print ' Author  : %s ' %   __author__    
    print ' Version : %s ' %   __version__
    print ' Date    : %s ' %   __date__
    print '*'*120  
    
# =============================================================================
# The END 
# =============================================================================
