################################################################################
# Package: Pi0Calibration
################################################################################
gaudi_subdir(Pi0Calibration v1r0)

gaudi_depends_on_subdirs(GaudiAlg
                         GaudiKernel
			 GaudiPython
                         Kernel/LHCbKernel
                         Kernel/LHCbMath
                         Det/CaloDet
                         Phys/LoKiAlgo
                         Phys/LoKiArrayFunctors
                         Phys/LoKiPhys
                         Calo/CaloUtils)

find_package(PythonLibs)
find_package(Boost COMPONENTS program_options REQUIRED)
find_package(ROOT COMPONENTS Core RIO Hist Graf Graf3d Postscript Gpad RooFitCore RooFit Tree Spectrum)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(Pi0Calibration
                  src/*.cpp
		  INCLUDE_DIRS Boost ROOT
		  INCLUDE_DIRS Phys/LoKi Phys/LoKiAlgo Phys/LoKiArrayFunctors
		  LINK_LIBRARIES Boost ROOT GaudiAlgLib CaloUtils CaloDetLib LHCbMathLib LoKiAlgo LoKiArrayFunctorsLib LoKiPhysLib)

gaudi_add_library(Pi0CalibrationLib                   
	           src/*.cpp                   
		   PUBLIC_HEADERS Pi0Calibration                   
		   INCLUDE_DIRS Boost ROOT        
		   INCLUDE_DIRS Phys/LoKi Phys/LoKiAlgo Phys/LoKiArrayFunctors
		  LINK_LIBRARIES Boost ROOT GaudiAlgLib CaloUtils CaloDetLib LHCbMathLib LoKiAlgo LoKiArrayFunctorsLib LoKiPhysLib)

gaudi_add_dictionary(Pi0Calibration
                     dict/Pi0CalibrationDict.h
                     dict/Pi0CalibrationDict.xml
                     INCLUDE_DIRS Boost ROOT
		     INCLUDE_DIRS Phys/LoKi Phys/LoKiAlgo Phys/LoKiArrayFunctors
		     LINK_LIBRARIES Boost ROOT GaudiAlgLib CaloUtils CaloDetLib LHCbMathLib Pi0CalibrationLib LoKiAlgo LoKiArrayFunctorsLib LoKiPhysLib
		     OPTIONS "-U__MINGW32__")

gaudi_install_python_modules()
