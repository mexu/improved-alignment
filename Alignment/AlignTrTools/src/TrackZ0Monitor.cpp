#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/ToolHandle.h"
#include "Event/ChiSquare.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "TrackKernel/TrackTraj.h"
#include "Event/Particle.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrackKernel/TrackStateVertex.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include <Math/Boost.h>


namespace {

  // Descends to find all proto tracks making up particle
  void addTracks( const LHCb::Particle& p,
                  std::vector<const LHCb::Track*>& tracks,
                  std::vector<double>& masshypos)
  {
    if( p.proto() && p.proto()->track() ) {
      tracks.push_back(p.proto()->track() );
      masshypos.push_back(p.momentum().M());
    } else {
      for( const LHCb::Particle* dau: p.daughters() )
        addTracks( *dau, tracks, masshypos );
    }
  }

  Gaudi::LorentzVector lorentzVector( const LHCb::State& state,
				      double mass )
  {
    const double P = 1.0/std::abs(state.qOverP()) ;
    const double Pz = P / std::sqrt(1.0+state.tx()*state.tx()+state.ty()*state.ty()) ;
    const double E = std::sqrt( P*P + mass*mass) ;
    return Gaudi::LorentzVector( state.tx()*Pz, state.ty()*Pz, Pz, E ) ;
  }

  void computeDerivatives( const LHCb::State& state1,
			   const LHCb::State& state2,
			   double mumass,
			   Gaudi::Vector3& J1,
			   Gaudi::Vector3& J2 )
  {
    const double tx_1 = state1.tx() ;
    const double ty_1 = state1.ty() ;
    const double tx_2 = state2.tx() ;
    const double ty_2 = state2.ty() ;
    
    const double n2_1 = 1.0/(1.0 + tx_1*tx_1 + ty_1*ty_1) ;
    const double n2_2 = 1.0/(1.0 + tx_2*tx_2 + ty_2*ty_2) ;
    const double n_1 = std::sqrt(n2_1) ;
    const double n_2 = std::sqrt(n2_2) ;
    const double costheta = n_1 * n_2 * (1.0 + tx_1*tx_2 + ty_1*ty_2) ;

    const double p1 = 1.0/std::abs(state1.qOverP() ) ;
    const double p2 = 1.0/std::abs(state2.qOverP() ) ;
    const double q1 = state1.qOverP() >1 ? +1 : -1 ;
    const double q2 = state2.qOverP() >1 ? +1 : -1 ;
    const double m2 = mumass*mumass ;
    const double E1 = std::sqrt(p1*p1 + m2 ) ;
    const double E2 = std::sqrt(p2*p2 + m2 ) ;
    const double M2 = 2*m2 + 2*(E1*E2 - p1*p2 * costheta ) ;
    const double M = std::sqrt(M2) ;

    // dM/dtx1,ty1,q/p1
    J1(0) = 0.5/M * -2*p1*p2 * ( n_1*n_2*tx_2 - n2_1 * tx_1 * costheta ) ;
    J1(1) = 0.5/M * -2*p1*p2 * ( n_1*n_2*ty_2 - n2_1 * ty_1 * costheta ) ;
    J1(2) = 0.5/M * -p1*p1/q1 * ( 2*E2/E1*p1 - 2*p2*costheta ) ;
    
    J2(0) = 0.5/M * -2*p1*p2 * ( n_1*n_2*tx_1 - n2_2 * tx_2 * costheta ) ;
    J2(1) = 0.5/M * -2*p1*p2 * ( n_1*n_2*ty_1 - n2_2 * ty_2 * costheta ) ;
    J2(2) = 0.5/M * -p2*p2/q2 * ( 2*E1/E2*p2 - 2*p1*costheta ) ;

    // for debugging
    /*
    const double covM1 = ROOT::Math::Similarity(J1,state1.covariance().Sub<Gaudi::SymMatrix3x3>(2,2)) ;
    const double covM2 = ROOT::Math::Similarity(J2,state2.covariance().Sub<Gaudi::SymMatrix3x3>(2,2)) ;
    std::cout << "Mass, masserr: "
              << M << "," << std::sqrt(covM1+covM2) << std::endl ;
    */
  }

  double cosThetaStarCS( const Gaudi::LorentzVector& p4p, const Gaudi::LorentzVector& p4m)
  {
    auto p4sum = p4p + p4m ;
    auto lorentzboost = ROOT::Math::Boost( p4sum.BoostToCM() ) ;
    //auto boostvector = - p4sum.BetaVector()  ;
    auto p4mprime = lorentzboost(p4m) ;
    auto p4mprimedir = p4mprime.Vect().Unit() ;

    const double Ebeam = 6.5e6 ;
    auto protonA = lorentzboost( Gaudi::LorentzVector{0.,0.,Ebeam,Ebeam} ) ;
    auto protonB = lorentzboost( Gaudi::LorentzVector{0.,0.,-Ebeam,Ebeam} ) ;
       
    //Gaudi::LorentzVector protonA{0.,0.,Ebeam,Ebeam} ;
    //protonA.Boost( boostvector ) ;
    //Gaudi::LorentzVector protonB(0.,0.,-Ebeam,Ebeam) ;
    //protonB.Boost( boostvector ) ;
    auto zdir = (0.5*(protonA.Vect().Unit() - protonB.Vect().Unit() )).Unit() ;
    return p4mprimedir.Dot( zdir ) ;
  }

  double cosThetaStarCS( const LHCb::State& stateMuPlus, const LHCb::State& stateMuMinus,
			 double mumass)
  {
    return cosThetaStarCS(lorentzVector(stateMuPlus,mumass), lorentzVector(stateMuMinus,mumass) ) ;
  }

  /*
  double phidomain(double phi)
  {
    return phi>M_PI ? (phi-M_PI) : ( phi<-M_PI ? phi+M_PI : phi ) ;
  }
  */

  double decayPlanePhi( const LHCb::State& stateMuPlus, const LHCb::State& stateMuMinus )
  {
    double dtx = stateMuPlus.tx() - stateMuMinus.tx() ;
    double dty = stateMuMinus.ty() - stateMuPlus.ty() ;
    return std::atan2( dty, dtx ) ;
  }

  /*
  double massErrorFromp4Cov( const Gaudi::LorentzVector& p4,
			     const Gaudi::SymMatrix4x4& p4cov)
  {
    // m = sqrt( E^2-p^2)
    const double M = p4.M() ;
    Gaudi::Vector4 J ;
    J(0) = -p4.Px()/M ;
    J(1) = -p4.Py()/M ;
    J(2) = -p4.Pz()/M ;
    J(3) =  p4.E()/M ;
    return std::sqrt(ROOT::Math::Similarity(J,p4cov)) ;
  }
  */

  template<class OSStream>
  void printTrack(const LHCb::Track& trk, OSStream& os)
  {
    os << "First state: "
       << trk.firstState().p() << "+/-" << std::sqrt( trk.firstState().errP2() ) << std::endl
       << trk << std::endl ;
  }
  
  struct Histogrammer
  {
    AIDA::IHistogram1D* m_vertexchi2H1 ;
    AIDA::IHistogram1D* m_massH1 ;
    AIDA::IHistogram1D* m_massErrH1 ;
    AIDA::IHistogram1D* m_massErrSlopeH1 ;
    AIDA::IHistogram1D* m_massErrMomH1 ;
    AIDA::IProfile1D* m_afbPr ;
    AIDA::IProfile1D* m_afbVertexedPr ;
    // 
  // AIDA::IProfile1D* m_massVsMomPr ;
  // AIDA::IProfile1D* m_massVsPtPr  ;
  // AIDA::IProfile1D* m_massVsEtaPr;
  // AIDA::IProfile1D* m_massVsMomDiffPr       ;
  // AIDA::IProfile1D* m_massVsMomDiffTyPosPr ;
  // AIDA::IProfile1D* m_massVsMomDiffTyNegPr ;
  // AIDA::IProfile1D* m_massVsMomDiffTyPosPosPr ;
  // AIDA::IProfile1D* m_massVsMomDiffTyPosNegPr ;
  // AIDA::IProfile1D* m_massVsMomDiffTyNegPosPr ;
  // AIDA::IProfile1D* m_massVsMomDiffTyNegNegPr ;
                             
  // AIDA::IProfile1D* m_massVsCosThetaStarPr  ;
  // AIDA::IProfile1D* m_massVsMomAsymPr ;
  // AIDA::IProfile1D* m_massVsTyPosPr  ;
  // AIDA::IProfile1D* m_massVsTxPosPr ;
  // AIDA::IProfile1D* m_massVsTyNegPr ;
  // AIDA::IProfile1D* m_massVsTxNegPr ;
    AIDA::IProfile1D* m_massVsPhiPosPr ;
    AIDA::IProfile1D* m_massVsPhiNegPr;
    AIDA::IProfile1D* m_massVsDecayPlanePhiPr ;
    AIDA::IHistogram2D* m_massVsPhiPosH2 ;
    AIDA::IHistogram2D* m_massVsPhiNegH2 ;
    AIDA::IHistogram2D* m_massVsDecayPlanePhiH2 ;
    AIDA::IProfile1D* m_massErrVsDecayPlanePhiPr ;
    AIDA::IProfile1D* m_massErrSlopeVsPhiPosPr ;
    AIDA::IProfile1D* m_massErrMomVsPhiPosPr ;
    AIDA::IProfile1D* m_massErrSlopeVsPhiNegPr ;
    AIDA::IProfile1D* m_massErrMomVsPhiNegPr ;
    AIDA::IHistogram1D* m_muonMomH1 ;
    AIDA::IHistogram1D* m_deltaXT1H1 ;
    AIDA::IProfile1D* m_deltaXT1VsQOverPPr ;
    
  // AIDA::IProfile1D* m_afbPr ;
    
    void book(const std::string& prefix, GaudiHistoAlg& p )
    {
      m_vertexchi2H1 = p.book1D(prefix + "/vertexchi2","vertex chi2 " + prefix,0,20) ;
      m_massH1 = p.book1D(prefix + "/mass","mass " + prefix,60,160) ;
      m_massErrH1 = p.book1D(prefix + "/masserr","mass error" + prefix,0,3) ;
      m_massErrMomH1 = p.book1D(prefix + "/masserrmom","mass error from momenta" + prefix,0,3) ;
      m_massErrSlopeH1 = p.book1D(prefix + "/masserrslope","mass error from slopes" + prefix,0,3) ;
      
      std::vector<double> binboundaries = { 60., 70., 80., 84., 86., 88., 89., 90., 90.5, 91., 91.5, 92., 93., 94., 96, 100., 120., 160. } ;
      //binboundaries = { 60,70,85,87,89,91,93,98,105,120,160} ;
      //int nbins = int(binboundaries.size())-1 ;
      //float *bins = &(binboundaries.front()) ;
      m_afbPr         = p.bookProfile1D(prefix + "/afbPr","AFB " + prefix, binboundaries) ;
      m_afbVertexedPr = p.bookProfile1D(prefix + "/afbPr","AFB (vertexed)" + prefix, binboundaries) ;
      m_massVsPhiPosPr = p.bookProfile1D(prefix + "/massVsPhiPos","mass vs phi of positive track " + prefix,-M_PI,M_PI) ;
      m_massVsPhiNegPr = p.bookProfile1D(prefix + "/massVsPhiNeg","mass vs phi of negative track " + prefix,-M_PI,M_PI) ;
      m_massVsDecayPlanePhiPr = p.bookProfile1D(prefix + "/massVsDecayPlanePhi","mass vs decay plane phi" + prefix,-M_PI,M_PI) ;
      m_massVsPhiPosH2 = p.book2D(prefix + "/massVsPhiPosH2","mass vs phi of positive track " + prefix,-M_PI,M_PI,20,60.0,120.0,60) ;
      m_massVsPhiNegH2 = p.book2D(prefix + "/massVsPhiNegH2","mass vs phi of negative track " + prefix,-M_PI,M_PI,20,60.0,120.0,60) ;
      m_massVsDecayPlanePhiH2 = p.book2D(prefix + "/massVsDecayPlanePhiH2","mass vs decay plane phi" + prefix,-M_PI,M_PI,20,60.0,120.0,60) ;
      m_massErrVsDecayPlanePhiPr = p.bookProfile1D(prefix + "/massErrVsDecayPlanePhi","mass error vs decay plane phi" + prefix,-M_PI,M_PI) ;

      m_massErrSlopeVsPhiPosPr =  p.bookProfile1D(prefix + "/massErrSlopeVsPhiPos","mass err slope vs phi of positive track " + prefix,-M_PI,M_PI) ;
      m_massErrMomVsPhiPosPr   =  p.bookProfile1D(prefix + "/massErrMomVsPhiPos","mass err mom vs phi of positive track " + prefix,-M_PI,M_PI) ;
      m_massErrSlopeVsPhiNegPr =  p.bookProfile1D(prefix + "/massErrSlopeVsPhiNeg","mass err slope vs phi of negative track " + prefix,-M_PI,M_PI) ;
      m_massErrMomVsPhiNegPr   =  p.bookProfile1D(prefix + "/massErrMomVsPhiNeg","mass err mom vs phi of negative track " + prefix,-M_PI,M_PI) ;
      m_muonMomH1 = p.book1D(prefix + "/muonMom","muon momentum (GeV) " + prefix,0,2000) ;
      m_deltaXT1H1 = p.book1D(prefix + "/deltaXT1","delta X at T1 " + prefix,-30,30) ;
      m_deltaXT1VsQOverPPr = p.bookProfile1D(prefix + "/deltaXT1VsQOverP", "delta X (mm) at T1 vs q/p (1/GeV)" + prefix,-0.01,0.01) ;
    }
  } ;
}

class TrackZ0Monitor : public GaudiHistoAlg
{
public:

  /** Standard construtor */
  TrackZ0Monitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Destructor */
  virtual ~TrackZ0Monitor();

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm initialize */
  StatusCode finalize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  std::string m_inputLocation; // Input Tracks container location
  ILHCbMagnetSvc* m_magfieldsvc;
  ToolHandle<ITrackStateProvider> m_stateprovider;
  const LHCb::IParticlePropertySvc* m_propertysvc;

  Histogrammer m_hup ;
  Histogrammer m_hdown ;
} ;



// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackZ0Monitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  TrackZ0Monitor::TrackZ0Monitor( const std::string& name,
                                              ISvcLocator* pSvcLocator)
    : GaudiHistoAlg( name , pSvcLocator ),
      m_magfieldsvc(0),
      m_stateprovider("TrackStateProvider"),
      m_propertysvc(0)
{
  declareProperty( "InputLocation", m_inputLocation = "" );

}

//=============================================================================
// Destructor
//=============================================================================
TrackZ0Monitor::~TrackZ0Monitor()
{
}

StatusCode TrackZ0Monitor::initialize()
{
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  // SETUP SERVICES
  m_magfieldsvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  m_propertysvc = svc<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc",true);
  m_stateprovider.retrieve().ignore();

  m_hup.book( "UP", *this ) ;
  m_hdown.book( "DOWN", *this ) ;
  
    
  //   // m_massVsMomPr = bookProfile1D("massVsMom","mass vs mom",0,3000) ;
  // m_massVsPtPr  = bookProfile1D("massVsPt","mass vs p_T",0,100) ;
  // m_massVsEtaPr = bookProfile1D("massVsEta","mass vs eta",2,8) ;
  // m_massVsMomDiffPr = bookProfile1D("massVsMomDiff","mass vs mom-diff",-1000,1000) ;
  // m_massVsMomDiffTyPosPr = bookProfile1D("massVsMomDiffTyPos","mass vs mom-diff for positive track Ty>0",-1000,1000) ;
  // m_massVsMomDiffTyNegPr = bookProfile1D("massVsMomDiffTyNeg","mass vs mom-diff for positive track Ty<0",-1000,1000) ;

  // m_massVsMomDiffTyPosPosPr = bookProfile1D("massVsMomDiffTyPosPos",
  //                                                   "mass vs mom-diff for Ty_pos>0, Ty_neg>0",-1000,1000) ;
  // m_massVsMomDiffTyPosNegPr = bookProfile1D("massVsMomDiffTyPosNeg",
  //                                                   "mass vs mom-diff for Ty_pos>0, Ty_neg<0",-1000,1000) ;
  // m_massVsMomDiffTyNegPosPr = bookProfile1D("massVsMomDiffTyNegPos",
  //                                                   "mass vs mom-diff for Ty_pos<0, Ty_neg>0",-1000,1000) ;
  // m_massVsMomDiffTyNegNegPr = bookProfile1D("massVsMomDiffTyNegNeg",
  //                                                   "mass vs mom-diff for Ty_pos<0, Ty_neg<0",-1000,1000) ;
  // m_massVsCosThetaStarPr = bookProfile1D("massVsCosThetaStar","mass vs cos-theta-star",-1,1) ;
  // m_massVsMomAsymPr = bookProfile1D("massVsMomAsym","mass vs mom-asym",-1,1) ;
  // m_massVsTyPosPr = bookProfile1D("massVsTyPos","z-mass vs ty of positive track",-0.25,0.25) ;
  // m_massVsTxPosPr = bookProfile1D("massVsTxPos","z-mass vs tx of positive track",-0.3,0.3) ;
  // m_massVsPhiPosPr = bookProfile1D("massVsPhiPos","z-mass vs phi of positive track",-TMath::Pi(),TMath::Pi()) ;
  // m_massVsTyNegPr = bookProfile1D("massVsTyNeg","z-mass vs ty of negative track",-0.25,0.25) ;
  // m_massVsTxNegPr = bookProfile1D("massVsTxNeg","z-mass vs tx of negative track",-0.3,0.3) ;
  // m_massVsPhiNegPr = bookProfile1D("massVsPhiNeg","z-mass vs phi of negative track",-TMath::Pi(),TMath::Pi()) ;

  // RETURN
  return sc;
}

StatusCode TrackZ0Monitor::finalize()
{
  m_stateprovider.release().ignore();
  return  GaudiHistoAlg::finalize();
}


StatusCode TrackZ0Monitor::execute()
{
  LHCb::Particle::Range particles  = get<LHCb::Particle::Range>(m_inputLocation);
  for( const LHCb::Particle* particle: particles) {
    
    std::vector< const LHCb::Track* > tracks;
    std::vector< double > masshypos;
    addTracks(*particle, tracks, masshypos);

    std::vector< const LHCb::State* > states;
    double z = particle->referencePoint().z();

    // make sure the first track is the positive one
    const LHCb::Track* mup = tracks.front() ;
    const LHCb::Track* mum = tracks.back() ;
    const double mumass = masshypos.front() ;
    
    if( mup->firstState().qOverP() < 0 ) std::swap( mup, mum) ;
        
    LHCb::State stateA ;
    m_stateprovider->clearCache(*mup) ;
    m_stateprovider->clearCache(*mum) ;
    m_stateprovider->stateFromTrajectory(stateA,*mup,z).ignore();
    LHCb::State stateB  ;
    m_stateprovider->stateFromTrajectory(stateB,*mum,z).ignore();
    // double pdgmass(0),pdgwidth(0);
    // const LHCb::ParticleProperty* prop = m_propertysvc->find( particle->particleID() );
    // if( prop ) {
    //   pdgmass  = prop->mass() ;
    //   pdgwidth = prop->width() ;
    // }
    
    LHCb::TrackStateVertex vertex( stateA, stateB);
    vertex.fit();
    Gaudi::LorentzVector p4 = vertex.p4( masshypos ) ;
    LHCb::State stateAvertexed = vertex.state(0);
    LHCb::State stateBvertexed = vertex.state(1);
    
    const bool fieldDown = m_magfieldsvc->isDown() ;
    const double massGeV = p4.M()/Gaudi::Units::GeV;
    const double massError = vertex.massErr( masshypos ) ;
    const double massErrorGeV = massError/Gaudi::Units::GeV;
    const double AFB         =  cosThetaStarCS( stateA, stateB, mumass) >0 ? 1.0 : -1.0 ;
    const double AFBVertexed =  cosThetaStarCS( stateAvertexed, stateBvertexed, mumass) >0 ? 1.0 : -1.0 ;
    const double planePhi = decayPlanePhi( stateA, stateB ) ;
    const double posPhi = std::atan2(stateA.ty(),stateA.tx()) ;
    const double negPhi = std::atan2(stateB.ty(),stateB.tx()) ;
    
    const double MassInGeV = 91.1876 ;
    const double MassWin = 5 ;

    Gaudi::Vector3 JA, JB ;
    computeDerivatives( stateA,stateB,mumass,JA,JB ) ;
    const double massErrSlopeA = std::sqrt( ROOT::Math::Similarity( JA.Sub<Gaudi::Vector2>(0), 
								    stateA.covariance().Sub<Gaudi::SymMatrix2x2>(2,2) ) ) ;
    const double massErrMomA = std::abs( JA(2) ) * std::sqrt( stateA.covariance()(4,4) ) ;
    const double massErrSlopeB = std::sqrt( ROOT::Math::Similarity( JB.Sub<Gaudi::Vector2>(0), 
								    stateB.covariance().Sub<Gaudi::SymMatrix2x2>(2,2) ) ) ;
    const double massErrMomB = std::abs( JB(2) ) * std::sqrt( stateB.covariance()(4,4) ) ;
    const double massErr2Slope =
      ROOT::Math::Similarity( JA.Sub<Gaudi::Vector2>(0), 
			      stateA.covariance().Sub<Gaudi::SymMatrix2x2>(2,2) )
      + ROOT::Math::Similarity( JB.Sub<Gaudi::Vector2>(0), 
				stateB.covariance().Sub<Gaudi::SymMatrix2x2>(2,2) );
    const double massErr2Mom = massError*massError - massErr2Slope ;
    
    if( massErrorGeV > 0.1 * massGeV ) {
      std::stringstream ss ;
      
      ss << "Strange error on mass: " << massGeV << "+/-" << massErrorGeV << std::endl
	 << "StateA: " << stateA.p() << " " << std::sqrt(stateA.errP2()) << std::endl
	 << "StateB: " << stateB.p() << " " << std::sqrt(stateB.errP2()) << std::endl ;
      if(  std::sqrt(stateB.errP2()) > 0.1 * stateB.p() )
	printTrack(*mum,ss) ;
      if(  std::sqrt(stateA.errP2()) > 0.1 * stateA.p() )
	printTrack(*mup,ss) ;
      info() << ss.str() << endmsg ;
    }
    
    Histogrammer* h = fieldDown ? &m_hdown : & m_hup ;
    h->m_vertexchi2H1->fill( vertex.chi2() ) ;
    h->m_afbPr->fill( massGeV, AFB) ;
    h->m_afbVertexedPr->fill( massGeV, AFBVertexed) ;
    h->m_massH1->fill( massGeV ) ;
    if( std::abs( massGeV - MassInGeV ) < MassWin ) {
      h->m_massErrH1->fill( massErrorGeV ) ;
      h->m_massErrSlopeH1->fill( std::sqrt( massErr2Slope ) / Gaudi::Units::GeV ) ;
      h->m_massErrMomH1->fill( std::sqrt( massErr2Mom ) / Gaudi::Units::GeV ) ;
      h->m_massVsPhiPosPr->fill( posPhi, massGeV ) ;
      h->m_massVsPhiNegPr->fill( negPhi, massGeV ) ;
      h->m_massVsDecayPlanePhiPr->fill( planePhi, massGeV ) ;
      h->m_massErrVsDecayPlanePhiPr->fill( planePhi, massErrorGeV ) ;
      for( const auto& trk : tracks ) {
	const auto& state = trk->firstState() ;
	h->m_muonMomH1->fill( state.p() /Gaudi::Units::GeV) ;
	const auto* Tstate = trk->stateAt( LHCb::State::Location::AtT ) ;
	if( Tstate ) {
	  const double dx = Tstate->x() - (state.x() + ( Tstate->z() - state.z()) * state.tx() ) ;
	  h->m_deltaXT1H1->fill( dx ) ;
	  h->m_deltaXT1VsQOverPPr->fill( state.qOverP()*Gaudi::Units::GeV, dx ) ;
	}
      }
    }
    h->m_massVsPhiPosH2->fill( posPhi, massGeV ) ;
    h->m_massVsPhiNegH2->fill( negPhi, massGeV ) ;
    h->m_massVsDecayPlanePhiH2->fill( planePhi, massGeV ) ;
    h->m_massErrMomVsPhiPosPr->fill( posPhi, massErrMomA/Gaudi::Units::GeV ) ;
    h->m_massErrSlopeVsPhiPosPr->fill( posPhi, massErrSlopeA/Gaudi::Units::GeV ) ;
    h->m_massErrMomVsPhiNegPr->fill( negPhi, massErrMomB/Gaudi::Units::GeV ) ;
    h->m_massErrSlopeVsPhiNegPr->fill( negPhi, massErrSlopeB/Gaudi::Units::GeV ) ;
  }

  return StatusCode::SUCCESS;
}
