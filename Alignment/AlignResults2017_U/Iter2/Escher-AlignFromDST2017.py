###############################################################################
# File for running alignment with D0 reconstructed from HLT sel reports
###############################################################################
# Syntax is:
#   gaudiiter.py Escher-AlignHltJpsi.py <someDataFiles>.py
###############################################################################

#import os
#os.environ['NO_GIT_CONDDB']='1'
#os.environ['SQLITEDBPATH']='/afs/cern.ch/lhcb/software/DEV/DBASE/Det/SQLDDDB/db'

from Gaudi.Configuration import *
MessageSvc().Format = '% F%40W%S%7W%R%T %0W%M'

from Configurables import Escher

# Just instantiate the configurable...
theApp = Escher()
theApp.DataType   = "2017"
theApp.InputType  = "DST"
theApp.PrintFreq = 10
theApp.EvtMax = 10000
theApp.DatasetName = 'AlignZ0FromDST'
theApp.RecoSequence = ["Decoding"]
#theApp.RecoSequence = []
theApp.MoniSequence = []
#theApp.DDDBtag   = 'dddb-20150724'
#theApp.CondDBtag = 'cond-20170325'
#theApp.DDDBtag   = 'dddb-20170721-3'
#theApp.CondDBtag = 'default'
#theApp.CondDBtag  = "cond-20170325"
#theApp.DDDBtag    = "dddb-20170721-3"
#from Configurables import LHCbApp
#LHCbApp().CaliboffTag = 'ttitot-20151228'
#from Configurables import CondDB
#CondDB().Tags['CALIBOFF']='ttitot-20151228'
#CondDB().LoadCALIBDB = "HLT1"

theApp.UseDBSnapshot = False

from Configurables import DataOnDemandSvc
#DataOnDemandSvc().AlgMap["Raw/Velo/Clusters"]     = "DecodeVeloRawBuffer/createVeloClusters"
#DataOnDemandSvc().AlgMap["Raw/Velo/LiteClusters"] = "DecodeVeloRawBuffer/createVeloLiteClusters"
DataOnDemandSvc().AlgMap["Raw/Velo/Clusters"]     = "DecodeVeloRawBuffer/createBothVeloClusters"
DataOnDemandSvc().AlgMap["Raw/Velo/LiteClusters"] = "DecodeVeloRawBuffer/createBothVeloClusters"
DataOnDemandSvc().AlgMap["Raw/TT/Clusters"]       = "RawBankToSTClusterAlg/createTTClusters"
DataOnDemandSvc().AlgMap["Raw/IT/Clusters"]       = "RawBankToSTClusterAlg/createITClusters"
DataOnDemandSvc().AlgMap["Raw/UT/Clusters"]       = "RawBankToSTClusterAlg/createUTClusters"
DataOnDemandSvc().AlgMap["Raw/TT/LiteClusters"]   = "RawBankToSTLiteClusterAlg/createTTLiteClusters"
DataOnDemandSvc().AlgMap["Raw/IT/LiteClusters"]   = "RawBankToSTLiteClusterAlg/createITLiteClusters"
DataOnDemandSvc().AlgMap["Raw/UT/LiteClusters"]   = "RawBankToSTLiteClusterAlg/createUTLiteClusters"

ignoreTTHits = False
ignoreITHits = False
addOtherTracks = True

# gain some time by disabling Hlt sequence
from Configurables import GaudiSequencer
GaudiSequencer("HltFilterSeq").Enable = False
# make sure the 

from Configurables import TrackSys
TrackSys().ExpertTracking += [ "simplifiedGeometry" ]
#TrackSys().ExpertTracking += [ "noDrifttimes" ]

# specify what we actually align for
from TAlignment.AlignmentScenarios import *
TAlignment().WriteCondSubDetList += ['TT','IT','OT','VeloModules']
#configure2012DataAlignment()
#configureEarlyDataAlignment()

# turn on histogramming
from Configurables import AlignAlgorithm
AlignAlgorithm('Alignment').FillHistos = True

# define the alignment elements
elements = Alignables()
elements.Velo("None")
#elements.VeloRight("TxTyRzRy")
#elements.VeloLeft("TxTyRzRy")
# make sure that the velo stays where it was
TAlignment().Constraints = constraints = []
TAlignment().DataCondType = 'Run2'

surveyconstraints = SurveyConstraints()
surveyconstraints.All('Run2')

# constraints.append("VeloHalfAverage : Velo/Velo(Left|Right) : Tx Ty Rz Ry : total ")
# constrain the velo rotations back to survey: releasing them didn't help for the Z.
# surveyconstraints.Constraints += [ "Velo  : 0 0 0 -0.0001 0 -0.0001 : 0.2 0.2 0.2 0.000000001 0.000000001 0.001" ]

elements.Tracker("None")
elements.OT("None")
elements.IT("None")
elements.TT("None")
longmodules  = ["/M1","/M2","/M3","/M4","/M5","/M6","/M7"]
shortmodules = ["/M8","/M9"]

elements.ITBoxes("None")
elements.TTLayers("None")
elements.OTCFrames("None")
#elements.OTModules("None")

import os
scenario = os.environ['ALIGNSCENARIO']
print 'alignscenario: %s' % scenario

if scenario.find("OT") >=0 :
    elements.OTCFrames("TxRz")
    elements.OTModules("TxRz", longmodules)
    elements.OTHalfModules("TxRz", shortmodules)
    surveyconstraints.Constraints += [ "OT/.*?M. : 0 0 0 0 0 0 : 0.50 0.05 0.05 0.00001 0.001 0.0001" ]    
    surveyconstraints.Constraints += [ "OT/.*?M9 : 0 0 0 0 0 0 : 2.00 0.05 0.05 0.00001 0.001 0.001" ]
    surveyconstraints.Constraints += [ "OT/.*?M8 : 0 0 0 0 0 0 : 2.00 0.05 0.05 0.00001 0.001 0.001" ]    
    if scenario.find("OTz") >=0 :
        elements.OTCFrameLayers("TzRy")
        if scenario.find("OTzC") >=0 :
            # tweak the survey a little bit to fix the z-scale to survey ... or not?
            surveyconstraints.XmlUncertainties += ["OT/T3X1U : 0.5 0.5 0.00001 0.0001 0.0001 0.0001" ]
    # align modules M9 in z: I see a strange pattern!
    #if scenario.find("OTzM9") >=0 :
    if scenario.find("OTGlobal") >= 0 :
        elements.OT("TxRz")
    if scenario.find("OTCFrameRz") >= 0 :
        elements.OTCFrames("Rz")
        surveyconstraints.XmlUncertainties += ["OT/T.(X1U|VX2).Side : 0.5 0.5 0.5 0.0001 0.0001 0.001" ]
if scenario.find("TT") >=0 :
    elements.TTLayers("Tz")
    elements.TTHalfModules("TxRz")
    elements.TTShortModules("TxTzRxRz")
    # also release some stuff in TT, in particular the modules above/below beampipe
    surveyconstraints.XmlUncertainties += ["TT..Layer : 0.1 0.1 0.5 0.0005 0.0005 0.0005"]
    surveyconstraints.XmlUncertainties += ["TT.*?Module.*?(B|T) : 1.0 0.1 0.1 0.0005 0.0005 0.005"]
    if scenario.find("TTGlobal") >= 0 :
        elements.TT("TxRz")
if scenario.find("IT") >=0 :
    if scenario.find("BoxYIT") >=0 :
        elements.ITBoxes("TxTyTzRz")
    else:
        elements.ITBoxes("TxTzRz")
    elements.ITLayers("TxTz")
    # release z positions of IT boxes
    surveyconstraints.XmlUncertainties += ["ITT.*?Box : 2.0 2.0 5.0 0.001 0.001 0.002",
                                           "ITT.*?Layer.{1,2} : 0.2 0.05 0.2 0.0001 0.0001 0.001",]
    if scenario.find("ITInternal") >=0 :
        elements.ITLadders("TxRz")
if scenario.find("VeloGlobal") >=0 :
    TAlignment().WriteCondSubDetList += ['Velo']
    elements.Velo("None")
    elements.VeloRight("TxTyTzRxRyRz")
    elements.VeloLeft("TxTyTzRxRyRz")
    # make sure that the velo stays where it was
    constraints.append("VeloHalfAverage : Velo/Velo(Left|Right) : Tx Ty Tz Rx Ry Rz")

#slight increase the uncdrtainty on Ry since that seems to be small.
surveyconstraints.XmlUncertainties += ["Module(PU|).. : 0.02 0.02 0.02 0.0002 0.0005 0.0002" ]
#surveyconstraints.XmlUncertainties += ["Module20 : 0.0001 0.0001 0.0001 0.0002 0.001 0.000001"]
surveyconstraints.XmlUncertainties += ["Module(12|38) : 0.0001 0.0001 0.0001 0.000001 0.000001 0.000001"]

if scenario.find("VeloModules") >=0 :
    elements.Velo("None")
    elements.VeloRight("None")
    elements.VeloLeft("None")
    elements.VeloModules("TxTyRz")
    #constraints.append("VeloModuleTxTyRz : Velo/VeloLeft/Module.. : Tx Ty Rz Szx Szy SRz : survey")
    
    elements.VeloPhiSensors("None")
    elements.VeloRSensors("None")
    if scenario.find("VeloModulesZ") >=0 :
        elements.VeloModules("TxTyRzTz")
        #constraints.append("VeloModule12Tz : Velo/VeloLeft/Module12 : Tz : survey")
        #constraints.append("VeloModule40Tz : Velo/VeloLeft/Module40 : Tz : survey")
        #increase survey uncertainties on z and Ry
        #surveyconstraints.XmlUncertainties += ["Module(PU|).. : 0.02 0.02 0.1 0.0002 0.001 0.0002"]
        if scenario.find("VeloModulesZRy") >=0 :
            elements.VeloModules("TxTyRzTzRy")
        if scenario.find("VeloModulesZRyRx") >=0 :
            elements.VeloModules("TxTyRzTzRyRx")
    #fix module 20 in x,y,z,Rz
if scenario.find("VeloSensors") >=0 :
    elements.Velo("None")
    elements.VeloRight("None")
    elements.VeloLeft("None")
    elements.VeloModules("TxTyTzRxRzRy")
    #constraints.append("VeloModuleTxTyRz : Velo/VeloLeft/Module.. : Tx Ty Rz Szx Szy SRz : survey")
    #constraints.append("VeloModule12Tz : Velo/VeloLeft/Module12 : Tz : survey")
    #constraints.append("VeloModule40Tz : Velo/VeloLeft/Module40 : Tz : survey")

    # constrain also the average Rx and Ry and the right and left side
    #constraints.append("VeloModuleLeftRxRy : Velo/VeloLeft/Module.. : Rx Ry : survey")
    #constraints.append("VeloModuleRightRxRy : Velo/VeloRight/Module.. : Rx Ry : survey")
    
    elements.VeloPhiSensors("TxTy")
    elements.VeloRSensors("None")

if scenario.find("VeloGlobal") >=0 :
    elements.Velo("RzRyRx")

#global Tz seems a really bad idea: the mass-resolution becomes much worse    
if scenario.find("TrackerTz") >=0 :
    elements.Tracker("Tz")
        
#elements.OTLayersASide("TxRz")
#elements.OTLayersCSide("TxRz")
#elements.OTCFrameLayers("Tz")
#elements.OTModules("Rz", modules = longmodules )
#elements.OTHalfModules("Tx", modules = longmodules)
# release separate Rz only for modules 8 and 9
#elements.OTHalfModules("TxRz", shortmodules )
#elements.OTHalfModules("Tx")
for i in elements:
    print i
    
#elements.IT("TxTzRz")
#elements.OT("TxTzRz")
#elements.TT("TxTzRz")
TAlignment().ElementsToAlign = list(elements)

# release x positions of modules 9
#surveyconstraints.Constraints += [ "OT/.*?M. : 0 0 0 0 0 0 : 2.00 0.05 0.05 0.00001 0.001 0.001" ]
#surveyconstraints.Constraints += [ "OT/.*?M9 : 0 0 0 0 0 0 : 2.00 0.05 0.05 0.00001 0.001 0.001" ]
#surveyconstraints.Constraints += [ "OT/.*?M8 : 0 0 0 0 0 0 : 2.00 0.05 0.05 0.00001 0.001 0.001" ]


# can we introduce something to fix the X scaling observed in OT?
#TAlignment().Constraints += ["T%d%sQ%dSxx : OT/T%d%sQ%dM. : Sxx : total" % (s,l,q,s,l,q) \
#                             for s in [1,2,3] \
#                             for l in ["X1","U","V","X2"] \
#                             for q in [0,1,2,3] ]

# specify the input to the alignment
from Configurables import TAlignment

    
    
AlignAlgorithm("Alignment").OutputLevel = 1

from Configurables import FilterDesktop
# first make a preselection of the stripping candidates such that we
# need to refit a few less tracks. this is also the mass range for
# which we 'monitor' the resolution. (we need the tracks to be
# refitted for the monitoring.)
strippinglocation = '/Event/seq/Phys/ZSel/Particles'
strippinglocation = '/Event/seq/Phys/sel/Particles'

prefilter = FilterDesktop( 'Z02MuMuLinePre',
                           Inputs = [ strippinglocation ],
                           Code = "(M>60*GeV) & (M<160*GeV) & (MINTREE (HASTRACK,PT)>20*GeV) "
                           " & (CHILD(PT,1)>10*GeV) & (CHILD(PT,2)>10*GeV) " 
                           " & (MAXTREE (HASTRACK,BPVIPCHI2())<10)"
                           " & (M*M > CHILD(P,1) * CHILD(P,2) * 0.08*0.08) " 
                           " & (CHILD(P,1)<2000*GeV) & (CHILD(P,2)<2000*GeV)" )
#                           " & (MINANG(ALL) > 0.03)" )

#from Configurables import OTHitMultiplicityFilter
#otfilter = OTHitMultiplicityFilter("DefaultOTHitMultiplicityFilter",MaxNumOTHits = 6000)

GaudiSequencer("AlignSequence", IgnoreFilterPassed = True).Members += [ prefilter ]

# now create the particle selection for alignment
from TAlignment.ParticleSelections import configuredParticleListFromDST
z0sel =  configuredParticleListFromDST( ParticleLocation = '/Event/Phys/Z02MuMuLinePre/Particles'
                                      , FilterCode =  "(M>86.1876*GeV) & (M<96.1876*GeV)" ) 
TAlignment().ParticleSelections = [ z0sel ]

if TrackSys().noDrifttimes():
    #something goes wrong in configuring fitters. let;s try this:
    fitter  = z0sel.algorithm().Members[0].TrackFitter
    from Configurables import TrackProjectorSelector, TrajOTProjector
    fitter.Projector = TrackProjectorSelector()
    defaultOTNoDriftTimeProjector = TrajOTProjector("OTNoDrifttimesProjector")
    defaultOTNoDriftTimeProjector.UseDrift = False
    fitter.Projector.OT = defaultOTNoDriftTimeProjector

    
# create a sequence with nice long tracks. these need to be fitted and clones removed. shall we use TrackBestTrackSelector?
# first get the cocktail
# now build a new selection to add the refitting part
from  TAlignment.TrackSelections import TrackSelection, TrackRefiner
# Selection for good long tracks for alignment
TAlignment().TrackSelections = []
if addOtherTracks :
    class LowChi2Tracks( TrackRefiner ):
        def configureSelector( self, a ):
            from Configurables import TrackSelector
            a.Selector = TrackSelector()
            a.Selector.MaxChi2Cut = 3
            a.Selector.MaxChi2PerDoFMatch = 3
            a.Selector.MaxChi2PerDoFVelo = 3
            a.Selector.MaxNTHoles = 0
            #a.Selector.MaxChi2PerDoFDownstream = 5
    class FavouriteCocktailWithRefit( TrackSelection ) :
        def algorithm( self ) :
            from TAlignment.TrackSelections import FavouriteTrackCocktail
            cocktail  = FavouriteTrackCocktail(self.name() + 'BeforeFit',Fitted = False)
            lowchi2sel = LowChi2Tracks(self.name() + 'AfterFit', InputLocation = self.location() + "ReFitted") 
            from Configurables import GaudiSequencer, TrackContainerCopy, CountingPrescaler, TrackSelectionMerger,TrackHitAdder
            from Configurables import OTHitMultiplicityFilter, TrackStateInitAlg
            seq = GaudiSequencer("TrackSelSeq", IgnoreFilterPassed = True)
            seq.Members += [
            GaudiSequencer(self.name()+"FilterSeq",
                           Members = [
                               #OTHitMultiplicityFilter(self.name() + "OTMultiplicityFilter",MaxNumOTHits = 4000),
            #CountingPrescaler(self.name() + "Prescaler", Offset=0, Interval=5),
                               cocktail.algorithm(),
                               TrackContainerCopy(self.name() + "Copier",
                                                  inputLocation = cocktail.location(),
                                                  outputLocation = self.location() + "PreFitted"),
                               TrackStateInitAlg( "BestForAlignmentInitAlg", TrackLocation = self.location()+ "PreFitted" ),
                               ConfiguredEventFitter( "BestForAlignmentFitter",
                                                    TracksInContainer = self.location()+ "PreFitted",
                                                    TracksOutContainer = self.location()+ "Fitted",
                                                      SimplifiedGeometry = True ),
                               TrackHitAdder("TrackHitAdder",TrackLocation = self.location()+ "Fitted"),
                               ConfiguredEventFitter( "BestForAlignmentReFitter",
                                                       TracksInContainer = self.location()+ "Fitted",
                                                          TracksOutContainer = self.location()+ "ReFitted",
                                                      SimplifiedGeometry = True ),
                               lowchi2sel.algorithm() ]),
            TrackSelectionMerger(self.name()+"Merger",
#                            IgnoreIfMissing = True,
                            InputLocations = [ lowchi2sel.location() ],
                            OutputLocation = self.location())
            ]
            from Configurables import TrackEventFitter
            #TrackEventFitter("BestForAlignmentFitter").Fitter.addTool(MeasurementProvider, "MeasProvider")
            TrackEventFitter("BestForAlignmentFitter").Fitter.MeasProvider.IgnoreTT = ignoreTTHits
            TrackEventFitter("BestForAlignmentFitter").Fitter.MeasProvider.IgnoreIT = ignoreITHits
            TrackEventFitter("BestForAlignmentReFitter").Fitter.MeasProvider.IgnoreTT = ignoreTTHits
            TrackEventFitter("BestForAlignmentReFitter").Fitter.MeasProvider.IgnoreIT = ignoreITHits
            print "Track selection output location: ", self.location()
            
            return seq
    TAlignment().TrackSelections = [ FavouriteCocktailWithRefit('BestForAlignment') ]
    # a bit ugly, bit okay for now
    minMomentumEnv = os.environ['MINMOMENTUM']
    if minMomentumEnv:
        from TAlignment.TrackSelections import TrackSelectionDict
        TrackSelectionDict["MinP"] = float(minMomentumEnv)
    
from Configurables import DstConf
DstConf(EnableUnpack=["Reconstruction", "Stripping"])


#from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
#Z0StrippingSel = AutomaticData(Location = '/Event/EW/Phys/Z02MuMuLine/Particles')
#from Configurables import FilterDesktop
#Z0Sel = Selection('GoodZ0Selection',
#                  Algorithm = FilterDesktop('GoodZ0Filter',
#                                            Code = 
#"(M>85*GeV) & (M<95*GeV)"
#                                            "(MINTREE (HASTRACK,PT)>20*GeV) "
#                                            " & (MAXTREE (HASTRACK,BPVIPCHI2())<10)"
#                                            " & (M>85.1876*GeV) & (M<97.1876*GeV)"
#                                            ),
#                                            RequiredSelections = [Z0StrippingSel])
#Z0Seq = SelectionSequence('GoodZ0Sequence',TopSelection = Z0Sel )

#GaudiSequencer("AlignSequence").Members += [Z0Seq.sequence() ]
#outputlocation =  str(Z0Sel.outputLocation())

#TAlignment().ParticleSelections = [ configuredParticleListFromDST( outputlocation ) ]

from Configurables import TrackParticleMonitor, TrackZ0Monitor
GaudiSequencer("AlignMonitorSeq").Members += [
#TrackParticleMonitor("GoodZ0Monitor", InputLocation = sel.location(), MinMass=75000, MaxMass=105000 ) 
TrackParticleMonitor("AllMuMuMonitor", InputLocation = strippinglocation,
                     MinMass=0, MaxMass=105000 ),
TrackParticleMonitor("AllZ0Monitor", InputLocation = '/Event/Phys/Z02MuMuLinePre/Particles',
                     MinMass=75000, MaxMass=105000 ),
TrackParticleMonitor("TightZ0Monitor", InputLocation = '/Event/Phys/Z02MuMuLinePreFiltered/Particles',
                     MinMass=75000, MaxMass=105000 ),
TrackZ0Monitor("TrackZ0Monitor", InputLocation = '/Event/Phys/Z02MuMuLinePre/Particles'),
TrackZ0Monitor("TightTrackZ0Monitor", InputLocation = '/Event/Phys/Z02MuMuLinePreFiltered/Particles')
]

# new try: the tight mass cut should only come after the fit

# let's get rid of TT hits in the track fit

from Configurables import TrackParticleRefitter, TrackMasterFitter, MeasurementProvider
#TrackParticleRefitter("Z02MuMuLinePreRefitter").addTool(TrackMasterFitter,"TrackFitter")
TrackParticleRefitter("Z02MuMuLinePreRefitter").TrackFitter.addTool(MeasurementProvider, "MeasProvider")
TrackParticleRefitter("Z02MuMuLinePreRefitter").TrackFitter.MeasProvider.IgnoreTT = ignoreTTHits
TrackParticleRefitter("Z02MuMuLinePreRefitter").TrackFitter.MeasProvider.IgnoreIT = ignoreITHits


# make an extra monitoring loop for other tracks in the event
from Configurables import (CountingPrescaler, TrackMonitor, OTTrackMonitor,
                           TrackVeloOverlapMonitor,TrackITOverlapMonitor,TrackFitMatchMonitor, TTTrackMonitor,
                           OTHitMultiplicityFilter,TrackPV2HalfAlignMonitor )
from TrackFitter.ConfiguredFitters import ConfiguredEventFitter
monseq = GaudiSequencer("BestTrackMonSequence", IgnoreFilterPassed = False)
# insert a filter to select only small fraction of events. make sure
# that the interval is not a multiple of the number of parale jobs:
# otherwise theydon't finish simultaneously!
monseq.Members.append(OTHitMultiplicityFilter( "OTMultiplicityFilter",MaxNumOTHits = 4000) )
#monseq.Members.append( CountingPrescaler("BestTrackMonSequencePrescaler", Offset=0, Interval=19) )
# insert the trackfitter
monseq.Members.append( ConfiguredEventFitter(Name = "BestTrackRefitter", TracksInContainer = "Rec/Track/Best",
                                                 TracksOutContainer = "Rec/Track/BestRefitted", SimplifiedGeometry=True) )
# insert some of the standard monitoring algs
monseq.Members += [ TrackMonitor(FullDetail=True,TracksInContainer="Rec/Track/BestRefitted"),
                    TrackVeloOverlapMonitor(TrackLocation="Rec/Track/BestRefitted"),
                    TrackITOverlapMonitor(TrackLocation="Rec/Track/BestRefitted"),
                    TrackFitMatchMonitor(TrackContainer="Rec/Track/BestRefitted"),
                    TTTrackMonitor(TracksInContainer="Rec/Track/BestRefitted"),
                    OTTrackMonitor(TrackLocation="Rec/Track/BestRefitted"),
                    TrackPV2HalfAlignMonitor(TrackContainer="Rec/Track/BestRefitted")]
                       
GaudiSequencer("AlignMonitorSeq").Members.append(monseq)


from Configurables import OTWriteMonoAlignmentToXml,OTMonoLayerAlignment,ParticleToTrackContainer
z0trackextractorSeq = GaudiSequencer("Z0ExtracterSequence")
z0trackextractorSeq.Members += [
    z0sel.algorithm(),
    ParticleToTrackContainer("Z0TrackExtractor",
                                ParticleLocation = z0sel.location(),#'/Event/Phys/Z02MuMuLinePre/Particles',#
                                TrackLocation = "Rec/Track/SelZ0Tracks") ]
GaudiSequencer("AlignmentAlgSeq").Members += [
#z0trackextractorSeq,
#OTMonoLayerAlignment(TrackLocation = "Rec/Track/SelZ0Tracks"),
#    OTWriteMonoAlignmentToXml()
]

#TAlignment().TrackSelections.append( TrackSelection( Name = "SelZ0Tracks",
#                                                      Algorithm = z0trackextractorSeq) )

#disable pid, because it complains
GaudiSequencer("ProtoParticleCombDLLs").Enable = False


# WH: added Dece,ber 2017 to test z alignment of phi sensors
# 2018/05/18: something goes entirely wrong in velo alignment if I add this??
if addOtherTracks:
    if (scenario.find("Velo") >=0) :
        TAlignment().VertexLocation = "Rec/Vertex/PrimaryNew"
    def addPVAlg():
        from PatPV.PVConf import StandardPV
        from Configurables import PatPVOffline,TrackListMerger,TrackListRefiner,TrackVertexMonitor
        pvalg = PatPVOffline("PatPVOffline")
        StandardPV().configureAlg(pvalg)
        pvalg.InputTracks = [TrackListRefiner("BestForAlignmentAfterFitSelectorAlg").outputLocation]
        pvalg.InputTracks = "Rec/Track/BestForAlignmentFitted"
        #Rec/Track/AlignTracks"] #TrackListMerger("BestForAlignmentMerger").outputLocation
        pvalg.OutputVertices = "Rec/Vertex/PrimaryNew"
        vtxmonitor = TrackVertexMonitor("AlignVertexMonitor",
                                         PVContainer = pvalg.OutputVertices.Path,
                                         TrackContainer = "Rec/Track/AlignTracks"#pvalg.InputTracks.Path,
                                       )
        pvseq = GaudiSequencer("AlignPVSequence",Members =  [pvalg,vtxmonitor] )
        GaudiSequencer("AlignmentAlgSeq").Members.insert(0,pvseq)
         
    from Gaudi.Configuration import appendPostConfigAction
    appendPostConfigAction(addPVAlg) 

def printCONDDBsetup():
    print "Trying to print CondDB setup"
    from Configurables import CondDB, LHCbApp
    import GaudiKernel.ProcessJobOptions
    GaudiKernel.ProcessJobOptions.PrintOn()
    log.info( CondDB() )
    GaudiKernel.ProcessJobOptions.PrintOff()
    print CondDB()
    from Configurables import COOLConfSvc
    print COOLConfSvc()
    print LHCbApp()
    
    
appendPostConfigAction(printCONDDBsetup)


    
from Configurables import LHCb__ParticlePropertySvc 
ApplicationMgr().ExtSvc.append(LHCb__ParticlePropertySvc())

print("End of Escher-AlignFromDST2017.py")


