#ifndef ALIGNMENT_OTYALIGNMENT_OTYALIGNMENT_H
#define ALIGNMENT_OTYALIGNMENT_OTYALIGNMENT_H

// USER
#include "OT2DPlot.h"
#include "Parser.h"
// STL
#include <map>
#include <string>
#include <vector>
#include <list>
#include <utility>
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
// ROOT
#include "TCanvas.h"
#include "TFitResult.h"
#include "TString.h"
#include "TCut.h"
#include "TH1I.h"
#include "TH2I.h"
#include "TProfile.h"
#include "THStack.h"
#include "TPaveStats.h"
#include "TLegend.h"
#include "TF1.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TKey.h"
#include "TGraphErrors.h"

// RooFit
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooProdPdf.h"
#include "RooAddPdf.h"
#include "RooGaussian.h"
#include "RooEfficiency.h"
#include "RooEffProd.h"
#include "RooUnblindUniform.h"
#include "RooChebychev.h"
#include "RooConstVar.h"
#include "RooExponential.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooStats/SPlot.h"
#include "RooCategory.h"
#include "RooAbsBinning.h"
#include "RooBinning.h"
#include "RooEffProd.h"
#include "RooGaussModel.h"
#include "RooTruthModel.h"
#include "RooDecay.h"
#include "RooSimultaneous.h"
#include "RooHist.h"
#include "RooCurve.h"
#include "RooCustomizer.h"
#include "RooGenericPdf.h"
#include "RooTrace.h"
#include "RooAbsDataStore.h"

#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>

#include "TVerticalAlignment/OT2DPlot.h"
#include "TVerticalAlignment/Parser.h"
#include "TVerticalAlignment/TVerticalAlignment.h"

using namespace std;
using namespace RooFit;

namespace Alignment
{
  namespace TVerticalAlignment
  {

    class OTYAlignMagOff
    {
      public:
        // Constructors
        OTYAlignMagOff( TString filename, TString dbfilename, TString outputdir, bool constraint=false, bool saveplots=false );
        // Destructor
        ~OTYAlignMagOff() {}
         
        // Methods
        void fit_efficiency();
	void plots();
	void glimpse_data();

      private:
        TVerticalAlignment* m_va;
        Param m_param;
        TString m_filename;
        TString m_dbfilename;
        TString m_outDirectory;
        bool m_constraint;
        bool m_saveplots;

	bool m_glimpse = false;

        OTNames* m_Names;
        std::map<std::string, double> m_GlobalXCoord;
        std::map<std::string, double> m_GlobalYCoord;

        ofstream m_YPosFitFile;
        ofstream m_LengthFitFile;
        ofstream m_DistanceFitFile;
        ofstream m_FitParameterFile;
	TString  m_outputname;

        std::map<std::string, double> CreateXMapFromFile();
        std::map<std::string, double> CreateYMapFromFile();
        void FitfromROOTFile();
        void GetMeanFromHisto(TString ModuleName);
        double GetShift(TString ModuleName, TString ModuleName2);
        RooDataSet* GetRooDataSetFromTH1(TH1F* histoA1, TH1F* histoA1_exp, TH1F* histoA2, TH1F* histoA2_exp, TH1F* histoB1, TH1F* histoB1_exp, TH1F* histoB2, TH1F* histoB2_exp, RooRealVar y, RooRealVar weight, RooCategory cal_lr, RooCategory cat_AB, RooCategory cat_eff, double Distance, double Length, double EdgeScale);
    };
    
  }// namespace TVerticalAlignment
}// namespace Alignment

#endif
