#export GITCONDDBPATH=$PWD
RUNNINGDIR=$PWD
NUMEVENTS=-1
ALIGNDIR=AlignResults2016_D
export MINMOMENTUM=10000

# removing the 'global' velo entirely
# introducing velo-Z in stages
export ALIGNSCENARIO="TTITOT"
../build.x86_64-centos7-gcc7-opt/run gaudipariter.py --splitbyfile -n 5 -e $NUMEVENTS -p 32 \
-b $ALIGNDIR \
-l \
$RUNNINGDIR/Escher-AlignFromDST2016.py $RUNNINGDIR/Z0Files2016_D.py | tee output.txt

##adding VeloModules
export ALIGNSCENARIO="TTITOTVeloModulesZRyRx"
../build.x86_64-centos7-gcc7-opt/run gaudipariter.py --splitbyfile -n 9 -e $NUMEVENTS -p 32 \
-b $ALIGNDIR \
-l \
$RUNNINGDIR/Escher-AlignFromDST2016.py $RUNNINGDIR/Z0Files2016_D.py | tee output.txt
