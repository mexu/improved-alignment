// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME AlignmentInterfacesDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignmentInterfaces/dict/AlignmentInterfacesDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *IAlignSolvTool_Dictionary();
   static void IAlignSolvTool_TClassManip(TClass*);
   static void delete_IAlignSolvTool(void *p);
   static void deleteArray_IAlignSolvTool(void *p);
   static void destruct_IAlignSolvTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IAlignSolvTool*)
   {
      ::IAlignSolvTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IAlignSolvTool));
      static ::ROOT::TGenericClassInfo 
         instance("IAlignSolvTool", "AlignmentInterfaces/IAlignSolvTool.h", 25,
                  typeid(::IAlignSolvTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &IAlignSolvTool_Dictionary, isa_proxy, 4,
                  sizeof(::IAlignSolvTool) );
      instance.SetDelete(&delete_IAlignSolvTool);
      instance.SetDeleteArray(&deleteArray_IAlignSolvTool);
      instance.SetDestructor(&destruct_IAlignSolvTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IAlignSolvTool*)
   {
      return GenerateInitInstanceLocal((::IAlignSolvTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::IAlignSolvTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IAlignSolvTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IAlignSolvTool*)0x0)->GetClass();
      IAlignSolvTool_TClassManip(theClass);
   return theClass;
   }

   static void IAlignSolvTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlcLcLIAlignUpdateTool_Dictionary();
   static void AlcLcLIAlignUpdateTool_TClassManip(TClass*);
   static void delete_AlcLcLIAlignUpdateTool(void *p);
   static void deleteArray_AlcLcLIAlignUpdateTool(void *p);
   static void destruct_AlcLcLIAlignUpdateTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Al::IAlignUpdateTool*)
   {
      ::Al::IAlignUpdateTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Al::IAlignUpdateTool));
      static ::ROOT::TGenericClassInfo 
         instance("Al::IAlignUpdateTool", "AlignmentInterfaces/IAlignUpdateTool.h", 12,
                  typeid(::Al::IAlignUpdateTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlcLcLIAlignUpdateTool_Dictionary, isa_proxy, 4,
                  sizeof(::Al::IAlignUpdateTool) );
      instance.SetDelete(&delete_AlcLcLIAlignUpdateTool);
      instance.SetDeleteArray(&deleteArray_AlcLcLIAlignUpdateTool);
      instance.SetDestructor(&destruct_AlcLcLIAlignUpdateTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Al::IAlignUpdateTool*)
   {
      return GenerateInitInstanceLocal((::Al::IAlignUpdateTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Al::IAlignUpdateTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlcLcLIAlignUpdateTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Al::IAlignUpdateTool*)0x0)->GetClass();
      AlcLcLIAlignUpdateTool_TClassManip(theClass);
   return theClass;
   }

   static void AlcLcLIAlignUpdateTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *IATrackSelectorTool_Dictionary();
   static void IATrackSelectorTool_TClassManip(TClass*);
   static void delete_IATrackSelectorTool(void *p);
   static void deleteArray_IATrackSelectorTool(void *p);
   static void destruct_IATrackSelectorTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IATrackSelectorTool*)
   {
      ::IATrackSelectorTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IATrackSelectorTool));
      static ::ROOT::TGenericClassInfo 
         instance("IATrackSelectorTool", "AlignmentInterfaces/IATrackSelectorTool.h", 17,
                  typeid(::IATrackSelectorTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &IATrackSelectorTool_Dictionary, isa_proxy, 4,
                  sizeof(::IATrackSelectorTool) );
      instance.SetDelete(&delete_IATrackSelectorTool);
      instance.SetDeleteArray(&deleteArray_IATrackSelectorTool);
      instance.SetDestructor(&destruct_IATrackSelectorTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IATrackSelectorTool*)
   {
      return GenerateInitInstanceLocal((::IATrackSelectorTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::IATrackSelectorTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IATrackSelectorTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IATrackSelectorTool*)0x0)->GetClass();
      IATrackSelectorTool_TClassManip(theClass);
   return theClass;
   }

   static void IATrackSelectorTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ITAConfigTool_Dictionary();
   static void ITAConfigTool_TClassManip(TClass*);
   static void delete_ITAConfigTool(void *p);
   static void deleteArray_ITAConfigTool(void *p);
   static void destruct_ITAConfigTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ITAConfigTool*)
   {
      ::ITAConfigTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ITAConfigTool));
      static ::ROOT::TGenericClassInfo 
         instance("ITAConfigTool", "AlignmentInterfaces/ITAConfigTool.h", 45,
                  typeid(::ITAConfigTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ITAConfigTool_Dictionary, isa_proxy, 4,
                  sizeof(::ITAConfigTool) );
      instance.SetDelete(&delete_ITAConfigTool);
      instance.SetDeleteArray(&deleteArray_ITAConfigTool);
      instance.SetDestructor(&destruct_ITAConfigTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ITAConfigTool*)
   {
      return GenerateInitInstanceLocal((::ITAConfigTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ITAConfigTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ITAConfigTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ITAConfigTool*)0x0)->GetClass();
      ITAConfigTool_TClassManip(theClass);
   return theClass;
   }

   static void ITAConfigTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *IDerivatives_Dictionary();
   static void IDerivatives_TClassManip(TClass*);
   static void delete_IDerivatives(void *p);
   static void deleteArray_IDerivatives(void *p);
   static void destruct_IDerivatives(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IDerivatives*)
   {
      ::IDerivatives *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IDerivatives));
      static ::ROOT::TGenericClassInfo 
         instance("IDerivatives", "AlignmentInterfaces/IDerivatives.h", 25,
                  typeid(::IDerivatives), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &IDerivatives_Dictionary, isa_proxy, 4,
                  sizeof(::IDerivatives) );
      instance.SetDelete(&delete_IDerivatives);
      instance.SetDeleteArray(&deleteArray_IDerivatives);
      instance.SetDestructor(&destruct_IDerivatives);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IDerivatives*)
   {
      return GenerateInitInstanceLocal((::IDerivatives*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::IDerivatives*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IDerivatives_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IDerivatives*)0x0)->GetClass();
      IDerivatives_TClassManip(theClass);
   return theClass;
   }

   static void IDerivatives_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *IMillepede_Dictionary();
   static void IMillepede_TClassManip(TClass*);
   static void delete_IMillepede(void *p);
   static void deleteArray_IMillepede(void *p);
   static void destruct_IMillepede(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IMillepede*)
   {
      ::IMillepede *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IMillepede));
      static ::ROOT::TGenericClassInfo 
         instance("IMillepede", "AlignmentInterfaces/IMillepede.h", 21,
                  typeid(::IMillepede), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &IMillepede_Dictionary, isa_proxy, 4,
                  sizeof(::IMillepede) );
      instance.SetDelete(&delete_IMillepede);
      instance.SetDeleteArray(&deleteArray_IMillepede);
      instance.SetDestructor(&destruct_IMillepede);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IMillepede*)
   {
      return GenerateInitInstanceLocal((::IMillepede*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::IMillepede*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IMillepede_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IMillepede*)0x0)->GetClass();
      IMillepede_TClassManip(theClass);
   return theClass;
   }

   static void IMillepede_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ICentipede_Dictionary();
   static void ICentipede_TClassManip(TClass*);
   static void delete_ICentipede(void *p);
   static void deleteArray_ICentipede(void *p);
   static void destruct_ICentipede(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ICentipede*)
   {
      ::ICentipede *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ICentipede));
      static ::ROOT::TGenericClassInfo 
         instance("ICentipede", "AlignmentInterfaces/ICentipede.h", 21,
                  typeid(::ICentipede), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ICentipede_Dictionary, isa_proxy, 4,
                  sizeof(::ICentipede) );
      instance.SetDelete(&delete_ICentipede);
      instance.SetDeleteArray(&deleteArray_ICentipede);
      instance.SetDestructor(&destruct_ICentipede);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ICentipede*)
   {
      return GenerateInitInstanceLocal((::ICentipede*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ICentipede*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ICentipede_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ICentipede*)0x0)->GetClass();
      ICentipede_TClassManip(theClass);
   return theClass;
   }

   static void ICentipede_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *IWriteAlignmentConditionsTool_Dictionary();
   static void IWriteAlignmentConditionsTool_TClassManip(TClass*);
   static void delete_IWriteAlignmentConditionsTool(void *p);
   static void deleteArray_IWriteAlignmentConditionsTool(void *p);
   static void destruct_IWriteAlignmentConditionsTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IWriteAlignmentConditionsTool*)
   {
      ::IWriteAlignmentConditionsTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IWriteAlignmentConditionsTool));
      static ::ROOT::TGenericClassInfo 
         instance("IWriteAlignmentConditionsTool", "AlignmentInterfaces/IWriteAlignmentConditionsTool.h", 26,
                  typeid(::IWriteAlignmentConditionsTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &IWriteAlignmentConditionsTool_Dictionary, isa_proxy, 4,
                  sizeof(::IWriteAlignmentConditionsTool) );
      instance.SetDelete(&delete_IWriteAlignmentConditionsTool);
      instance.SetDeleteArray(&deleteArray_IWriteAlignmentConditionsTool);
      instance.SetDestructor(&destruct_IWriteAlignmentConditionsTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IWriteAlignmentConditionsTool*)
   {
      return GenerateInitInstanceLocal((::IWriteAlignmentConditionsTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::IWriteAlignmentConditionsTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IWriteAlignmentConditionsTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IWriteAlignmentConditionsTool*)0x0)->GetClass();
      IWriteAlignmentConditionsTool_TClassManip(theClass);
   return theClass;
   }

   static void IWriteAlignmentConditionsTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IAlignSolvTool(void *p) {
      delete ((::IAlignSolvTool*)p);
   }
   static void deleteArray_IAlignSolvTool(void *p) {
      delete [] ((::IAlignSolvTool*)p);
   }
   static void destruct_IAlignSolvTool(void *p) {
      typedef ::IAlignSolvTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IAlignSolvTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlcLcLIAlignUpdateTool(void *p) {
      delete ((::Al::IAlignUpdateTool*)p);
   }
   static void deleteArray_AlcLcLIAlignUpdateTool(void *p) {
      delete [] ((::Al::IAlignUpdateTool*)p);
   }
   static void destruct_AlcLcLIAlignUpdateTool(void *p) {
      typedef ::Al::IAlignUpdateTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Al::IAlignUpdateTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IATrackSelectorTool(void *p) {
      delete ((::IATrackSelectorTool*)p);
   }
   static void deleteArray_IATrackSelectorTool(void *p) {
      delete [] ((::IATrackSelectorTool*)p);
   }
   static void destruct_IATrackSelectorTool(void *p) {
      typedef ::IATrackSelectorTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IATrackSelectorTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ITAConfigTool(void *p) {
      delete ((::ITAConfigTool*)p);
   }
   static void deleteArray_ITAConfigTool(void *p) {
      delete [] ((::ITAConfigTool*)p);
   }
   static void destruct_ITAConfigTool(void *p) {
      typedef ::ITAConfigTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ITAConfigTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IDerivatives(void *p) {
      delete ((::IDerivatives*)p);
   }
   static void deleteArray_IDerivatives(void *p) {
      delete [] ((::IDerivatives*)p);
   }
   static void destruct_IDerivatives(void *p) {
      typedef ::IDerivatives current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IDerivatives

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IMillepede(void *p) {
      delete ((::IMillepede*)p);
   }
   static void deleteArray_IMillepede(void *p) {
      delete [] ((::IMillepede*)p);
   }
   static void destruct_IMillepede(void *p) {
      typedef ::IMillepede current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IMillepede

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ICentipede(void *p) {
      delete ((::ICentipede*)p);
   }
   static void deleteArray_ICentipede(void *p) {
      delete [] ((::ICentipede*)p);
   }
   static void destruct_ICentipede(void *p) {
      typedef ::ICentipede current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ICentipede

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IWriteAlignmentConditionsTool(void *p) {
      delete ((::IWriteAlignmentConditionsTool*)p);
   }
   static void deleteArray_IWriteAlignmentConditionsTool(void *p) {
      delete [] ((::IWriteAlignmentConditionsTool*)p);
   }
   static void destruct_IWriteAlignmentConditionsTool(void *p) {
      typedef ::IWriteAlignmentConditionsTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IWriteAlignmentConditionsTool

namespace {
  void TriggerDictionaryInitialization_AlignmentInterfacesDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignmentInterfaces",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignKernel",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Alignment/AlignmentInterfaces/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "AlignmentInterfacesDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/IAlignSolvTool.h")))  IAlignSolvTool;
namespace Al{class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/IAlignUpdateTool.h")))  IAlignUpdateTool;}
class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/IATrackSelectorTool.h")))  IATrackSelectorTool;
class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/IDerivatives.h")))  ITAConfigTool;
class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/IDerivatives.h")))  IDerivatives;
class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/IMillepede.h")))  IMillepede;
class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/ICentipede.h")))  ICentipede;
class __attribute__((annotate("$clingAutoload$AlignmentInterfaces/IWriteAlignmentConditionsTool.h")))  IWriteAlignmentConditionsTool;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "AlignmentInterfacesDict dictionary payload"
#ifdef __MINGW32__
  #undef __MINGW32__
#endif
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations AlignmentInterfaces_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "AlignmentInterfaces"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v3r7"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef DICT_ALIGNMENTINTERFACESDICT_H 
#define DICT_ALIGNMENTINTERFACESDICT_H  1

#include "AlignmentInterfaces/IAlignSolvTool.h"
#include "AlignmentInterfaces/IAlignUpdateTool.h"
#include "AlignmentInterfaces/IATrackSelectorTool.h"
#include "AlignmentInterfaces/IDerivatives.h"
#include "AlignmentInterfaces/ITAConfigTool.h"
#include "AlignmentInterfaces/IMillepede.h"
#include "AlignmentInterfaces/ICentipede.h"
#include "AlignmentInterfaces/IWriteAlignmentConditionsTool.h"

#endif // DICT_ALIGNMENTINTERFACESDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Al::IAlignUpdateTool", payloadCode, "@",
"IATrackSelectorTool", payloadCode, "@",
"IAlignSolvTool", payloadCode, "@",
"ICentipede", payloadCode, "@",
"IDerivatives", payloadCode, "@",
"IMillepede", payloadCode, "@",
"ITAConfigTool", payloadCode, "@",
"IWriteAlignmentConditionsTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("AlignmentInterfacesDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_AlignmentInterfacesDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_AlignmentInterfacesDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_AlignmentInterfacesDict() {
  TriggerDictionaryInitialization_AlignmentInterfacesDict_Impl();
}
