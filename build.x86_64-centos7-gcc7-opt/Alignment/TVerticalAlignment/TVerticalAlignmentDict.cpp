// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME TVerticalAlignmentDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/TVerticalAlignment/dict/TVerticalAlignmentDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment_Dictionary();
   static void AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment_TClassManip(TClass*);
   static void *new_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p = 0);
   static void *newArray_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(Long_t size, void *p);
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p);
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p);
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::TVerticalAlignment::TVerticalAlignment*)
   {
      ::Alignment::TVerticalAlignment::TVerticalAlignment *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::TVerticalAlignment::TVerticalAlignment));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::TVerticalAlignment::TVerticalAlignment", "TVerticalAlignment/TVerticalAlignment.h", 99,
                  typeid(::Alignment::TVerticalAlignment::TVerticalAlignment), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::TVerticalAlignment::TVerticalAlignment) );
      instance.SetNew(&new_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment);
      instance.SetNewArray(&newArray_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment);
      instance.SetDelete(&delete_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment);
      instance.SetDestructor(&destruct_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::TVerticalAlignment::TVerticalAlignment*)
   {
      return GenerateInitInstanceLocal((::Alignment::TVerticalAlignment::TVerticalAlignment*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::TVerticalAlignment*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::TVerticalAlignment*)0x0)->GetClass();
      AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff_Dictionary();
   static void AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff_TClassManip(TClass*);
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff(void *p);
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff(void *p);
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::TVerticalAlignment::OTYAlignMagOff*)
   {
      ::Alignment::TVerticalAlignment::OTYAlignMagOff *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::TVerticalAlignment::OTYAlignMagOff));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::TVerticalAlignment::OTYAlignMagOff", "TVerticalAlignment/OTYAlignMagOff.h", 91,
                  typeid(::Alignment::TVerticalAlignment::OTYAlignMagOff), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::TVerticalAlignment::OTYAlignMagOff) );
      instance.SetDelete(&delete_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff);
      instance.SetDestructor(&destruct_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::TVerticalAlignment::OTYAlignMagOff*)
   {
      return GenerateInitInstanceLocal((::Alignment::TVerticalAlignment::OTYAlignMagOff*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::OTYAlignMagOff*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::OTYAlignMagOff*)0x0)->GetClass();
      AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff_Dictionary();
   static void AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff_TClassManip(TClass*);
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff(void *p);
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff(void *p);
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::TVerticalAlignment::ITYAlignMagOff*)
   {
      ::Alignment::TVerticalAlignment::ITYAlignMagOff *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::TVerticalAlignment::ITYAlignMagOff));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::TVerticalAlignment::ITYAlignMagOff", "TVerticalAlignment/ITYAlignMagOff.h", 87,
                  typeid(::Alignment::TVerticalAlignment::ITYAlignMagOff), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::TVerticalAlignment::ITYAlignMagOff) );
      instance.SetDelete(&delete_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff);
      instance.SetDestructor(&destruct_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::TVerticalAlignment::ITYAlignMagOff*)
   {
      return GenerateInitInstanceLocal((::Alignment::TVerticalAlignment::ITYAlignMagOff*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::ITYAlignMagOff*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::ITYAlignMagOff*)0x0)->GetClass();
      AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff_Dictionary();
   static void AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff_TClassManip(TClass*);
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff(void *p);
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff(void *p);
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::TVerticalAlignment::TTYAlignMagOff*)
   {
      ::Alignment::TVerticalAlignment::TTYAlignMagOff *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::TVerticalAlignment::TTYAlignMagOff));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::TVerticalAlignment::TTYAlignMagOff", "TVerticalAlignment/TTYAlignMagOff.h", 88,
                  typeid(::Alignment::TVerticalAlignment::TTYAlignMagOff), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::TVerticalAlignment::TTYAlignMagOff) );
      instance.SetDelete(&delete_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff);
      instance.SetDestructor(&destruct_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::TVerticalAlignment::TTYAlignMagOff*)
   {
      return GenerateInitInstanceLocal((::Alignment::TVerticalAlignment::TTYAlignMagOff*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::TTYAlignMagOff*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::TVerticalAlignment::TTYAlignMagOff*)0x0)->GetClass();
      AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::TVerticalAlignment::TVerticalAlignment : new ::Alignment::TVerticalAlignment::TVerticalAlignment;
   }
   static void *newArray_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::TVerticalAlignment::TVerticalAlignment[nElements] : new ::Alignment::TVerticalAlignment::TVerticalAlignment[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p) {
      delete ((::Alignment::TVerticalAlignment::TVerticalAlignment*)p);
   }
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p) {
      delete [] ((::Alignment::TVerticalAlignment::TVerticalAlignment*)p);
   }
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLTVerticalAlignment(void *p) {
      typedef ::Alignment::TVerticalAlignment::TVerticalAlignment current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::TVerticalAlignment::TVerticalAlignment

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff(void *p) {
      delete ((::Alignment::TVerticalAlignment::OTYAlignMagOff*)p);
   }
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff(void *p) {
      delete [] ((::Alignment::TVerticalAlignment::OTYAlignMagOff*)p);
   }
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLOTYAlignMagOff(void *p) {
      typedef ::Alignment::TVerticalAlignment::OTYAlignMagOff current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::TVerticalAlignment::OTYAlignMagOff

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff(void *p) {
      delete ((::Alignment::TVerticalAlignment::ITYAlignMagOff*)p);
   }
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff(void *p) {
      delete [] ((::Alignment::TVerticalAlignment::ITYAlignMagOff*)p);
   }
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLITYAlignMagOff(void *p) {
      typedef ::Alignment::TVerticalAlignment::ITYAlignMagOff current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::TVerticalAlignment::ITYAlignMagOff

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff(void *p) {
      delete ((::Alignment::TVerticalAlignment::TTYAlignMagOff*)p);
   }
   static void deleteArray_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff(void *p) {
      delete [] ((::Alignment::TVerticalAlignment::TTYAlignMagOff*)p);
   }
   static void destruct_AlignmentcLcLTVerticalAlignmentcLcLTTYAlignMagOff(void *p) {
      typedef ::Alignment::TVerticalAlignment::TTYAlignMagOff current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::TVerticalAlignment::TTYAlignMagOff

namespace ROOT {
   static TClass *maplEstringcOstringgR_Dictionary();
   static void maplEstringcOstringgR_TClassManip(TClass*);
   static void *new_maplEstringcOstringgR(void *p = 0);
   static void *newArray_maplEstringcOstringgR(Long_t size, void *p);
   static void delete_maplEstringcOstringgR(void *p);
   static void deleteArray_maplEstringcOstringgR(void *p);
   static void destruct_maplEstringcOstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,string>*)
   {
      map<string,string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,string>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,string>", -2, "map", 99,
                  typeid(map<string,string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOstringgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,string>) );
      instance.SetNew(&new_maplEstringcOstringgR);
      instance.SetNewArray(&newArray_maplEstringcOstringgR);
      instance.SetDelete(&delete_maplEstringcOstringgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOstringgR);
      instance.SetDestructor(&destruct_maplEstringcOstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,string>*)0x0)->GetClass();
      maplEstringcOstringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string> : new map<string,string>;
   }
   static void *newArray_maplEstringcOstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string>[nElements] : new map<string,string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOstringgR(void *p) {
      delete ((map<string,string>*)p);
   }
   static void deleteArray_maplEstringcOstringgR(void *p) {
      delete [] ((map<string,string>*)p);
   }
   static void destruct_maplEstringcOstringgR(void *p) {
      typedef map<string,string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,string>

namespace ROOT {
   static TClass *maplEstringcOintgR_Dictionary();
   static void maplEstringcOintgR_TClassManip(TClass*);
   static void *new_maplEstringcOintgR(void *p = 0);
   static void *newArray_maplEstringcOintgR(Long_t size, void *p);
   static void delete_maplEstringcOintgR(void *p);
   static void deleteArray_maplEstringcOintgR(void *p);
   static void destruct_maplEstringcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,int>*)
   {
      map<string,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,int>", -2, "map", 99,
                  typeid(map<string,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOintgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,int>) );
      instance.SetNew(&new_maplEstringcOintgR);
      instance.SetNewArray(&newArray_maplEstringcOintgR);
      instance.SetDelete(&delete_maplEstringcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOintgR);
      instance.SetDestructor(&destruct_maplEstringcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,int>*)0x0)->GetClass();
      maplEstringcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,int> : new map<string,int>;
   }
   static void *newArray_maplEstringcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,int>[nElements] : new map<string,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOintgR(void *p) {
      delete ((map<string,int>*)p);
   }
   static void deleteArray_maplEstringcOintgR(void *p) {
      delete [] ((map<string,int>*)p);
   }
   static void destruct_maplEstringcOintgR(void *p) {
      typedef map<string,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,int>

namespace ROOT {
   static TClass *maplEstringcOdoublegR_Dictionary();
   static void maplEstringcOdoublegR_TClassManip(TClass*);
   static void *new_maplEstringcOdoublegR(void *p = 0);
   static void *newArray_maplEstringcOdoublegR(Long_t size, void *p);
   static void delete_maplEstringcOdoublegR(void *p);
   static void deleteArray_maplEstringcOdoublegR(void *p);
   static void destruct_maplEstringcOdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,double>*)
   {
      map<string,double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,double>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,double>", -2, "map", 99,
                  typeid(map<string,double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,double>) );
      instance.SetNew(&new_maplEstringcOdoublegR);
      instance.SetNewArray(&newArray_maplEstringcOdoublegR);
      instance.SetDelete(&delete_maplEstringcOdoublegR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOdoublegR);
      instance.SetDestructor(&destruct_maplEstringcOdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,double>*)0x0)->GetClass();
      maplEstringcOdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double> : new map<string,double>;
   }
   static void *newArray_maplEstringcOdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double>[nElements] : new map<string,double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOdoublegR(void *p) {
      delete ((map<string,double>*)p);
   }
   static void deleteArray_maplEstringcOdoublegR(void *p) {
      delete [] ((map<string,double>*)p);
   }
   static void destruct_maplEstringcOdoublegR(void *p) {
      typedef map<string,double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,double>

namespace ROOT {
   static TClass *maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR_Dictionary();
   static void maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p);
   static void deleteArray_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p);
   static void destruct_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >*)
   {
      map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >", -2, "map", 99,
                  typeid(map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >) );
      instance.SetNew(&new_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR);
      instance.SetDelete(&delete_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >*)0x0)->GetClass();
      maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> > : new map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >;
   }
   static void *newArray_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >[nElements] : new map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p) {
      delete ((map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >*)p);
   }
   static void deleteArray_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p) {
      delete [] ((map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >*)p);
   }
   static void destruct_maplEstringcOboostcLcLvariantlEAlignmentcLcLTVerticalAlignmentcLcLOTParamcOAlignmentcLcLTVerticalAlignmentcLcLSTParamgRsPgR(void *p) {
      typedef map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,boost::variant<Alignment::TVerticalAlignment::OTParam,Alignment::TVerticalAlignment::STParam> >

namespace ROOT {
   static TClass *maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR_Dictionary();
   static void maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR_TClassManip(TClass*);
   static void *new_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p = 0);
   static void *newArray_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(Long_t size, void *p);
   static void delete_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p);
   static void deleteArray_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p);
   static void destruct_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,basic_ostream<char,char_traits<char> >*>*)
   {
      map<string,basic_ostream<char,char_traits<char> >*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,basic_ostream<char,char_traits<char> >*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,basic_ostream<char,char_traits<char> >*>", -2, "map", 99,
                  typeid(map<string,basic_ostream<char,char_traits<char> >*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,basic_ostream<char,char_traits<char> >*>) );
      instance.SetNew(&new_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR);
      instance.SetNewArray(&newArray_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR);
      instance.SetDelete(&delete_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR);
      instance.SetDestructor(&destruct_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,basic_ostream<char,char_traits<char> >*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,basic_ostream<char,char_traits<char> >*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,basic_ostream<char,char_traits<char> >*>*)0x0)->GetClass();
      maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,basic_ostream<char,char_traits<char> >*> : new map<string,basic_ostream<char,char_traits<char> >*>;
   }
   static void *newArray_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,basic_ostream<char,char_traits<char> >*>[nElements] : new map<string,basic_ostream<char,char_traits<char> >*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p) {
      delete ((map<string,basic_ostream<char,char_traits<char> >*>*)p);
   }
   static void deleteArray_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p) {
      delete [] ((map<string,basic_ostream<char,char_traits<char> >*>*)p);
   }
   static void destruct_maplEstringcObasic_ostreamlEcharcOchar_traitslEchargRsPgRmUgR(void *p) {
      typedef map<string,basic_ostream<char,char_traits<char> >*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,basic_ostream<char,char_traits<char> >*>

namespace ROOT {
   static TClass *maplEstringcOSTNamescLcLSectNamesgR_Dictionary();
   static void maplEstringcOSTNamescLcLSectNamesgR_TClassManip(TClass*);
   static void *new_maplEstringcOSTNamescLcLSectNamesgR(void *p = 0);
   static void *newArray_maplEstringcOSTNamescLcLSectNamesgR(Long_t size, void *p);
   static void delete_maplEstringcOSTNamescLcLSectNamesgR(void *p);
   static void deleteArray_maplEstringcOSTNamescLcLSectNamesgR(void *p);
   static void destruct_maplEstringcOSTNamescLcLSectNamesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,STNames::SectNames>*)
   {
      map<string,STNames::SectNames> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,STNames::SectNames>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,STNames::SectNames>", -2, "map", 99,
                  typeid(map<string,STNames::SectNames>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOSTNamescLcLSectNamesgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,STNames::SectNames>) );
      instance.SetNew(&new_maplEstringcOSTNamescLcLSectNamesgR);
      instance.SetNewArray(&newArray_maplEstringcOSTNamescLcLSectNamesgR);
      instance.SetDelete(&delete_maplEstringcOSTNamescLcLSectNamesgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOSTNamescLcLSectNamesgR);
      instance.SetDestructor(&destruct_maplEstringcOSTNamescLcLSectNamesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,STNames::SectNames> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,STNames::SectNames>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOSTNamescLcLSectNamesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,STNames::SectNames>*)0x0)->GetClass();
      maplEstringcOSTNamescLcLSectNamesgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOSTNamescLcLSectNamesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOSTNamescLcLSectNamesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::SectNames> : new map<string,STNames::SectNames>;
   }
   static void *newArray_maplEstringcOSTNamescLcLSectNamesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::SectNames>[nElements] : new map<string,STNames::SectNames>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOSTNamescLcLSectNamesgR(void *p) {
      delete ((map<string,STNames::SectNames>*)p);
   }
   static void deleteArray_maplEstringcOSTNamescLcLSectNamesgR(void *p) {
      delete [] ((map<string,STNames::SectNames>*)p);
   }
   static void destruct_maplEstringcOSTNamescLcLSectNamesgR(void *p) {
      typedef map<string,STNames::SectNames> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,STNames::SectNames>

namespace ROOT {
   static TClass *maplEstringcOSTNamescLcLLayerNamesgR_Dictionary();
   static void maplEstringcOSTNamescLcLLayerNamesgR_TClassManip(TClass*);
   static void *new_maplEstringcOSTNamescLcLLayerNamesgR(void *p = 0);
   static void *newArray_maplEstringcOSTNamescLcLLayerNamesgR(Long_t size, void *p);
   static void delete_maplEstringcOSTNamescLcLLayerNamesgR(void *p);
   static void deleteArray_maplEstringcOSTNamescLcLLayerNamesgR(void *p);
   static void destruct_maplEstringcOSTNamescLcLLayerNamesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,STNames::LayerNames>*)
   {
      map<string,STNames::LayerNames> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,STNames::LayerNames>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,STNames::LayerNames>", -2, "map", 99,
                  typeid(map<string,STNames::LayerNames>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOSTNamescLcLLayerNamesgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,STNames::LayerNames>) );
      instance.SetNew(&new_maplEstringcOSTNamescLcLLayerNamesgR);
      instance.SetNewArray(&newArray_maplEstringcOSTNamescLcLLayerNamesgR);
      instance.SetDelete(&delete_maplEstringcOSTNamescLcLLayerNamesgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOSTNamescLcLLayerNamesgR);
      instance.SetDestructor(&destruct_maplEstringcOSTNamescLcLLayerNamesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,STNames::LayerNames> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,STNames::LayerNames>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOSTNamescLcLLayerNamesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,STNames::LayerNames>*)0x0)->GetClass();
      maplEstringcOSTNamescLcLLayerNamesgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOSTNamescLcLLayerNamesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOSTNamescLcLLayerNamesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::LayerNames> : new map<string,STNames::LayerNames>;
   }
   static void *newArray_maplEstringcOSTNamescLcLLayerNamesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::LayerNames>[nElements] : new map<string,STNames::LayerNames>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOSTNamescLcLLayerNamesgR(void *p) {
      delete ((map<string,STNames::LayerNames>*)p);
   }
   static void deleteArray_maplEstringcOSTNamescLcLLayerNamesgR(void *p) {
      delete [] ((map<string,STNames::LayerNames>*)p);
   }
   static void destruct_maplEstringcOSTNamescLcLLayerNamesgR(void *p) {
      typedef map<string,STNames::LayerNames> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,STNames::LayerNames>

namespace ROOT {
   static TClass *maplEstringcOSTNamescLcLBoxNamesgR_Dictionary();
   static void maplEstringcOSTNamescLcLBoxNamesgR_TClassManip(TClass*);
   static void *new_maplEstringcOSTNamescLcLBoxNamesgR(void *p = 0);
   static void *newArray_maplEstringcOSTNamescLcLBoxNamesgR(Long_t size, void *p);
   static void delete_maplEstringcOSTNamescLcLBoxNamesgR(void *p);
   static void deleteArray_maplEstringcOSTNamescLcLBoxNamesgR(void *p);
   static void destruct_maplEstringcOSTNamescLcLBoxNamesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,STNames::BoxNames>*)
   {
      map<string,STNames::BoxNames> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,STNames::BoxNames>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,STNames::BoxNames>", -2, "map", 99,
                  typeid(map<string,STNames::BoxNames>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOSTNamescLcLBoxNamesgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,STNames::BoxNames>) );
      instance.SetNew(&new_maplEstringcOSTNamescLcLBoxNamesgR);
      instance.SetNewArray(&newArray_maplEstringcOSTNamescLcLBoxNamesgR);
      instance.SetDelete(&delete_maplEstringcOSTNamescLcLBoxNamesgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOSTNamescLcLBoxNamesgR);
      instance.SetDestructor(&destruct_maplEstringcOSTNamescLcLBoxNamesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,STNames::BoxNames> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,STNames::BoxNames>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOSTNamescLcLBoxNamesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,STNames::BoxNames>*)0x0)->GetClass();
      maplEstringcOSTNamescLcLBoxNamesgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOSTNamescLcLBoxNamesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOSTNamescLcLBoxNamesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::BoxNames> : new map<string,STNames::BoxNames>;
   }
   static void *newArray_maplEstringcOSTNamescLcLBoxNamesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::BoxNames>[nElements] : new map<string,STNames::BoxNames>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOSTNamescLcLBoxNamesgR(void *p) {
      delete ((map<string,STNames::BoxNames>*)p);
   }
   static void deleteArray_maplEstringcOSTNamescLcLBoxNamesgR(void *p) {
      delete [] ((map<string,STNames::BoxNames>*)p);
   }
   static void destruct_maplEstringcOSTNamescLcLBoxNamesgR(void *p) {
      typedef map<string,STNames::BoxNames> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,STNames::BoxNames>

namespace ROOT {
   static TClass *maplEstringcOSTNamescLcLBiLayerNamesgR_Dictionary();
   static void maplEstringcOSTNamescLcLBiLayerNamesgR_TClassManip(TClass*);
   static void *new_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p = 0);
   static void *newArray_maplEstringcOSTNamescLcLBiLayerNamesgR(Long_t size, void *p);
   static void delete_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p);
   static void deleteArray_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p);
   static void destruct_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,STNames::BiLayerNames>*)
   {
      map<string,STNames::BiLayerNames> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,STNames::BiLayerNames>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,STNames::BiLayerNames>", -2, "map", 99,
                  typeid(map<string,STNames::BiLayerNames>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOSTNamescLcLBiLayerNamesgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,STNames::BiLayerNames>) );
      instance.SetNew(&new_maplEstringcOSTNamescLcLBiLayerNamesgR);
      instance.SetNewArray(&newArray_maplEstringcOSTNamescLcLBiLayerNamesgR);
      instance.SetDelete(&delete_maplEstringcOSTNamescLcLBiLayerNamesgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOSTNamescLcLBiLayerNamesgR);
      instance.SetDestructor(&destruct_maplEstringcOSTNamescLcLBiLayerNamesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,STNames::BiLayerNames> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,STNames::BiLayerNames>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOSTNamescLcLBiLayerNamesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,STNames::BiLayerNames>*)0x0)->GetClass();
      maplEstringcOSTNamescLcLBiLayerNamesgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOSTNamescLcLBiLayerNamesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::BiLayerNames> : new map<string,STNames::BiLayerNames>;
   }
   static void *newArray_maplEstringcOSTNamescLcLBiLayerNamesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,STNames::BiLayerNames>[nElements] : new map<string,STNames::BiLayerNames>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p) {
      delete ((map<string,STNames::BiLayerNames>*)p);
   }
   static void deleteArray_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p) {
      delete [] ((map<string,STNames::BiLayerNames>*)p);
   }
   static void destruct_maplEstringcOSTNamescLcLBiLayerNamesgR(void *p) {
      typedef map<string,STNames::BiLayerNames> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,STNames::BiLayerNames>

namespace ROOT {
   static TClass *maplEstringcORooSharedPropertiesmUgR_Dictionary();
   static void maplEstringcORooSharedPropertiesmUgR_TClassManip(TClass*);
   static void *new_maplEstringcORooSharedPropertiesmUgR(void *p = 0);
   static void *newArray_maplEstringcORooSharedPropertiesmUgR(Long_t size, void *p);
   static void delete_maplEstringcORooSharedPropertiesmUgR(void *p);
   static void deleteArray_maplEstringcORooSharedPropertiesmUgR(void *p);
   static void destruct_maplEstringcORooSharedPropertiesmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,RooSharedProperties*>*)
   {
      map<string,RooSharedProperties*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,RooSharedProperties*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,RooSharedProperties*>", -2, "map", 99,
                  typeid(map<string,RooSharedProperties*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcORooSharedPropertiesmUgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,RooSharedProperties*>) );
      instance.SetNew(&new_maplEstringcORooSharedPropertiesmUgR);
      instance.SetNewArray(&newArray_maplEstringcORooSharedPropertiesmUgR);
      instance.SetDelete(&delete_maplEstringcORooSharedPropertiesmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcORooSharedPropertiesmUgR);
      instance.SetDestructor(&destruct_maplEstringcORooSharedPropertiesmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,RooSharedProperties*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,RooSharedProperties*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcORooSharedPropertiesmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,RooSharedProperties*>*)0x0)->GetClass();
      maplEstringcORooSharedPropertiesmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcORooSharedPropertiesmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcORooSharedPropertiesmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,RooSharedProperties*> : new map<string,RooSharedProperties*>;
   }
   static void *newArray_maplEstringcORooSharedPropertiesmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,RooSharedProperties*>[nElements] : new map<string,RooSharedProperties*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcORooSharedPropertiesmUgR(void *p) {
      delete ((map<string,RooSharedProperties*>*)p);
   }
   static void deleteArray_maplEstringcORooSharedPropertiesmUgR(void *p) {
      delete [] ((map<string,RooSharedProperties*>*)p);
   }
   static void destruct_maplEstringcORooSharedPropertiesmUgR(void *p) {
      typedef map<string,RooSharedProperties*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,RooSharedProperties*>

namespace ROOT {
   static TClass *maplEstringcORooAbsDatamUgR_Dictionary();
   static void maplEstringcORooAbsDatamUgR_TClassManip(TClass*);
   static void *new_maplEstringcORooAbsDatamUgR(void *p = 0);
   static void *newArray_maplEstringcORooAbsDatamUgR(Long_t size, void *p);
   static void delete_maplEstringcORooAbsDatamUgR(void *p);
   static void deleteArray_maplEstringcORooAbsDatamUgR(void *p);
   static void destruct_maplEstringcORooAbsDatamUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,RooAbsData*>*)
   {
      map<string,RooAbsData*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,RooAbsData*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,RooAbsData*>", -2, "map", 99,
                  typeid(map<string,RooAbsData*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcORooAbsDatamUgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,RooAbsData*>) );
      instance.SetNew(&new_maplEstringcORooAbsDatamUgR);
      instance.SetNewArray(&newArray_maplEstringcORooAbsDatamUgR);
      instance.SetDelete(&delete_maplEstringcORooAbsDatamUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcORooAbsDatamUgR);
      instance.SetDestructor(&destruct_maplEstringcORooAbsDatamUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,RooAbsData*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,RooAbsData*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcORooAbsDatamUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,RooAbsData*>*)0x0)->GetClass();
      maplEstringcORooAbsDatamUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcORooAbsDatamUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcORooAbsDatamUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,RooAbsData*> : new map<string,RooAbsData*>;
   }
   static void *newArray_maplEstringcORooAbsDatamUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,RooAbsData*>[nElements] : new map<string,RooAbsData*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcORooAbsDatamUgR(void *p) {
      delete ((map<string,RooAbsData*>*)p);
   }
   static void deleteArray_maplEstringcORooAbsDatamUgR(void *p) {
      delete [] ((map<string,RooAbsData*>*)p);
   }
   static void destruct_maplEstringcORooAbsDatamUgR(void *p) {
      typedef map<string,RooAbsData*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,RooAbsData*>

namespace ROOT {
   static TClass *maplEstringcOOTNamescLcLStationNamesgR_Dictionary();
   static void maplEstringcOOTNamescLcLStationNamesgR_TClassManip(TClass*);
   static void *new_maplEstringcOOTNamescLcLStationNamesgR(void *p = 0);
   static void *newArray_maplEstringcOOTNamescLcLStationNamesgR(Long_t size, void *p);
   static void delete_maplEstringcOOTNamescLcLStationNamesgR(void *p);
   static void deleteArray_maplEstringcOOTNamescLcLStationNamesgR(void *p);
   static void destruct_maplEstringcOOTNamescLcLStationNamesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,OTNames::StationNames>*)
   {
      map<string,OTNames::StationNames> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,OTNames::StationNames>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,OTNames::StationNames>", -2, "map", 99,
                  typeid(map<string,OTNames::StationNames>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOOTNamescLcLStationNamesgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,OTNames::StationNames>) );
      instance.SetNew(&new_maplEstringcOOTNamescLcLStationNamesgR);
      instance.SetNewArray(&newArray_maplEstringcOOTNamescLcLStationNamesgR);
      instance.SetDelete(&delete_maplEstringcOOTNamescLcLStationNamesgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOOTNamescLcLStationNamesgR);
      instance.SetDestructor(&destruct_maplEstringcOOTNamescLcLStationNamesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,OTNames::StationNames> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,OTNames::StationNames>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOOTNamescLcLStationNamesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,OTNames::StationNames>*)0x0)->GetClass();
      maplEstringcOOTNamescLcLStationNamesgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOOTNamescLcLStationNamesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOOTNamescLcLStationNamesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,OTNames::StationNames> : new map<string,OTNames::StationNames>;
   }
   static void *newArray_maplEstringcOOTNamescLcLStationNamesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,OTNames::StationNames>[nElements] : new map<string,OTNames::StationNames>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOOTNamescLcLStationNamesgR(void *p) {
      delete ((map<string,OTNames::StationNames>*)p);
   }
   static void deleteArray_maplEstringcOOTNamescLcLStationNamesgR(void *p) {
      delete [] ((map<string,OTNames::StationNames>*)p);
   }
   static void destruct_maplEstringcOOTNamescLcLStationNamesgR(void *p) {
      typedef map<string,OTNames::StationNames> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,OTNames::StationNames>

namespace ROOT {
   static TClass *maplEstringcOOTNamescLcLModuleNamesgR_Dictionary();
   static void maplEstringcOOTNamescLcLModuleNamesgR_TClassManip(TClass*);
   static void *new_maplEstringcOOTNamescLcLModuleNamesgR(void *p = 0);
   static void *newArray_maplEstringcOOTNamescLcLModuleNamesgR(Long_t size, void *p);
   static void delete_maplEstringcOOTNamescLcLModuleNamesgR(void *p);
   static void deleteArray_maplEstringcOOTNamescLcLModuleNamesgR(void *p);
   static void destruct_maplEstringcOOTNamescLcLModuleNamesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,OTNames::ModuleNames>*)
   {
      map<string,OTNames::ModuleNames> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,OTNames::ModuleNames>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,OTNames::ModuleNames>", -2, "map", 99,
                  typeid(map<string,OTNames::ModuleNames>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOOTNamescLcLModuleNamesgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,OTNames::ModuleNames>) );
      instance.SetNew(&new_maplEstringcOOTNamescLcLModuleNamesgR);
      instance.SetNewArray(&newArray_maplEstringcOOTNamescLcLModuleNamesgR);
      instance.SetDelete(&delete_maplEstringcOOTNamescLcLModuleNamesgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOOTNamescLcLModuleNamesgR);
      instance.SetDestructor(&destruct_maplEstringcOOTNamescLcLModuleNamesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,OTNames::ModuleNames> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,OTNames::ModuleNames>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOOTNamescLcLModuleNamesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,OTNames::ModuleNames>*)0x0)->GetClass();
      maplEstringcOOTNamescLcLModuleNamesgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOOTNamescLcLModuleNamesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOOTNamescLcLModuleNamesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,OTNames::ModuleNames> : new map<string,OTNames::ModuleNames>;
   }
   static void *newArray_maplEstringcOOTNamescLcLModuleNamesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,OTNames::ModuleNames>[nElements] : new map<string,OTNames::ModuleNames>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOOTNamescLcLModuleNamesgR(void *p) {
      delete ((map<string,OTNames::ModuleNames>*)p);
   }
   static void deleteArray_maplEstringcOOTNamescLcLModuleNamesgR(void *p) {
      delete [] ((map<string,OTNames::ModuleNames>*)p);
   }
   static void destruct_maplEstringcOOTNamescLcLModuleNamesgR(void *p) {
      typedef map<string,OTNames::ModuleNames> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,OTNames::ModuleNames>

namespace ROOT {
   static TClass *maplEstringcOOTNamescLcLLayerNamesgR_Dictionary();
   static void maplEstringcOOTNamescLcLLayerNamesgR_TClassManip(TClass*);
   static void *new_maplEstringcOOTNamescLcLLayerNamesgR(void *p = 0);
   static void *newArray_maplEstringcOOTNamescLcLLayerNamesgR(Long_t size, void *p);
   static void delete_maplEstringcOOTNamescLcLLayerNamesgR(void *p);
   static void deleteArray_maplEstringcOOTNamescLcLLayerNamesgR(void *p);
   static void destruct_maplEstringcOOTNamescLcLLayerNamesgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,OTNames::LayerNames>*)
   {
      map<string,OTNames::LayerNames> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,OTNames::LayerNames>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,OTNames::LayerNames>", -2, "map", 99,
                  typeid(map<string,OTNames::LayerNames>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOOTNamescLcLLayerNamesgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,OTNames::LayerNames>) );
      instance.SetNew(&new_maplEstringcOOTNamescLcLLayerNamesgR);
      instance.SetNewArray(&newArray_maplEstringcOOTNamescLcLLayerNamesgR);
      instance.SetDelete(&delete_maplEstringcOOTNamescLcLLayerNamesgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOOTNamescLcLLayerNamesgR);
      instance.SetDestructor(&destruct_maplEstringcOOTNamescLcLLayerNamesgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,OTNames::LayerNames> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,OTNames::LayerNames>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOOTNamescLcLLayerNamesgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,OTNames::LayerNames>*)0x0)->GetClass();
      maplEstringcOOTNamescLcLLayerNamesgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOOTNamescLcLLayerNamesgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOOTNamescLcLLayerNamesgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,OTNames::LayerNames> : new map<string,OTNames::LayerNames>;
   }
   static void *newArray_maplEstringcOOTNamescLcLLayerNamesgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,OTNames::LayerNames>[nElements] : new map<string,OTNames::LayerNames>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOOTNamescLcLLayerNamesgR(void *p) {
      delete ((map<string,OTNames::LayerNames>*)p);
   }
   static void deleteArray_maplEstringcOOTNamescLcLLayerNamesgR(void *p) {
      delete [] ((map<string,OTNames::LayerNames>*)p);
   }
   static void destruct_maplEstringcOOTNamescLcLLayerNamesgR(void *p) {
      typedef map<string,OTNames::LayerNames> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,OTNames::LayerNames>

namespace {
  void TriggerDictionaryInitialization_TVerticalAlignmentDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/TVerticalAlignment",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Python/2.7.13/x86_64-centos7-gcc7-opt/include/python2.7",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Alignment/TVerticalAlignment/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "TVerticalAlignmentDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace std{template <class _CharT> struct __attribute__((annotate("$clingAutoload$bits/char_traits.h")))  __attribute__((annotate("$clingAutoload$string")))  char_traits;
}
namespace std{template <typename > class __attribute__((annotate("$clingAutoload$bits/memoryfwd.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace Alignment{namespace TVerticalAlignment{class __attribute__((annotate("$clingAutoload$TVerticalAlignment/TVerticalAlignment.h")))  TVerticalAlignment;}}
namespace Alignment{namespace TVerticalAlignment{class __attribute__((annotate("$clingAutoload$TVerticalAlignment/OTYAlignMagOff.h")))  OTYAlignMagOff;}}
namespace Alignment{namespace TVerticalAlignment{class __attribute__((annotate("$clingAutoload$TVerticalAlignment/ITYAlignMagOff.h")))  ITYAlignMagOff;}}
namespace Alignment{namespace TVerticalAlignment{class __attribute__((annotate("$clingAutoload$TVerticalAlignment/TTYAlignMagOff.h")))  TTYAlignMagOff;}}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "TVerticalAlignmentDict dictionary payload"
#ifdef __MINGW32__
  #undef __MINGW32__
#endif
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations TVerticalAlignment_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "TVerticalAlignment"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v1r1"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef DICT_TVERTICALALIGNMENTDICT_H 
#define DICT_TVERTICALALIGNMENTDICT_H 1

// Include files
#include "TVerticalAlignment/TVerticalAlignment.h"
#include "TVerticalAlignment/OTYAlignMagOff.h"
#include "TVerticalAlignment/ITYAlignMagOff.h"
#include "TVerticalAlignment/TTYAlignMagOff.h"

#endif // DICT_TVERTICALALIGNMENTDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Alignment::TVerticalAlignment::ITYAlignMagOff", payloadCode, "@",
"Alignment::TVerticalAlignment::OTYAlignMagOff", payloadCode, "@",
"Alignment::TVerticalAlignment::TTYAlignMagOff", payloadCode, "@",
"Alignment::TVerticalAlignment::TVerticalAlignment", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TVerticalAlignmentDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TVerticalAlignmentDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TVerticalAlignmentDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TVerticalAlignmentDict() {
  TriggerDictionaryInitialization_TVerticalAlignmentDict_Impl();
}
