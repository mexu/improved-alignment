#Mon Jun 15 10:14:12 2020"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.Proxy.Configurable import *

class AlignSensors( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'HistoProduce' : True, # bool
    'HistoPrint' : False, # bool
    'HistoCountersPrint' : True, # bool
    'HistoCheckForNaN' : True, # bool
    'HistoSplitDir' : False, # bool
    'HistoOffSet' : 0, # int
    'HistoTopDir' : '', # str
    'HistoDir' : 'DefaultName', # str
    'FullDetail' : False, # bool
    'MonitorHistograms' : True, # bool
    'FormatFor1DHistoTable' : '| %2$-45.45s | %3$=7d |%8$11.5g | %10$-11.5g|%12$11.5g |%14$11.5g |', # str
    'ShortFormatFor1DHistoTable' : ' | %1$-25.25s %2%', # str
    'HeaderFor1DHistoTable' : '|   Title                                       |    #    |     Mean   |    RMS     |  Skewness  |  Kurtosis  |', # str
    'UseSequencialNumericAutoIDs' : False, # bool
    'AutoStringIDPurgeMap' : { '/' : '=SLASH=' }, # list
    'NTupleProduce' : True, # bool
    'NTuplePrint' : True, # bool
    'NTupleSplitDir' : False, # bool
    'NTupleOffSet' : 0, # int
    'NTupleLUN' : 'FILE1', # str
    'NTupleTopDir' : '', # str
    'NTupleDir' : 'DefaultName', # str
    'EvtColsProduce' : False, # bool
    'EvtColsPrint' : False, # bool
    'EvtColSplitDir' : False, # bool
    'EvtColOffSet' : 0, # int
    'EvtColLUN' : 'EVTCOL', # str
    'EvtColTopDir' : '', # str
    'EvtColDir' : 'DefaultName', # str
    'PrintInfo' : False, # bool
    'SelectedEvent' : 1, # int
    'TrackLocation' : 'Rec/Track/Velo', # str
    'OutputNtuple' : False, # bool
    'TestSensors' : [  ], # list
    'Fitter' : 'TrackMasterFitter', # str
    'MinSenR' : 4, # int
    'MinSenPhi' : 4, # int
    'MinModule' : 4, # int
    'KalmanResidual' : False, # bool
    'MinNResiduals' : 0, # int
    'ACDC' : True, # bool
    'MinDeltaSig' : 1.0000000, # float
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'HistoProduce' : """ Switch on/off the production of histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HistoPrint' : """ Switch on/off the printout of histograms at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCountersPrint' : """ Switch on/off the printout of histogram counters at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCheckForNaN' : """ Switch on/off the checks for NaN and Infinity for histogram fill [GaudiHistos<GaudiAlgorithm>] """,
    'HistoSplitDir' : """ Split long directory names into short pieces (suitable for HBOOK) [GaudiHistos<GaudiAlgorithm>] """,
    'HistoOffSet' : """ OffSet for automatically assigned histogram numerical identifiers  [GaudiHistos<GaudiAlgorithm>] """,
    'HistoTopDir' : """ Top level histogram directory (take care that it ends with '/') [GaudiHistos<GaudiAlgorithm>] """,
    'HistoDir' : """ Histogram Directory [GaudiHistos<GaudiAlgorithm>] """,
    'FullDetail' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'MonitorHistograms' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'FormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'ShortFormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HeaderFor1DHistoTable' : """ The table header for printout of 1D histograms  [GaudiHistos<GaudiAlgorithm>] """,
    'UseSequencialNumericAutoIDs' : """ Flag to allow users to switch back to the old style of creating numerical automatic IDs [GaudiHistos<GaudiAlgorithm>] """,
    'AutoStringIDPurgeMap' : """ Map of strings to search and replace when using the title as the basis of automatically generated literal IDs [GaudiHistos<GaudiAlgorithm>] """,
    'NTupleProduce' : """ general switch to enable/disable N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTuplePrint' : """ print N-tuple statistics [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleSplitDir' : """ split long directory names into short pieces (suitable for HBOOK) [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleLUN' : """ Logical File Unit for N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleTopDir' : """ top-level directory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleDir' : """ subdirectory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsProduce' : """ general switch to enable/disable Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsPrint' : """ print statistics for Event Tag Collections  [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColSplitDir' : """ split long directory names into short pieces [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColLUN' : """ Logical File Unit for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColTopDir' : """ Top-level directory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColDir' : """ Subdirectory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(AlignSensors, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'VeloAlignment'
  def getType( self ):
      return 'AlignSensors'
  pass # class AlignSensors

class TrackStore( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'NonzerMin' : 13418256, # int
    'TakeOverlap' : True, # bool
    'NonzerOverlap' : 0, # int
    'XOverlapCut' : 1.9353007e+228, # float
    'MissedHits' : True, # bool
    'ACDC' : True, # bool
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TrackStore, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'VeloAlignment'
  def getType( self ):
      return 'TrackStore'
  pass # class TrackStore

class VAlign( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'HistoProduce' : True, # bool
    'HistoPrint' : False, # bool
    'HistoCountersPrint' : True, # bool
    'HistoCheckForNaN' : True, # bool
    'HistoSplitDir' : False, # bool
    'HistoOffSet' : 0, # int
    'HistoTopDir' : '', # str
    'HistoDir' : 'DefaultName', # str
    'FullDetail' : False, # bool
    'MonitorHistograms' : True, # bool
    'FormatFor1DHistoTable' : '| %2$-45.45s | %3$=7d |%8$11.5g | %10$-11.5g|%12$11.5g |%14$11.5g |', # str
    'ShortFormatFor1DHistoTable' : ' | %1$-25.25s %2%', # str
    'HeaderFor1DHistoTable' : '|   Title                                       |    #    |     Mean   |    RMS     |  Skewness  |  Kurtosis  |', # str
    'UseSequencialNumericAutoIDs' : False, # bool
    'AutoStringIDPurgeMap' : { '/' : '=SLASH=' }, # list
    'NTupleProduce' : True, # bool
    'NTuplePrint' : True, # bool
    'NTupleSplitDir' : False, # bool
    'NTupleOffSet' : 0, # int
    'NTupleLUN' : 'FILE1', # str
    'NTupleTopDir' : '', # str
    'NTupleDir' : 'DefaultName', # str
    'EvtColsProduce' : False, # bool
    'EvtColsPrint' : False, # bool
    'EvtColSplitDir' : False, # bool
    'EvtColOffSet' : 0, # int
    'EvtColLUN' : 'EVTCOL', # str
    'EvtColTopDir' : '', # str
    'EvtColDir' : 'DefaultName', # str
    'Internal_Alignment' : True, # bool
    'Internal_Mod_Left' : [ True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True ], # list
    'Internal_Mod_Right' : [ True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True , True ], # list
    'Internal_DOF' : [ True , True , True , True , True , True ], # list
    'Internal_EQs' : [ True , True , True , True , True , True , True , True , True ], # list
    'Box_DOF' : [ True , True , True , True , True , True ], # list
    'Internal_PTerms' : [ 0.010000000 , 0.010000000 , 0.0050000000 , 0.00050000000 , 0.00050000000 , 0.00020000000 ], # list
    'Internal_Residual_Cut' : [ 0.30000000 , 0.060000000 ], # list
    'Box_PTerms' : [ 10.000000 , 10.000000 , 10.000000 , 0.030000000 , 0.030000000 , 0.030000000 ], # list
    'Box_EQs' : [ True , True , True , True , True , False ], # list
    'Box_Residual_Cut' : [ 0.50000000 , 0.40000000 ], # list
    'Box_Alignment' : True, # bool
    'Box_VELOopen' : False, # bool
    'Box_MinTracks_perPV' : 10, # int
    'General_Startfactor' : 100.00000, # float
    'General_Maxtracks' : 100, # int
    'Monitor_Constants' : False, # bool
    'Monitor_PV' : False, # bool
    'Monitor_Overlaps' : False, # bool
    'Monitor_Tracks' : False, # bool
    'Monitor_Tracks_num' : 50, # int
    'Monitor_Events' : False, # bool
    'TrackStoreTool' : 'TrackStore', # str
    'MillepedeTool' : 'Millepede', # str
    'TrackContainer' : 'Rec/Track/Velo', # str
    'MinAdcPerTrack' : 0.00000, # float
    'MaxTheta' : 4.0000000, # float
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'HistoProduce' : """ Switch on/off the production of histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HistoPrint' : """ Switch on/off the printout of histograms at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCountersPrint' : """ Switch on/off the printout of histogram counters at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCheckForNaN' : """ Switch on/off the checks for NaN and Infinity for histogram fill [GaudiHistos<GaudiAlgorithm>] """,
    'HistoSplitDir' : """ Split long directory names into short pieces (suitable for HBOOK) [GaudiHistos<GaudiAlgorithm>] """,
    'HistoOffSet' : """ OffSet for automatically assigned histogram numerical identifiers  [GaudiHistos<GaudiAlgorithm>] """,
    'HistoTopDir' : """ Top level histogram directory (take care that it ends with '/') [GaudiHistos<GaudiAlgorithm>] """,
    'HistoDir' : """ Histogram Directory [GaudiHistos<GaudiAlgorithm>] """,
    'FullDetail' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'MonitorHistograms' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'FormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'ShortFormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HeaderFor1DHistoTable' : """ The table header for printout of 1D histograms  [GaudiHistos<GaudiAlgorithm>] """,
    'UseSequencialNumericAutoIDs' : """ Flag to allow users to switch back to the old style of creating numerical automatic IDs [GaudiHistos<GaudiAlgorithm>] """,
    'AutoStringIDPurgeMap' : """ Map of strings to search and replace when using the title as the basis of automatically generated literal IDs [GaudiHistos<GaudiAlgorithm>] """,
    'NTupleProduce' : """ general switch to enable/disable N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTuplePrint' : """ print N-tuple statistics [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleSplitDir' : """ split long directory names into short pieces (suitable for HBOOK) [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleLUN' : """ Logical File Unit for N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleTopDir' : """ top-level directory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleDir' : """ subdirectory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsProduce' : """ general switch to enable/disable Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsPrint' : """ print statistics for Event Tag Collections  [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColSplitDir' : """ split long directory names into short pieces [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColLUN' : """ Logical File Unit for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColTopDir' : """ Top-level directory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColDir' : """ Subdirectory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(VAlign, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'VeloAlignment'
  def getType( self ):
      return 'VAlign'
  pass # class VAlign
