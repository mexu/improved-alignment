// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME AlignEventDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Alignment/AlignEvent/god_dict/AlignEvent_dictionary.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *SmartReflELHCbcLcLAlignSummaryDatagR_Dictionary();
   static void SmartReflELHCbcLcLAlignSummaryDatagR_TClassManip(TClass*);
   static void *new_SmartReflELHCbcLcLAlignSummaryDatagR(void *p = 0);
   static void *newArray_SmartReflELHCbcLcLAlignSummaryDatagR(Long_t size, void *p);
   static void delete_SmartReflELHCbcLcLAlignSummaryDatagR(void *p);
   static void deleteArray_SmartReflELHCbcLcLAlignSummaryDatagR(void *p);
   static void destruct_SmartReflELHCbcLcLAlignSummaryDatagR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SmartRef<LHCb::AlignSummaryData>*)
   {
      ::SmartRef<LHCb::AlignSummaryData> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SmartRef<LHCb::AlignSummaryData>));
      static ::ROOT::TGenericClassInfo 
         instance("SmartRef<LHCb::AlignSummaryData>", "GaudiKernel/SmartRef.h", 66,
                  typeid(::SmartRef<LHCb::AlignSummaryData>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SmartReflELHCbcLcLAlignSummaryDatagR_Dictionary, isa_proxy, 4,
                  sizeof(::SmartRef<LHCb::AlignSummaryData>) );
      instance.SetNew(&new_SmartReflELHCbcLcLAlignSummaryDatagR);
      instance.SetNewArray(&newArray_SmartReflELHCbcLcLAlignSummaryDatagR);
      instance.SetDelete(&delete_SmartReflELHCbcLcLAlignSummaryDatagR);
      instance.SetDeleteArray(&deleteArray_SmartReflELHCbcLcLAlignSummaryDatagR);
      instance.SetDestructor(&destruct_SmartReflELHCbcLcLAlignSummaryDatagR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SmartRef<LHCb::AlignSummaryData>*)
   {
      return GenerateInitInstanceLocal((::SmartRef<LHCb::AlignSummaryData>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::SmartRef<LHCb::AlignSummaryData>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SmartReflELHCbcLcLAlignSummaryDatagR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SmartRef<LHCb::AlignSummaryData>*)0x0)->GetClass();
      SmartReflELHCbcLcLAlignSummaryDatagR_TClassManip(theClass);
   return theClass;
   }

   static void SmartReflELHCbcLcLAlignSummaryDatagR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SmartRefVectorlELHCbcLcLAlignSummaryDatagR_Dictionary();
   static void SmartRefVectorlELHCbcLcLAlignSummaryDatagR_TClassManip(TClass*);
   static void *new_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p = 0);
   static void *newArray_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(Long_t size, void *p);
   static void delete_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p);
   static void deleteArray_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p);
   static void destruct_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SmartRefVector<LHCb::AlignSummaryData>*)
   {
      ::SmartRefVector<LHCb::AlignSummaryData> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SmartRefVector<LHCb::AlignSummaryData>));
      static ::ROOT::TGenericClassInfo 
         instance("SmartRefVector<LHCb::AlignSummaryData>", "GaudiKernel/SmartRefVector.h", 55,
                  typeid(::SmartRefVector<LHCb::AlignSummaryData>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &SmartRefVectorlELHCbcLcLAlignSummaryDatagR_Dictionary, isa_proxy, 4,
                  sizeof(::SmartRefVector<LHCb::AlignSummaryData>) );
      instance.SetNew(&new_SmartRefVectorlELHCbcLcLAlignSummaryDatagR);
      instance.SetNewArray(&newArray_SmartRefVectorlELHCbcLcLAlignSummaryDatagR);
      instance.SetDelete(&delete_SmartRefVectorlELHCbcLcLAlignSummaryDatagR);
      instance.SetDeleteArray(&deleteArray_SmartRefVectorlELHCbcLcLAlignSummaryDatagR);
      instance.SetDestructor(&destruct_SmartRefVectorlELHCbcLcLAlignSummaryDatagR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SmartRefVector<LHCb::AlignSummaryData>*)
   {
      return GenerateInitInstanceLocal((::SmartRefVector<LHCb::AlignSummaryData>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::SmartRefVector<LHCb::AlignSummaryData>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SmartRefVectorlELHCbcLcLAlignSummaryDatagR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SmartRefVector<LHCb::AlignSummaryData>*)0x0)->GetClass();
      SmartRefVectorlELHCbcLcLAlignSummaryDatagR_TClassManip(theClass);
   return theClass;
   }

   static void SmartRefVectorlELHCbcLcLAlignSummaryDatagR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *LHCbcLcLAlignSummaryData_Dictionary();
   static void LHCbcLcLAlignSummaryData_TClassManip(TClass*);
   static void *new_LHCbcLcLAlignSummaryData(void *p = 0);
   static void *newArray_LHCbcLcLAlignSummaryData(Long_t size, void *p);
   static void delete_LHCbcLcLAlignSummaryData(void *p);
   static void deleteArray_LHCbcLcLAlignSummaryData(void *p);
   static void destruct_LHCbcLcLAlignSummaryData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LHCb::AlignSummaryData*)
   {
      ::LHCb::AlignSummaryData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LHCb::AlignSummaryData));
      static ::ROOT::TGenericClassInfo 
         instance("LHCb::AlignSummaryData", "Event/AlignSummaryData.h", 48,
                  typeid(::LHCb::AlignSummaryData), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &LHCbcLcLAlignSummaryData_Dictionary, isa_proxy, 4,
                  sizeof(::LHCb::AlignSummaryData) );
      instance.SetNew(&new_LHCbcLcLAlignSummaryData);
      instance.SetNewArray(&newArray_LHCbcLcLAlignSummaryData);
      instance.SetDelete(&delete_LHCbcLcLAlignSummaryData);
      instance.SetDeleteArray(&deleteArray_LHCbcLcLAlignSummaryData);
      instance.SetDestructor(&destruct_LHCbcLcLAlignSummaryData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LHCb::AlignSummaryData*)
   {
      return GenerateInitInstanceLocal((::LHCb::AlignSummaryData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LHCb::AlignSummaryData*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LHCbcLcLAlignSummaryData_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LHCb::AlignSummaryData*)0x0)->GetClass();
      LHCbcLcLAlignSummaryData_TClassManip(theClass);
   return theClass;
   }

   static void LHCbcLcLAlignSummaryData_TClassManip(TClass* theClass){
      theClass->CreateAttributeMap();
      TDictAttributeMap* attrMap( theClass->GetAttributeMap() );
      attrMap->AddProperty("id","000027e5-0000-0000-0000-000000000000");
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_SmartReflELHCbcLcLAlignSummaryDatagR(void *p) {
      return  p ? new(p) ::SmartRef<LHCb::AlignSummaryData> : new ::SmartRef<LHCb::AlignSummaryData>;
   }
   static void *newArray_SmartReflELHCbcLcLAlignSummaryDatagR(Long_t nElements, void *p) {
      return p ? new(p) ::SmartRef<LHCb::AlignSummaryData>[nElements] : new ::SmartRef<LHCb::AlignSummaryData>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SmartReflELHCbcLcLAlignSummaryDatagR(void *p) {
      delete ((::SmartRef<LHCb::AlignSummaryData>*)p);
   }
   static void deleteArray_SmartReflELHCbcLcLAlignSummaryDatagR(void *p) {
      delete [] ((::SmartRef<LHCb::AlignSummaryData>*)p);
   }
   static void destruct_SmartReflELHCbcLcLAlignSummaryDatagR(void *p) {
      typedef ::SmartRef<LHCb::AlignSummaryData> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SmartRef<LHCb::AlignSummaryData>

namespace ROOT {
   // Wrappers around operator new
   static void *new_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p) {
      return  p ? new(p) ::SmartRefVector<LHCb::AlignSummaryData> : new ::SmartRefVector<LHCb::AlignSummaryData>;
   }
   static void *newArray_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(Long_t nElements, void *p) {
      return p ? new(p) ::SmartRefVector<LHCb::AlignSummaryData>[nElements] : new ::SmartRefVector<LHCb::AlignSummaryData>[nElements];
   }
   // Wrapper around operator delete
   static void delete_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p) {
      delete ((::SmartRefVector<LHCb::AlignSummaryData>*)p);
   }
   static void deleteArray_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p) {
      delete [] ((::SmartRefVector<LHCb::AlignSummaryData>*)p);
   }
   static void destruct_SmartRefVectorlELHCbcLcLAlignSummaryDatagR(void *p) {
      typedef ::SmartRefVector<LHCb::AlignSummaryData> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SmartRefVector<LHCb::AlignSummaryData>

namespace ROOT {
   // Wrappers around operator new
   static void *new_LHCbcLcLAlignSummaryData(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::LHCb::AlignSummaryData : new ::LHCb::AlignSummaryData;
   }
   static void *newArray_LHCbcLcLAlignSummaryData(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::LHCb::AlignSummaryData[nElements] : new ::LHCb::AlignSummaryData[nElements];
   }
   // Wrapper around operator delete
   static void delete_LHCbcLcLAlignSummaryData(void *p) {
      delete ((::LHCb::AlignSummaryData*)p);
   }
   static void deleteArray_LHCbcLcLAlignSummaryData(void *p) {
      delete [] ((::LHCb::AlignSummaryData*)p);
   }
   static void destruct_LHCbcLcLAlignSummaryData(void *p) {
      typedef ::LHCb::AlignSummaryData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LHCb::AlignSummaryData

namespace ROOT {
   static TClass *vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR_Dictionary();
   static void vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR_TClassManip(TClass*);
   static void *new_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p = 0);
   static void *newArray_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(Long_t size, void *p);
   static void delete_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p);
   static void deleteArray_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p);
   static void destruct_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<SmartRef<LHCb::AlignSummaryData> >*)
   {
      vector<SmartRef<LHCb::AlignSummaryData> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<SmartRef<LHCb::AlignSummaryData> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<SmartRef<LHCb::AlignSummaryData> >", -2, "vector", 216,
                  typeid(vector<SmartRef<LHCb::AlignSummaryData> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<SmartRef<LHCb::AlignSummaryData> >) );
      instance.SetNew(&new_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR);
      instance.SetNewArray(&newArray_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR);
      instance.SetDelete(&delete_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR);
      instance.SetDestructor(&destruct_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<SmartRef<LHCb::AlignSummaryData> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<SmartRef<LHCb::AlignSummaryData> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<SmartRef<LHCb::AlignSummaryData> >*)0x0)->GetClass();
      vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<SmartRef<LHCb::AlignSummaryData> > : new vector<SmartRef<LHCb::AlignSummaryData> >;
   }
   static void *newArray_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<SmartRef<LHCb::AlignSummaryData> >[nElements] : new vector<SmartRef<LHCb::AlignSummaryData> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p) {
      delete ((vector<SmartRef<LHCb::AlignSummaryData> >*)p);
   }
   static void deleteArray_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p) {
      delete [] ((vector<SmartRef<LHCb::AlignSummaryData> >*)p);
   }
   static void destruct_vectorlESmartReflELHCbcLcLAlignSummaryDatagRsPgR(void *p) {
      typedef vector<SmartRef<LHCb::AlignSummaryData> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<SmartRef<LHCb::AlignSummaryData> >

namespace {
  void TriggerDictionaryInitialization_AlignEventDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignEvent",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/cppgsl/b07383ea/x86_64-centos7-gcc7-opt",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/vdt/0.3.9/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignKernel",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Alignment/AlignEvent/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "AlignEventDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace LHCb{class __attribute__((annotate(R"ATTRDUMP(id@@@000027e5-0000-0000-0000-000000000000)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$Event/AlignSummaryData.h")))  AlignSummaryData;}
template <class TYPE> class __attribute__((annotate("$clingAutoload$GaudiKernel/SmartRef.h")))  __attribute__((annotate("$clingAutoload$GaudiKernel/SmartRefVector.h")))  SmartRef;

namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
template <class TYPE> class __attribute__((annotate("$clingAutoload$GaudiKernel/SmartRefVector.h")))  SmartRefVector;

)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "AlignEventDict dictionary payload"
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations AlignEvent_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "AlignEvent"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v1r3"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H

//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

// begin include files
#include <vector>
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/SmartRef.h"
#include "Event/AlignSummaryData.h"
// end include files

namespace {
  struct AlignEvent_Instantiations {
    // begin instantiations
    SmartRef<LHCb::AlignSummaryData>               m_SmartRef_LHCb__AlignSummaryData;
    SmartRefVector<LHCb::AlignSummaryData>         m_SmartRefVector_LHCb__AlignSummaryData;
    std::vector<SmartRef<LHCb::AlignSummaryData> > m_std_vector_SmartRef_LHCb__AlignSummaryData;
    // end instantiations
  };
}

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"LHCb::AlignSummaryData", payloadCode, "@",
"SmartRef<LHCb::AlignSummaryData>", payloadCode, "@",
"SmartRefVector<LHCb::AlignSummaryData>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("AlignEventDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_AlignEventDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_AlignEventDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_AlignEventDict() {
  TriggerDictionaryInitialization_AlignEventDict_Impl();
}
