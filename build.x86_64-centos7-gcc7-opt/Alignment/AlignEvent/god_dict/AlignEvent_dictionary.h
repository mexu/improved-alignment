
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

// begin include files
#include <vector>
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/SmartRef.h"
#include "Event/AlignSummaryData.h"
// end include files

namespace {
  struct AlignEvent_Instantiations {
    // begin instantiations
    SmartRef<LHCb::AlignSummaryData>               m_SmartRef_LHCb__AlignSummaryData;
    SmartRefVector<LHCb::AlignSummaryData>         m_SmartRefVector_LHCb__AlignSummaryData;
    std::vector<SmartRef<LHCb::AlignSummaryData> > m_std_vector_SmartRef_LHCb__AlignSummaryData;
    // end instantiations
  };
}
