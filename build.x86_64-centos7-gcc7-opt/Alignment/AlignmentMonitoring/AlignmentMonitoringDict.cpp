// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME AlignmentMonitoringDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignmentMonitoring/dict/AlignmentMonitoringDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLHistHelper_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLHistHelper_TClassManip(TClass*);
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p = 0);
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(Long_t size, void *p);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::HistHelper*)
   {
      ::Alignment::AlignmentMonitoring::HistHelper *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::HistHelper));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::HistHelper", "AlignmentMonitoring/HistHelper.h", 21,
                  typeid(::Alignment::AlignmentMonitoring::HistHelper), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLHistHelper_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::HistHelper) );
      instance.SetNew(&new_AlignmentcLcLAlignmentMonitoringcLcLHistHelper);
      instance.SetNewArray(&newArray_AlignmentcLcLAlignmentMonitoringcLcLHistHelper);
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLHistHelper);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLHistHelper);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLHistHelper);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::HistHelper*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::HistHelper*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::HistHelper*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLHistHelper_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::HistHelper*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLHistHelper_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLHistHelper_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring_TClassManip(TClass*);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::AlignmentMonitoring*)
   {
      ::Alignment::AlignmentMonitoring::AlignmentMonitoring *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::AlignmentMonitoring));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::AlignmentMonitoring", "AlignmentMonitoring/AlignmentMonitoring.h", 30,
                  typeid(::Alignment::AlignmentMonitoring::AlignmentMonitoring), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::AlignmentMonitoring) );
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::AlignmentMonitoring*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::AlignmentMonitoring*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::AlignmentMonitoring*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::AlignmentMonitoring*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLCompareConstants_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLCompareConstants_TClassManip(TClass*);
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p = 0);
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(Long_t size, void *p);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::CompareConstants*)
   {
      ::Alignment::AlignmentMonitoring::CompareConstants *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::CompareConstants));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::CompareConstants", "AlignmentMonitoring/CompareConstants.h", 19,
                  typeid(::Alignment::AlignmentMonitoring::CompareConstants), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLCompareConstants_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::CompareConstants) );
      instance.SetNew(&new_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants);
      instance.SetNewArray(&newArray_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants);
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::CompareConstants*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::CompareConstants*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::CompareConstants*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLCompareConstants_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::CompareConstants*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLCompareConstants_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLCompareConstants_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage_TClassManip(TClass*);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::MonitoringPage*)
   {
      ::Alignment::AlignmentMonitoring::MonitoringPage *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::MonitoringPage));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::MonitoringPage", "AlignmentMonitoring/MonitoringPage.h", 19,
                  typeid(::Alignment::AlignmentMonitoring::MonitoringPage), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::MonitoringPage) );
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::MonitoringPage*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::MonitoringPage*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::MonitoringPage*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::MonitoringPage*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication_TClassManip(TClass*);
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p = 0);
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(Long_t size, void *p);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::MonitoringApplication*)
   {
      ::Alignment::AlignmentMonitoring::MonitoringApplication *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::MonitoringApplication));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::MonitoringApplication", "AlignmentMonitoring/MonitoringApplication.h", 17,
                  typeid(::Alignment::AlignmentMonitoring::MonitoringApplication), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::MonitoringApplication) );
      instance.SetNew(&new_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication);
      instance.SetNewArray(&newArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication);
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::MonitoringApplication*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::MonitoringApplication*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::MonitoringApplication*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::MonitoringApplication*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLParseConstants_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLParseConstants_TClassManip(TClass*);
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p = 0);
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(Long_t size, void *p);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::ParseConstants*)
   {
      ::Alignment::AlignmentMonitoring::ParseConstants *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::ParseConstants));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::ParseConstants", "AlignmentMonitoring/ParseConstants.h", 12,
                  typeid(::Alignment::AlignmentMonitoring::ParseConstants), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLParseConstants_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::ParseConstants) );
      instance.SetNew(&new_AlignmentcLcLAlignmentMonitoringcLcLParseConstants);
      instance.SetNewArray(&newArray_AlignmentcLcLAlignmentMonitoringcLcLParseConstants);
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLParseConstants);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLParseConstants);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLParseConstants);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::ParseConstants*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::ParseConstants*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::ParseConstants*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLParseConstants_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::ParseConstants*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLParseConstants_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLParseConstants_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLUtilities_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLUtilities_TClassManip(TClass*);
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p = 0);
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLUtilities(Long_t size, void *p);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::Utilities*)
   {
      ::Alignment::AlignmentMonitoring::Utilities *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::Utilities));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::Utilities", "AlignmentMonitoring/Utilities.h", 33,
                  typeid(::Alignment::AlignmentMonitoring::Utilities), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLUtilities_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::Utilities) );
      instance.SetNew(&new_AlignmentcLcLAlignmentMonitoringcLcLUtilities);
      instance.SetNewArray(&newArray_AlignmentcLcLAlignmentMonitoringcLcLUtilities);
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLUtilities);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLUtilities);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLUtilities);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::Utilities*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::Utilities*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::Utilities*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLUtilities_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::Utilities*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLUtilities_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLUtilities_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLNameITSector_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLNameITSector_TClassManip(TClass*);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLNameITSector(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLNameITSector(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLNameITSector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::NameITSector*)
   {
      ::Alignment::AlignmentMonitoring::NameITSector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::NameITSector));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::NameITSector", "AlignmentMonitoring/NameITSector.h", 15,
                  typeid(::Alignment::AlignmentMonitoring::NameITSector), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLNameITSector_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::NameITSector) );
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLNameITSector);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLNameITSector);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLNameITSector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::NameITSector*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::NameITSector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::NameITSector*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLNameITSector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::NameITSector*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLNameITSector_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLNameITSector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLNameTTSector_Dictionary();
   static void AlignmentcLcLAlignmentMonitoringcLcLNameTTSector_TClassManip(TClass*);
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector(void *p);
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector(void *p);
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Alignment::AlignmentMonitoring::NameTTSector*)
   {
      ::Alignment::AlignmentMonitoring::NameTTSector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Alignment::AlignmentMonitoring::NameTTSector));
      static ::ROOT::TGenericClassInfo 
         instance("Alignment::AlignmentMonitoring::NameTTSector", "AlignmentMonitoring/NameTTSector.h", 15,
                  typeid(::Alignment::AlignmentMonitoring::NameTTSector), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlignmentcLcLAlignmentMonitoringcLcLNameTTSector_Dictionary, isa_proxy, 4,
                  sizeof(::Alignment::AlignmentMonitoring::NameTTSector) );
      instance.SetDelete(&delete_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector);
      instance.SetDeleteArray(&deleteArray_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector);
      instance.SetDestructor(&destruct_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Alignment::AlignmentMonitoring::NameTTSector*)
   {
      return GenerateInitInstanceLocal((::Alignment::AlignmentMonitoring::NameTTSector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::NameTTSector*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlignmentcLcLAlignmentMonitoringcLcLNameTTSector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Alignment::AlignmentMonitoring::NameTTSector*)0x0)->GetClass();
      AlignmentcLcLAlignmentMonitoringcLcLNameTTSector_TClassManip(theClass);
   return theClass;
   }

   static void AlignmentcLcLAlignmentMonitoringcLcLNameTTSector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::HistHelper : new ::Alignment::AlignmentMonitoring::HistHelper;
   }
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::HistHelper[nElements] : new ::Alignment::AlignmentMonitoring::HistHelper[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p) {
      delete ((::Alignment::AlignmentMonitoring::HistHelper*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::HistHelper*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLHistHelper(void *p) {
      typedef ::Alignment::AlignmentMonitoring::HistHelper current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::HistHelper

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring(void *p) {
      delete ((::Alignment::AlignmentMonitoring::AlignmentMonitoring*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::AlignmentMonitoring*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLAlignmentMonitoring(void *p) {
      typedef ::Alignment::AlignmentMonitoring::AlignmentMonitoring current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::AlignmentMonitoring

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::CompareConstants : new ::Alignment::AlignmentMonitoring::CompareConstants;
   }
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::CompareConstants[nElements] : new ::Alignment::AlignmentMonitoring::CompareConstants[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p) {
      delete ((::Alignment::AlignmentMonitoring::CompareConstants*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::CompareConstants*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLCompareConstants(void *p) {
      typedef ::Alignment::AlignmentMonitoring::CompareConstants current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::CompareConstants

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage(void *p) {
      delete ((::Alignment::AlignmentMonitoring::MonitoringPage*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::MonitoringPage*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLMonitoringPage(void *p) {
      typedef ::Alignment::AlignmentMonitoring::MonitoringPage current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::MonitoringPage

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::MonitoringApplication : new ::Alignment::AlignmentMonitoring::MonitoringApplication;
   }
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::MonitoringApplication[nElements] : new ::Alignment::AlignmentMonitoring::MonitoringApplication[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p) {
      delete ((::Alignment::AlignmentMonitoring::MonitoringApplication*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::MonitoringApplication*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLMonitoringApplication(void *p) {
      typedef ::Alignment::AlignmentMonitoring::MonitoringApplication current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::MonitoringApplication

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::ParseConstants : new ::Alignment::AlignmentMonitoring::ParseConstants;
   }
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::ParseConstants[nElements] : new ::Alignment::AlignmentMonitoring::ParseConstants[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p) {
      delete ((::Alignment::AlignmentMonitoring::ParseConstants*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::ParseConstants*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLParseConstants(void *p) {
      typedef ::Alignment::AlignmentMonitoring::ParseConstants current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::ParseConstants

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::Utilities : new ::Alignment::AlignmentMonitoring::Utilities;
   }
   static void *newArray_AlignmentcLcLAlignmentMonitoringcLcLUtilities(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Alignment::AlignmentMonitoring::Utilities[nElements] : new ::Alignment::AlignmentMonitoring::Utilities[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p) {
      delete ((::Alignment::AlignmentMonitoring::Utilities*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::Utilities*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLUtilities(void *p) {
      typedef ::Alignment::AlignmentMonitoring::Utilities current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::Utilities

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLNameITSector(void *p) {
      delete ((::Alignment::AlignmentMonitoring::NameITSector*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLNameITSector(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::NameITSector*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLNameITSector(void *p) {
      typedef ::Alignment::AlignmentMonitoring::NameITSector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::NameITSector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector(void *p) {
      delete ((::Alignment::AlignmentMonitoring::NameTTSector*)p);
   }
   static void deleteArray_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector(void *p) {
      delete [] ((::Alignment::AlignmentMonitoring::NameTTSector*)p);
   }
   static void destruct_AlignmentcLcLAlignmentMonitoringcLcLNameTTSector(void *p) {
      typedef ::Alignment::AlignmentMonitoring::NameTTSector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Alignment::AlignmentMonitoring::NameTTSector

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 216,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlETLinemUgR_Dictionary();
   static void vectorlETLinemUgR_TClassManip(TClass*);
   static void *new_vectorlETLinemUgR(void *p = 0);
   static void *newArray_vectorlETLinemUgR(Long_t size, void *p);
   static void delete_vectorlETLinemUgR(void *p);
   static void deleteArray_vectorlETLinemUgR(void *p);
   static void destruct_vectorlETLinemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TLine*>*)
   {
      vector<TLine*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TLine*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TLine*>", -2, "vector", 216,
                  typeid(vector<TLine*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETLinemUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<TLine*>) );
      instance.SetNew(&new_vectorlETLinemUgR);
      instance.SetNewArray(&newArray_vectorlETLinemUgR);
      instance.SetDelete(&delete_vectorlETLinemUgR);
      instance.SetDeleteArray(&deleteArray_vectorlETLinemUgR);
      instance.SetDestructor(&destruct_vectorlETLinemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TLine*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TLine*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETLinemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TLine*>*)0x0)->GetClass();
      vectorlETLinemUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETLinemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETLinemUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TLine*> : new vector<TLine*>;
   }
   static void *newArray_vectorlETLinemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TLine*>[nElements] : new vector<TLine*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETLinemUgR(void *p) {
      delete ((vector<TLine*>*)p);
   }
   static void deleteArray_vectorlETLinemUgR(void *p) {
      delete [] ((vector<TLine*>*)p);
   }
   static void destruct_vectorlETLinemUgR(void *p) {
      typedef vector<TLine*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TLine*>

namespace ROOT {
   static TClass *maplEstringcOvectorlEdoublegRsPgR_Dictionary();
   static void maplEstringcOvectorlEdoublegRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEdoublegRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEdoublegRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEdoublegRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEdoublegRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<double> >*)
   {
      map<string,vector<double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<double> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<double> >", -2, "map", 99,
                  typeid(map<string,vector<double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEdoublegRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,vector<double> >) );
      instance.SetNew(&new_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEdoublegRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<double> >*)0x0)->GetClass();
      maplEstringcOvectorlEdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<double> > : new map<string,vector<double> >;
   }
   static void *newArray_maplEstringcOvectorlEdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<double> >[nElements] : new map<string,vector<double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      delete ((map<string,vector<double> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      delete [] ((map<string,vector<double> >*)p);
   }
   static void destruct_maplEstringcOvectorlEdoublegRsPgR(void *p) {
      typedef map<string,vector<double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<double> >

namespace ROOT {
   static TClass *maplEstringcOvectorlETLinemUgRsPgR_Dictionary();
   static void maplEstringcOvectorlETLinemUgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlETLinemUgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlETLinemUgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlETLinemUgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlETLinemUgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlETLinemUgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<TLine*> >*)
   {
      map<string,vector<TLine*> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<TLine*> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<TLine*> >", -2, "map", 99,
                  typeid(map<string,vector<TLine*> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlETLinemUgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,vector<TLine*> >) );
      instance.SetNew(&new_maplEstringcOvectorlETLinemUgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlETLinemUgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlETLinemUgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlETLinemUgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlETLinemUgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<TLine*> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<TLine*> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlETLinemUgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<TLine*> >*)0x0)->GetClass();
      maplEstringcOvectorlETLinemUgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlETLinemUgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlETLinemUgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<TLine*> > : new map<string,vector<TLine*> >;
   }
   static void *newArray_maplEstringcOvectorlETLinemUgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<TLine*> >[nElements] : new map<string,vector<TLine*> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlETLinemUgRsPgR(void *p) {
      delete ((map<string,vector<TLine*> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlETLinemUgRsPgR(void *p) {
      delete [] ((map<string,vector<TLine*> >*)p);
   }
   static void destruct_maplEstringcOvectorlETLinemUgRsPgR(void *p) {
      typedef map<string,vector<TLine*> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<TLine*> >

namespace ROOT {
   static TClass *maplEstringcOstringgR_Dictionary();
   static void maplEstringcOstringgR_TClassManip(TClass*);
   static void *new_maplEstringcOstringgR(void *p = 0);
   static void *newArray_maplEstringcOstringgR(Long_t size, void *p);
   static void delete_maplEstringcOstringgR(void *p);
   static void deleteArray_maplEstringcOstringgR(void *p);
   static void destruct_maplEstringcOstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,string>*)
   {
      map<string,string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,string>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,string>", -2, "map", 99,
                  typeid(map<string,string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOstringgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,string>) );
      instance.SetNew(&new_maplEstringcOstringgR);
      instance.SetNewArray(&newArray_maplEstringcOstringgR);
      instance.SetDelete(&delete_maplEstringcOstringgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOstringgR);
      instance.SetDestructor(&destruct_maplEstringcOstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,string>*)0x0)->GetClass();
      maplEstringcOstringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string> : new map<string,string>;
   }
   static void *newArray_maplEstringcOstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string>[nElements] : new map<string,string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOstringgR(void *p) {
      delete ((map<string,string>*)p);
   }
   static void deleteArray_maplEstringcOstringgR(void *p) {
      delete [] ((map<string,string>*)p);
   }
   static void destruct_maplEstringcOstringgR(void *p) {
      typedef map<string,string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,string>

namespace ROOT {
   static TClass *maplEstringcOpairlEstringcOdoublegRsPgR_Dictionary();
   static void maplEstringcOpairlEstringcOdoublegRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOpairlEstringcOdoublegRsPgR(void *p = 0);
   static void *newArray_maplEstringcOpairlEstringcOdoublegRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOpairlEstringcOdoublegRsPgR(void *p);
   static void deleteArray_maplEstringcOpairlEstringcOdoublegRsPgR(void *p);
   static void destruct_maplEstringcOpairlEstringcOdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,pair<string,double> >*)
   {
      map<string,pair<string,double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,pair<string,double> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,pair<string,double> >", -2, "map", 99,
                  typeid(map<string,pair<string,double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOpairlEstringcOdoublegRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,pair<string,double> >) );
      instance.SetNew(&new_maplEstringcOpairlEstringcOdoublegRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOpairlEstringcOdoublegRsPgR);
      instance.SetDelete(&delete_maplEstringcOpairlEstringcOdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOpairlEstringcOdoublegRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOpairlEstringcOdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,pair<string,double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,pair<string,double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOpairlEstringcOdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,pair<string,double> >*)0x0)->GetClass();
      maplEstringcOpairlEstringcOdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOpairlEstringcOdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOpairlEstringcOdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,pair<string,double> > : new map<string,pair<string,double> >;
   }
   static void *newArray_maplEstringcOpairlEstringcOdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,pair<string,double> >[nElements] : new map<string,pair<string,double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOpairlEstringcOdoublegRsPgR(void *p) {
      delete ((map<string,pair<string,double> >*)p);
   }
   static void deleteArray_maplEstringcOpairlEstringcOdoublegRsPgR(void *p) {
      delete [] ((map<string,pair<string,double> >*)p);
   }
   static void destruct_maplEstringcOpairlEstringcOdoublegRsPgR(void *p) {
      typedef map<string,pair<string,double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,pair<string,double> >

namespace ROOT {
   static TClass *maplEstringcOpairlEdoublecOdoublegRsPgR_Dictionary();
   static void maplEstringcOpairlEdoublecOdoublegRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p = 0);
   static void *newArray_maplEstringcOpairlEdoublecOdoublegRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p);
   static void deleteArray_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p);
   static void destruct_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,pair<double,double> >*)
   {
      map<string,pair<double,double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,pair<double,double> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,pair<double,double> >", -2, "map", 99,
                  typeid(map<string,pair<double,double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOpairlEdoublecOdoublegRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,pair<double,double> >) );
      instance.SetNew(&new_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetDelete(&delete_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOpairlEdoublecOdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,pair<double,double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,pair<double,double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOpairlEdoublecOdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,pair<double,double> >*)0x0)->GetClass();
      maplEstringcOpairlEdoublecOdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOpairlEdoublecOdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,pair<double,double> > : new map<string,pair<double,double> >;
   }
   static void *newArray_maplEstringcOpairlEdoublecOdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,pair<double,double> >[nElements] : new map<string,pair<double,double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      delete ((map<string,pair<double,double> >*)p);
   }
   static void deleteArray_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      delete [] ((map<string,pair<double,double> >*)p);
   }
   static void destruct_maplEstringcOpairlEdoublecOdoublegRsPgR(void *p) {
      typedef map<string,pair<double,double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,pair<double,double> >

namespace ROOT {
   static TClass *maplEstringcOintgR_Dictionary();
   static void maplEstringcOintgR_TClassManip(TClass*);
   static void *new_maplEstringcOintgR(void *p = 0);
   static void *newArray_maplEstringcOintgR(Long_t size, void *p);
   static void delete_maplEstringcOintgR(void *p);
   static void deleteArray_maplEstringcOintgR(void *p);
   static void destruct_maplEstringcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,int>*)
   {
      map<string,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,int>", -2, "map", 99,
                  typeid(map<string,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOintgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,int>) );
      instance.SetNew(&new_maplEstringcOintgR);
      instance.SetNewArray(&newArray_maplEstringcOintgR);
      instance.SetDelete(&delete_maplEstringcOintgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOintgR);
      instance.SetDestructor(&destruct_maplEstringcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,int>*)0x0)->GetClass();
      maplEstringcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,int> : new map<string,int>;
   }
   static void *newArray_maplEstringcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,int>[nElements] : new map<string,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOintgR(void *p) {
      delete ((map<string,int>*)p);
   }
   static void deleteArray_maplEstringcOintgR(void *p) {
      delete [] ((map<string,int>*)p);
   }
   static void destruct_maplEstringcOintgR(void *p) {
      typedef map<string,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,int>

namespace ROOT {
   static TClass *maplEstringcOdoublegR_Dictionary();
   static void maplEstringcOdoublegR_TClassManip(TClass*);
   static void *new_maplEstringcOdoublegR(void *p = 0);
   static void *newArray_maplEstringcOdoublegR(Long_t size, void *p);
   static void delete_maplEstringcOdoublegR(void *p);
   static void deleteArray_maplEstringcOdoublegR(void *p);
   static void destruct_maplEstringcOdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,double>*)
   {
      map<string,double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,double>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,double>", -2, "map", 99,
                  typeid(map<string,double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,double>) );
      instance.SetNew(&new_maplEstringcOdoublegR);
      instance.SetNewArray(&newArray_maplEstringcOdoublegR);
      instance.SetDelete(&delete_maplEstringcOdoublegR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOdoublegR);
      instance.SetDestructor(&destruct_maplEstringcOdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,double>*)0x0)->GetClass();
      maplEstringcOdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double> : new map<string,double>;
   }
   static void *newArray_maplEstringcOdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double>[nElements] : new map<string,double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOdoublegR(void *p) {
      delete ((map<string,double>*)p);
   }
   static void deleteArray_maplEstringcOdoublegR(void *p) {
      delete [] ((map<string,double>*)p);
   }
   static void destruct_maplEstringcOdoublegR(void *p) {
      typedef map<string,double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,double>

namespace ROOT {
   static TClass *maplEstringcOTH1DmUgR_Dictionary();
   static void maplEstringcOTH1DmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTH1DmUgR(void *p = 0);
   static void *newArray_maplEstringcOTH1DmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTH1DmUgR(void *p);
   static void deleteArray_maplEstringcOTH1DmUgR(void *p);
   static void destruct_maplEstringcOTH1DmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TH1D*>*)
   {
      map<string,TH1D*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TH1D*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TH1D*>", -2, "map", 99,
                  typeid(map<string,TH1D*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTH1DmUgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,TH1D*>) );
      instance.SetNew(&new_maplEstringcOTH1DmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTH1DmUgR);
      instance.SetDelete(&delete_maplEstringcOTH1DmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTH1DmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTH1DmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TH1D*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,TH1D*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTH1DmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,TH1D*>*)0x0)->GetClass();
      maplEstringcOTH1DmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTH1DmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTH1DmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH1D*> : new map<string,TH1D*>;
   }
   static void *newArray_maplEstringcOTH1DmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH1D*>[nElements] : new map<string,TH1D*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTH1DmUgR(void *p) {
      delete ((map<string,TH1D*>*)p);
   }
   static void deleteArray_maplEstringcOTH1DmUgR(void *p) {
      delete [] ((map<string,TH1D*>*)p);
   }
   static void destruct_maplEstringcOTH1DmUgR(void *p) {
      typedef map<string,TH1D*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,TH1D*>

namespace ROOT {
   static TClass *maplEstringcOTCanvasmUgR_Dictionary();
   static void maplEstringcOTCanvasmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTCanvasmUgR(void *p = 0);
   static void *newArray_maplEstringcOTCanvasmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTCanvasmUgR(void *p);
   static void deleteArray_maplEstringcOTCanvasmUgR(void *p);
   static void destruct_maplEstringcOTCanvasmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TCanvas*>*)
   {
      map<string,TCanvas*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TCanvas*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TCanvas*>", -2, "map", 99,
                  typeid(map<string,TCanvas*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTCanvasmUgR_Dictionary, isa_proxy, 4,
                  sizeof(map<string,TCanvas*>) );
      instance.SetNew(&new_maplEstringcOTCanvasmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTCanvasmUgR);
      instance.SetDelete(&delete_maplEstringcOTCanvasmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTCanvasmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTCanvasmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TCanvas*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,TCanvas*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTCanvasmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,TCanvas*>*)0x0)->GetClass();
      maplEstringcOTCanvasmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTCanvasmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTCanvasmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TCanvas*> : new map<string,TCanvas*>;
   }
   static void *newArray_maplEstringcOTCanvasmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TCanvas*>[nElements] : new map<string,TCanvas*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTCanvasmUgR(void *p) {
      delete ((map<string,TCanvas*>*)p);
   }
   static void deleteArray_maplEstringcOTCanvasmUgR(void *p) {
      delete [] ((map<string,TCanvas*>*)p);
   }
   static void destruct_maplEstringcOTCanvasmUgR(void *p) {
      typedef map<string,TCanvas*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,TCanvas*>

namespace {
  void TriggerDictionaryInitialization_AlignmentMonitoringDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignmentMonitoring",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Python/2.7.13/x86_64-centos7-gcc7-opt/include/python2.7",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Alignment/AlignmentMonitoring/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "AlignmentMonitoringDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/AlignmentMonitoring.h")))  HistHelper;}}
namespace std{template <class _CharT> struct __attribute__((annotate("$clingAutoload$bits/char_traits.h")))  __attribute__((annotate("$clingAutoload$string")))  char_traits;
}
namespace std{template <typename > class __attribute__((annotate("$clingAutoload$bits/memoryfwd.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/AlignmentMonitoring.h")))  AlignmentMonitoring;}}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/CompareConstants.h")))  CompareConstants;}}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/MonitoringPage.h")))  __attribute__((annotate("$clingAutoload$AlignmentMonitoring/MonitoringApplication.h")))  MonitoringPage;}}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/MonitoringApplication.h")))  MonitoringApplication;}}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/ParseConstants.h")))  ParseConstants;}}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/Utilities.h")))  Utilities;}}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/NameITSector.h")))  NameITSector;}}
namespace Alignment{namespace AlignmentMonitoring{class __attribute__((annotate("$clingAutoload$AlignmentMonitoring/NameTTSector.h")))  NameTTSector;}}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "AlignmentMonitoringDict dictionary payload"
#ifdef __MINGW32__
  #undef __MINGW32__
#endif
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations AlignmentMonitoring_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "AlignmentMonitoring"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v1r3"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef DICT_ALIGNMENTMONITORINGDICT_H 
#define DICT_ALIGNMENTMONITORINGDICT_H 1

// Include files
#include "AlignmentMonitoring/AlignmentMonitoring.h"
#include "AlignmentMonitoring/CompareConstants.h"
#include "AlignmentMonitoring/HistHelper.h"
#include "AlignmentMonitoring/MonitoringApplication.h"
#include "AlignmentMonitoring/MonitoringPage.h"
#include "AlignmentMonitoring/ParseConstants.h"
#include "AlignmentMonitoring/Utilities.h"
#include "AlignmentMonitoring/NameITSector.h"
#include "AlignmentMonitoring/NameTTSector.h"

#endif // DICT_ALIGNMENTMONITORINGDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Alignment::AlignmentMonitoring::AlignmentMonitoring", payloadCode, "@",
"Alignment::AlignmentMonitoring::CompareConstants", payloadCode, "@",
"Alignment::AlignmentMonitoring::HistHelper", payloadCode, "@",
"Alignment::AlignmentMonitoring::MonitoringApplication", payloadCode, "@",
"Alignment::AlignmentMonitoring::MonitoringPage", payloadCode, "@",
"Alignment::AlignmentMonitoring::NameITSector", payloadCode, "@",
"Alignment::AlignmentMonitoring::NameTTSector", payloadCode, "@",
"Alignment::AlignmentMonitoring::ParseConstants", payloadCode, "@",
"Alignment::AlignmentMonitoring::Utilities", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("AlignmentMonitoringDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_AlignmentMonitoringDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_AlignmentMonitoringDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_AlignmentMonitoringDict() {
  TriggerDictionaryInitialization_AlignmentMonitoringDict_Impl();
}
