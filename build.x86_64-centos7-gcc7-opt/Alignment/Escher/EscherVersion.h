#ifndef ESCHER_VERSION
/* Automatically generated file: do not modify! */
#ifndef CALC_GAUDI_VERSION
#define CALC_GAUDI_VERSION(maj,min) (((maj) << 16) + (min))
#endif
#define ESCHER_MAJOR_VERSION 5
#define ESCHER_MINOR_VERSION 8
#define ESCHER_PATCH_VERSION 0
#define ESCHER_VERSION CALC_GAUDI_VERSION(ESCHER_MAJOR_VERSION,ESCHER_MINOR_VERSION)
#endif
