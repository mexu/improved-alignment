#Mon Jun 15 10:14:15 2020"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.GaudiHandles import *
from GaudiKernel.Proxy.Configurable import *

class Al__AlignChisqConstraintTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'Constraints' : [  ], # list
    'XmlFiles' : [  ], # list
    'XmlUncertainties' : [  ], # list
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Al__AlignChisqConstraintTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'Al::AlignChisqConstraintTool'
  pass # class Al__AlignChisqConstraintTool

class Al__AlignConstraintTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'Constraints' : [  ], # list
    'UseWeightedAverage' : False, # bool
    'FixRedundantMotherDofs' : False, # bool
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Al__AlignConstraintTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'Al::AlignConstraintTool'
  pass # class Al__AlignConstraintTool

class Al__AlignUpdateTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'HistoProduce' : True, # bool
    'HistoPrint' : False, # bool
    'HistoCountersPrint' : True, # bool
    'HistoCheckForNaN' : True, # bool
    'HistoSplitDir' : False, # bool
    'HistoOffSet' : 0, # int
    'HistoTopDir' : '', # str
    'HistoDir' : 'AlgTool', # str
    'FullDetail' : False, # bool
    'MonitorHistograms' : True, # bool
    'FormatFor1DHistoTable' : '| %2$-45.45s | %3$=7d |%8$11.5g | %10$-11.5g|%12$11.5g |%14$11.5g |', # str
    'ShortFormatFor1DHistoTable' : ' | %1$-25.25s %2%', # str
    'HeaderFor1DHistoTable' : '|   Title                                       |    #    |     Mean   |    RMS     |  Skewness  |  Kurtosis  |', # str
    'UseSequencialNumericAutoIDs' : False, # bool
    'AutoStringIDPurgeMap' : { '/' : '=SLASH=' }, # list
    'MatrixSolverTool' : 'SpmInvTool', # str
    'MinNumberOfHits' : 100, # int
    'UsePreconditioning' : True, # bool
    'LogFile' : 'alignlog.txt', # str
    'SurveyConstraintTool' : PublicToolHandle('Al::AlignChisqConstraintTool'), # GaudiHandle
    'LagrangeConstraintTool' : PrivateToolHandle('Al::AlignConstraintTool'), # GaudiHandle
    'MaxDeltaChi2PDofForConvergence' : 4.0000000, # float
    'MaxModeDeltaChi2ForConvergence' : 25.000000, # float
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
    'HistoProduce' : """ Switch on/off the production of histograms [GaudiHistos<GaudiTool>] """,
    'HistoPrint' : """ Switch on/off the printout of histograms at finalization [GaudiHistos<GaudiTool>] """,
    'HistoCountersPrint' : """ Switch on/off the printout of histogram counters at finalization [GaudiHistos<GaudiTool>] """,
    'HistoCheckForNaN' : """ Switch on/off the checks for NaN and Infinity for histogram fill [GaudiHistos<GaudiTool>] """,
    'HistoSplitDir' : """ Split long directory names into short pieces (suitable for HBOOK) [GaudiHistos<GaudiTool>] """,
    'HistoOffSet' : """ OffSet for automatically assigned histogram numerical identifiers  [GaudiHistos<GaudiTool>] """,
    'HistoTopDir' : """ Top level histogram directory (take care that it ends with '/') [GaudiHistos<GaudiTool>] """,
    'HistoDir' : """ Histogram Directory [GaudiHistos<GaudiTool>] """,
    'FullDetail' : """  [GaudiHistos<GaudiTool>] """,
    'MonitorHistograms' : """  [GaudiHistos<GaudiTool>] """,
    'FormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiTool>] """,
    'ShortFormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiTool>] """,
    'HeaderFor1DHistoTable' : """ The table header for printout of 1D histograms  [GaudiHistos<GaudiTool>] """,
    'UseSequencialNumericAutoIDs' : """ Flag to allow users to switch back to the old style of creating numerical automatic IDs [GaudiHistos<GaudiTool>] """,
    'AutoStringIDPurgeMap' : """ Map of strings to search and replace when using the title as the basis of automatically generated literal IDs [GaudiHistos<GaudiTool>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Al__AlignUpdateTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'Al::AlignUpdateTool'
  pass # class Al__AlignUpdateTool

class Al__TrackResidualTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'TestPosDef' : False, # bool
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Al__TrackResidualTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'Al::TrackResidualTool'
  pass # class Al__TrackResidualTool

class Al__VertexResidualTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'TrackResidualTool' : PublicToolHandle('Al::TrackResidualTool'), # GaudiHandle
    'Extrapolator' : PublicToolHandle('TrackMasterExtrapolator'), # GaudiHandle
    'MaxHitsPerTrackForCorrelations' : 9999, # int
    'MaxVertexChi2PerDoF' : 10.000000, # float
    'MaxMassConstraintChi2' : 16.000000, # float
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Al__VertexResidualTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'Al::VertexResidualTool'
  pass # class Al__VertexResidualTool

class AlRobustAlignAlg( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'HistoProduce' : True, # bool
    'HistoPrint' : False, # bool
    'HistoCountersPrint' : True, # bool
    'HistoCheckForNaN' : True, # bool
    'HistoSplitDir' : False, # bool
    'HistoOffSet' : 0, # int
    'HistoTopDir' : '', # str
    'HistoDir' : 'DefaultName', # str
    'FullDetail' : False, # bool
    'MonitorHistograms' : True, # bool
    'FormatFor1DHistoTable' : '| %2$-45.45s | %3$=7d |%8$11.5g | %10$-11.5g|%12$11.5g |%14$11.5g |', # str
    'ShortFormatFor1DHistoTable' : ' | %1$-25.25s %2%', # str
    'HeaderFor1DHistoTable' : '|   Title                                       |    #    |     Mean   |    RMS     |  Skewness  |  Kurtosis  |', # str
    'UseSequencialNumericAutoIDs' : False, # bool
    'AutoStringIDPurgeMap' : { '/' : '=SLASH=' }, # list
    'TrackLocation' : 'Rec/Track/Best', # str
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'HistoProduce' : """ Switch on/off the production of histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HistoPrint' : """ Switch on/off the printout of histograms at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCountersPrint' : """ Switch on/off the printout of histogram counters at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCheckForNaN' : """ Switch on/off the checks for NaN and Infinity for histogram fill [GaudiHistos<GaudiAlgorithm>] """,
    'HistoSplitDir' : """ Split long directory names into short pieces (suitable for HBOOK) [GaudiHistos<GaudiAlgorithm>] """,
    'HistoOffSet' : """ OffSet for automatically assigned histogram numerical identifiers  [GaudiHistos<GaudiAlgorithm>] """,
    'HistoTopDir' : """ Top level histogram directory (take care that it ends with '/') [GaudiHistos<GaudiAlgorithm>] """,
    'HistoDir' : """ Histogram Directory [GaudiHistos<GaudiAlgorithm>] """,
    'FullDetail' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'MonitorHistograms' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'FormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'ShortFormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HeaderFor1DHistoTable' : """ The table header for printout of 1D histograms  [GaudiHistos<GaudiAlgorithm>] """,
    'UseSequencialNumericAutoIDs' : """ Flag to allow users to switch back to the old style of creating numerical automatic IDs [GaudiHistos<GaudiAlgorithm>] """,
    'AutoStringIDPurgeMap' : """ Map of strings to search and replace when using the title as the basis of automatically generated literal IDs [GaudiHistos<GaudiAlgorithm>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(AlRobustAlignAlg, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'AlRobustAlignAlg'
  pass # class AlRobustAlignAlg

class AlignAlgorithm( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'HistoProduce' : True, # bool
    'HistoPrint' : False, # bool
    'HistoCountersPrint' : True, # bool
    'HistoCheckForNaN' : True, # bool
    'HistoSplitDir' : False, # bool
    'HistoOffSet' : 0, # int
    'HistoTopDir' : '', # str
    'HistoDir' : 'DefaultName', # str
    'FullDetail' : False, # bool
    'MonitorHistograms' : True, # bool
    'FormatFor1DHistoTable' : '| %2$-45.45s | %3$=7d |%8$11.5g | %10$-11.5g|%12$11.5g |%14$11.5g |', # str
    'ShortFormatFor1DHistoTable' : ' | %1$-25.25s %2%', # str
    'HeaderFor1DHistoTable' : '|   Title                                       |    #    |     Mean   |    RMS     |  Skewness  |  Kurtosis  |', # str
    'UseSequencialNumericAutoIDs' : False, # bool
    'AutoStringIDPurgeMap' : { '/' : '=SLASH=' }, # list
    'NumberOfIterations' : 10, # int
    'TracksLocation' : 'Rec/Track/Best', # str
    'VertexLocation' : '', # str
    'DimuonLocation' : '', # str
    'ParticleLocation' : '', # str
    'ProjectorSelector' : 'TrackProjectorSelector', # str
    'UseCorrelations' : True, # bool
    'UpdateInFinalize' : False, # bool
    'OutputDataFile' : '', # str
    'InputDataFiles' : [  ], # list
    'Chi2Outlier' : 10000.000, # float
    'UpdateTool' : PublicToolHandle('Al::AlignUpdateTool'), # GaudiHandle
    'TrackResidualTool' : PublicToolHandle('Al::TrackResidualTool'), # GaudiHandle
    'VertexResidualTool' : PublicToolHandle('Al::VertexResidualTool'), # GaudiHandle
    'XmlWritersTool' : PublicToolHandle('WriteMultiAlignmentConditionsTool'), # GaudiHandle
    'MaxTracksPerVertex' : 8, # int
    'MinTracksPerVertex' : 2, # int
    'AlignSummaryDataSvc' : 'DetectorDataSvc', # str
    'AlignSummaryLocation' : 'AlignDerivativeData', # str
    'FillHistos' : False, # bool
    'ForcedInitialTime' : 0L, # long
    'OnlineMode' : False, # bool
    'PartitionName' : 'LHCbA', # str
    'ReferenceFile' : '', # str
    'RunList' : [  ], # list
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'HistoProduce' : """ Switch on/off the production of histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HistoPrint' : """ Switch on/off the printout of histograms at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCountersPrint' : """ Switch on/off the printout of histogram counters at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCheckForNaN' : """ Switch on/off the checks for NaN and Infinity for histogram fill [GaudiHistos<GaudiAlgorithm>] """,
    'HistoSplitDir' : """ Split long directory names into short pieces (suitable for HBOOK) [GaudiHistos<GaudiAlgorithm>] """,
    'HistoOffSet' : """ OffSet for automatically assigned histogram numerical identifiers  [GaudiHistos<GaudiAlgorithm>] """,
    'HistoTopDir' : """ Top level histogram directory (take care that it ends with '/') [GaudiHistos<GaudiAlgorithm>] """,
    'HistoDir' : """ Histogram Directory [GaudiHistos<GaudiAlgorithm>] """,
    'FullDetail' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'MonitorHistograms' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'FormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'ShortFormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HeaderFor1DHistoTable' : """ The table header for printout of 1D histograms  [GaudiHistos<GaudiAlgorithm>] """,
    'UseSequencialNumericAutoIDs' : """ Flag to allow users to switch back to the old style of creating numerical automatic IDs [GaudiHistos<GaudiAlgorithm>] """,
    'AutoStringIDPurgeMap' : """ Map of strings to search and replace when using the title as the basis of automatically generated literal IDs [GaudiHistos<GaudiAlgorithm>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(AlignAlgorithm, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'AlignAlgorithm'
  pass # class AlignAlgorithm

class CountingPrescaler( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'Interval' : 1, # int
    'Offset' : 0, # int
    'PrintFreq' : 0, # int
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(CountingPrescaler, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'CountingPrescaler'
  pass # class CountingPrescaler

class GAlign( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'HistoProduce' : True, # bool
    'HistoPrint' : False, # bool
    'HistoCountersPrint' : True, # bool
    'HistoCheckForNaN' : True, # bool
    'HistoSplitDir' : False, # bool
    'HistoOffSet' : 0, # int
    'HistoTopDir' : '', # str
    'HistoDir' : 'DefaultName', # str
    'FullDetail' : False, # bool
    'MonitorHistograms' : True, # bool
    'FormatFor1DHistoTable' : '| %2$-45.45s | %3$=7d |%8$11.5g | %10$-11.5g|%12$11.5g |%14$11.5g |', # str
    'ShortFormatFor1DHistoTable' : ' | %1$-25.25s %2%', # str
    'HeaderFor1DHistoTable' : '|   Title                                       |    #    |     Mean   |    RMS     |  Skewness  |  Kurtosis  |', # str
    'UseSequencialNumericAutoIDs' : False, # bool
    'AutoStringIDPurgeMap' : { '/' : '=SLASH=' }, # list
    'NTupleProduce' : True, # bool
    'NTuplePrint' : True, # bool
    'NTupleSplitDir' : False, # bool
    'NTupleOffSet' : 0, # int
    'NTupleLUN' : 'FILE1', # str
    'NTupleTopDir' : '', # str
    'NTupleDir' : 'DefaultName', # str
    'EvtColsProduce' : False, # bool
    'EvtColsPrint' : False, # bool
    'EvtColSplitDir' : False, # bool
    'EvtColOffSet' : 0, # int
    'EvtColLUN' : 'EVTCOL', # str
    'EvtColTopDir' : '', # str
    'EvtColDir' : 'DefaultName', # str
    'TAlignment_Config_Tool' : 'TAConfig', # str
    'InputContainer' : 'Rec/Track/Best', # str
    'Detectors' : [  ], # list
    'myChi2Scale' : 1000.0000, # float
    'MaxIterations' : 5, # int
    'forceIterations' : False, # bool
    'skipBigCluster' : False, # bool
    'CutonTrackSlope' : 0, # int
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'HistoProduce' : """ Switch on/off the production of histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HistoPrint' : """ Switch on/off the printout of histograms at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCountersPrint' : """ Switch on/off the printout of histogram counters at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCheckForNaN' : """ Switch on/off the checks for NaN and Infinity for histogram fill [GaudiHistos<GaudiAlgorithm>] """,
    'HistoSplitDir' : """ Split long directory names into short pieces (suitable for HBOOK) [GaudiHistos<GaudiAlgorithm>] """,
    'HistoOffSet' : """ OffSet for automatically assigned histogram numerical identifiers  [GaudiHistos<GaudiAlgorithm>] """,
    'HistoTopDir' : """ Top level histogram directory (take care that it ends with '/') [GaudiHistos<GaudiAlgorithm>] """,
    'HistoDir' : """ Histogram Directory [GaudiHistos<GaudiAlgorithm>] """,
    'FullDetail' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'MonitorHistograms' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'FormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'ShortFormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HeaderFor1DHistoTable' : """ The table header for printout of 1D histograms  [GaudiHistos<GaudiAlgorithm>] """,
    'UseSequencialNumericAutoIDs' : """ Flag to allow users to switch back to the old style of creating numerical automatic IDs [GaudiHistos<GaudiAlgorithm>] """,
    'AutoStringIDPurgeMap' : """ Map of strings to search and replace when using the title as the basis of automatically generated literal IDs [GaudiHistos<GaudiAlgorithm>] """,
    'NTupleProduce' : """ general switch to enable/disable N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTuplePrint' : """ print N-tuple statistics [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleSplitDir' : """ split long directory names into short pieces (suitable for HBOOK) [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleLUN' : """ Logical File Unit for N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleTopDir' : """ top-level directory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleDir' : """ subdirectory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsProduce' : """ general switch to enable/disable Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsPrint' : """ print statistics for Event Tag Collections  [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColSplitDir' : """ split long directory names into short pieces [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColLUN' : """ Logical File Unit for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColTopDir' : """ Top-level directory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColDir' : """ Subdirectory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(GAlign, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'GAlign'
  pass # class GAlign

class GetElementsToBeAligned( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'Elements' : [  ], # list
    'UseLocalFrame' : True, # bool
    'Upgrade' : False, # bool
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(GetElementsToBeAligned, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'GetElementsToBeAligned'
  pass # class GetElementsToBeAligned

class TStation( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'HistoProduce' : True, # bool
    'HistoPrint' : False, # bool
    'HistoCountersPrint' : True, # bool
    'HistoCheckForNaN' : True, # bool
    'HistoSplitDir' : False, # bool
    'HistoOffSet' : 0, # int
    'HistoTopDir' : '', # str
    'HistoDir' : 'DefaultName', # str
    'FullDetail' : False, # bool
    'MonitorHistograms' : True, # bool
    'FormatFor1DHistoTable' : '| %2$-45.45s | %3$=7d |%8$11.5g | %10$-11.5g|%12$11.5g |%14$11.5g |', # str
    'ShortFormatFor1DHistoTable' : ' | %1$-25.25s %2%', # str
    'HeaderFor1DHistoTable' : '|   Title                                       |    #    |     Mean   |    RMS     |  Skewness  |  Kurtosis  |', # str
    'UseSequencialNumericAutoIDs' : False, # bool
    'AutoStringIDPurgeMap' : { '/' : '=SLASH=' }, # list
    'NTupleProduce' : True, # bool
    'NTuplePrint' : True, # bool
    'NTupleSplitDir' : False, # bool
    'NTupleOffSet' : 0, # int
    'NTupleLUN' : 'FILE1', # str
    'NTupleTopDir' : '', # str
    'NTupleDir' : 'DefaultName', # str
    'EvtColsProduce' : False, # bool
    'EvtColsPrint' : False, # bool
    'EvtColSplitDir' : False, # bool
    'EvtColOffSet' : 0, # int
    'EvtColLUN' : 'EVTCOL', # str
    'EvtColTopDir' : '', # str
    'EvtColDir' : 'DefaultName', # str
    'InputContainer' : 'Rec/Track/Best', # str
    'OTTrackContainer' : 'Event/Alignment/OTTracks', # str
    'ITTrackContainer' : 'Event/Alignment/ITTracks', # str
    'CrossedTrackContainer' : 'Event/Alignment/XTracks', # str
    'MinITHitCut' : 0.0000000, # float
    'MinOTHitCut' : 0.0000000, # float
    'MinTTHitCut' : 0.0000000, # float
    'MinVeloHitCut' : 0.0000000, # float
    'Refit' : False, # bool
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'HistoProduce' : """ Switch on/off the production of histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HistoPrint' : """ Switch on/off the printout of histograms at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCountersPrint' : """ Switch on/off the printout of histogram counters at finalization [GaudiHistos<GaudiAlgorithm>] """,
    'HistoCheckForNaN' : """ Switch on/off the checks for NaN and Infinity for histogram fill [GaudiHistos<GaudiAlgorithm>] """,
    'HistoSplitDir' : """ Split long directory names into short pieces (suitable for HBOOK) [GaudiHistos<GaudiAlgorithm>] """,
    'HistoOffSet' : """ OffSet for automatically assigned histogram numerical identifiers  [GaudiHistos<GaudiAlgorithm>] """,
    'HistoTopDir' : """ Top level histogram directory (take care that it ends with '/') [GaudiHistos<GaudiAlgorithm>] """,
    'HistoDir' : """ Histogram Directory [GaudiHistos<GaudiAlgorithm>] """,
    'FullDetail' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'MonitorHistograms' : """  [GaudiHistos<GaudiAlgorithm>] """,
    'FormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'ShortFormatFor1DHistoTable' : """ Format string for printout of 1D histograms [GaudiHistos<GaudiAlgorithm>] """,
    'HeaderFor1DHistoTable' : """ The table header for printout of 1D histograms  [GaudiHistos<GaudiAlgorithm>] """,
    'UseSequencialNumericAutoIDs' : """ Flag to allow users to switch back to the old style of creating numerical automatic IDs [GaudiHistos<GaudiAlgorithm>] """,
    'AutoStringIDPurgeMap' : """ Map of strings to search and replace when using the title as the basis of automatically generated literal IDs [GaudiHistos<GaudiAlgorithm>] """,
    'NTupleProduce' : """ general switch to enable/disable N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTuplePrint' : """ print N-tuple statistics [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleSplitDir' : """ split long directory names into short pieces (suitable for HBOOK) [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleLUN' : """ Logical File Unit for N-tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleTopDir' : """ top-level directory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'NTupleDir' : """ subdirectory for N-Tuples [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsProduce' : """ general switch to enable/disable Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColsPrint' : """ print statistics for Event Tag Collections  [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColSplitDir' : """ split long directory names into short pieces [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColOffSet' : """ offset for numerical N-tuple ID [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColLUN' : """ Logical File Unit for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColTopDir' : """ Top-level directory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
    'EvtColDir' : """ Subdirectory for Event Tag Collections [GaudiTuples<GaudiHistoAlg>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TStation, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'TStation'
  pass # class TStation

class TrackFilterAlg( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'TracksInputContainer' : 'Rec/Track/Best', # str
    'TracksOutputContainer' : 'Alignment/FilteredTracks', # str
    'TrackType' : 'Long', # str
    'TrackSelector' : 'AlignSelTool', # str
    'StripUnwantedDetectorHits' : False, # bool
    'KeepDetectorHits' : 'OT', # str
    'MinNHits' : 0, # int
    'Extrapolator' : 'TrackLinearExtrapolator', # str
    'MuonFilter' : False, # bool
    'MuonPcut' : 0.0000000, # float
    'inCaloAcceptance' : False, # bool
    'noOverlap' : False, # bool
    'minHitStation' : 2, # int
    'MuonChisquareCut' : 0.0000000, # float
    'TheRegion' : -1, # int
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TrackFilterAlg, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TAlignment'
  def getType( self ):
      return 'TrackFilterAlg'
  pass # class TrackFilterAlg
