// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME TAlignmentDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/TAlignment/dict/TAlignmentDict.h"

// Header files passed via #pragma extra_include

namespace {
  void TriggerDictionaryInitialization_TAlignmentDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/TAlignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignmentInterfaces",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Tr/TrackKernel",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/XercesC/3.1.3/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignKernel",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/cppgsl/b07383ea/x86_64-centos7-gcc7-opt",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/vdt/0.3.9/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignEvent",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Python/2.7.13/x86_64-centos7-gcc7-opt/include/python2.7",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Alignment/TAlignment/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "TAlignmentDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "TAlignmentDict dictionary payload"
#ifdef __MINGW32__
  #undef __MINGW32__
#endif
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations TAlignment_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "TAlignment"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v6r9"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef TALIGNMENT_DICT_H 
#define TALIGNMENT_DICT_H 1
#include <vector>
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Vector3DTypes.h"

#endif // TALIGNMENT_DICT

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TAlignmentDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TAlignmentDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TAlignmentDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TAlignmentDict() {
  TriggerDictionaryInitialization_TAlignmentDict_Impl();
}
