// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME AlignKernelDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignKernel/dict/AlignKernelDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *AlMat_Dictionary();
   static void AlMat_TClassManip(TClass*);
   static void *new_AlMat(void *p = 0);
   static void *newArray_AlMat(Long_t size, void *p);
   static void delete_AlMat(void *p);
   static void deleteArray_AlMat(void *p);
   static void destruct_AlMat(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AlMat*)
   {
      ::AlMat *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AlMat));
      static ::ROOT::TGenericClassInfo 
         instance("AlMat", "AlignKernel/AlMat.h", 11,
                  typeid(::AlMat), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlMat_Dictionary, isa_proxy, 4,
                  sizeof(::AlMat) );
      instance.SetNew(&new_AlMat);
      instance.SetNewArray(&newArray_AlMat);
      instance.SetDelete(&delete_AlMat);
      instance.SetDeleteArray(&deleteArray_AlMat);
      instance.SetDestructor(&destruct_AlMat);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AlMat*)
   {
      return GenerateInitInstanceLocal((::AlMat*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::AlMat*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlMat_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AlMat*)0x0)->GetClass();
      AlMat_TClassManip(theClass);
   return theClass;
   }

   static void AlMat_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlSymMat_Dictionary();
   static void AlSymMat_TClassManip(TClass*);
   static void *new_AlSymMat(void *p = 0);
   static void *newArray_AlSymMat(Long_t size, void *p);
   static void delete_AlSymMat(void *p);
   static void deleteArray_AlSymMat(void *p);
   static void destruct_AlSymMat(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AlSymMat*)
   {
      ::AlSymMat *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AlSymMat));
      static ::ROOT::TGenericClassInfo 
         instance("AlSymMat", "AlignKernel/AlSymMat.h", 14,
                  typeid(::AlSymMat), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlSymMat_Dictionary, isa_proxy, 4,
                  sizeof(::AlSymMat) );
      instance.SetNew(&new_AlSymMat);
      instance.SetNewArray(&newArray_AlSymMat);
      instance.SetDelete(&delete_AlSymMat);
      instance.SetDeleteArray(&deleteArray_AlSymMat);
      instance.SetDestructor(&destruct_AlSymMat);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AlSymMat*)
   {
      return GenerateInitInstanceLocal((::AlSymMat*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::AlSymMat*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlSymMat_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AlSymMat*)0x0)->GetClass();
      AlSymMat_TClassManip(theClass);
   return theClass;
   }

   static void AlSymMat_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlVec_Dictionary();
   static void AlVec_TClassManip(TClass*);
   static void *new_AlVec(void *p = 0);
   static void *newArray_AlVec(Long_t size, void *p);
   static void delete_AlVec(void *p);
   static void deleteArray_AlVec(void *p);
   static void destruct_AlVec(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AlVec*)
   {
      ::AlVec *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AlVec));
      static ::ROOT::TGenericClassInfo 
         instance("AlVec", "AlignKernel/AlVec.h", 12,
                  typeid(::AlVec), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlVec_Dictionary, isa_proxy, 4,
                  sizeof(::AlVec) );
      instance.SetNew(&new_AlVec);
      instance.SetNewArray(&newArray_AlVec);
      instance.SetDelete(&delete_AlVec);
      instance.SetDeleteArray(&deleteArray_AlVec);
      instance.SetDestructor(&destruct_AlVec);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AlVec*)
   {
      return GenerateInitInstanceLocal((::AlVec*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::AlVec*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlVec_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AlVec*)0x0)->GetClass();
      AlVec_TClassManip(theClass);
   return theClass;
   }

   static void AlVec_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR_Dictionary();
   static void ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR_TClassManip(TClass*);
   static void *new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p = 0);
   static void *newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(Long_t size, void *p);
   static void delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p);
   static void deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p);
   static void destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ROOT::Math::MatRepStd<double,6,5>*)
   {
      ::ROOT::Math::MatRepStd<double,6,5> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ROOT::Math::MatRepStd<double,6,5>));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::Math::MatRepStd<double,6,5>", "Math/MatrixRepresentationsStatic.h", 53,
                  typeid(::ROOT::Math::MatRepStd<double,6,5>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR_Dictionary, isa_proxy, 4,
                  sizeof(::ROOT::Math::MatRepStd<double,6,5>) );
      instance.SetNew(&new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR);
      instance.SetNewArray(&newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR);
      instance.SetDelete(&delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR);
      instance.SetDestructor(&destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ROOT::Math::MatRepStd<double,6,5>*)
   {
      return GenerateInitInstanceLocal((::ROOT::Math::MatRepStd<double,6,5>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ROOT::Math::MatRepStd<double,6,5>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ROOT::Math::MatRepStd<double,6,5>*)0x0)->GetClass();
      ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR_Dictionary();
   static void ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR_TClassManip(TClass*);
   static void *new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p = 0);
   static void *newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(Long_t size, void *p);
   static void delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p);
   static void deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p);
   static void destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ROOT::Math::MatRepStd<double,6,3>*)
   {
      ::ROOT::Math::MatRepStd<double,6,3> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ROOT::Math::MatRepStd<double,6,3>));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::Math::MatRepStd<double,6,3>", "Math/MatrixRepresentationsStatic.h", 53,
                  typeid(::ROOT::Math::MatRepStd<double,6,3>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR_Dictionary, isa_proxy, 4,
                  sizeof(::ROOT::Math::MatRepStd<double,6,3>) );
      instance.SetNew(&new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR);
      instance.SetNewArray(&newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR);
      instance.SetDelete(&delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR);
      instance.SetDestructor(&destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ROOT::Math::MatRepStd<double,6,3>*)
   {
      return GenerateInitInstanceLocal((::ROOT::Math::MatRepStd<double,6,3>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ROOT::Math::MatRepStd<double,6,3>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ROOT::Math::MatRepStd<double,6,3>*)0x0)->GetClass();
      ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR_Dictionary();
   static void ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR_TClassManip(TClass*);
   static void *new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p = 0);
   static void *newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(Long_t size, void *p);
   static void delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p);
   static void deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p);
   static void destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ROOT::Math::MatRepStd<double,6,1>*)
   {
      ::ROOT::Math::MatRepStd<double,6,1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ROOT::Math::MatRepStd<double,6,1>));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::Math::MatRepStd<double,6,1>", "Math/MatrixRepresentationsStatic.h", 53,
                  typeid(::ROOT::Math::MatRepStd<double,6,1>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR_Dictionary, isa_proxy, 4,
                  sizeof(::ROOT::Math::MatRepStd<double,6,1>) );
      instance.SetNew(&new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR);
      instance.SetNewArray(&newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR);
      instance.SetDelete(&delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR);
      instance.SetDestructor(&destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ROOT::Math::MatRepStd<double,6,1>*)
   {
      return GenerateInitInstanceLocal((::ROOT::Math::MatRepStd<double,6,1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ROOT::Math::MatRepStd<double,6,1>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ROOT::Math::MatRepStd<double,6,1>*)0x0)->GetClass();
      ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR_Dictionary();
   static void ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR_TClassManip(TClass*);
   static void *new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p = 0);
   static void *newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(Long_t size, void *p);
   static void delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p);
   static void deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p);
   static void destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >*)
   {
      ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >", "Math/SMatrix.h", 124,
                  typeid(::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >) );
      instance.SetNew(&new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR);
      instance.SetNewArray(&newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR);
      instance.SetDelete(&delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR);
      instance.SetDestructor(&destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >*)
   {
      return GenerateInitInstanceLocal((::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >*)0x0)->GetClass();
      ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR_Dictionary();
   static void ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR_TClassManip(TClass*);
   static void *new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p = 0);
   static void *newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(Long_t size, void *p);
   static void delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p);
   static void deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p);
   static void destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >*)
   {
      ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >", "Math/SMatrix.h", 124,
                  typeid(::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >) );
      instance.SetNew(&new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR);
      instance.SetNewArray(&newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR);
      instance.SetDelete(&delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR);
      instance.SetDestructor(&destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >*)
   {
      return GenerateInitInstanceLocal((::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >*)0x0)->GetClass();
      ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR_Dictionary();
   static void ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR_TClassManip(TClass*);
   static void *new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p = 0);
   static void *newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(Long_t size, void *p);
   static void delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p);
   static void deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p);
   static void destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >*)
   {
      ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >", "Math/SMatrix.h", 124,
                  typeid(::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >) );
      instance.SetNew(&new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR);
      instance.SetNewArray(&newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR);
      instance.SetDelete(&delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR);
      instance.SetDestructor(&destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >*)
   {
      return GenerateInitInstanceLocal((::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >*)0x0)->GetClass();
      ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlcLcLOffDiagonalData_Dictionary();
   static void AlcLcLOffDiagonalData_TClassManip(TClass*);
   static void *new_AlcLcLOffDiagonalData(void *p = 0);
   static void *newArray_AlcLcLOffDiagonalData(Long_t size, void *p);
   static void delete_AlcLcLOffDiagonalData(void *p);
   static void deleteArray_AlcLcLOffDiagonalData(void *p);
   static void destruct_AlcLcLOffDiagonalData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Al::OffDiagonalData*)
   {
      ::Al::OffDiagonalData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Al::OffDiagonalData));
      static ::ROOT::TGenericClassInfo 
         instance("Al::OffDiagonalData", "AlignKernel/AlEquations.h", 18,
                  typeid(::Al::OffDiagonalData), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlcLcLOffDiagonalData_Dictionary, isa_proxy, 4,
                  sizeof(::Al::OffDiagonalData) );
      instance.SetNew(&new_AlcLcLOffDiagonalData);
      instance.SetNewArray(&newArray_AlcLcLOffDiagonalData);
      instance.SetDelete(&delete_AlcLcLOffDiagonalData);
      instance.SetDeleteArray(&deleteArray_AlcLcLOffDiagonalData);
      instance.SetDestructor(&destruct_AlcLcLOffDiagonalData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Al::OffDiagonalData*)
   {
      return GenerateInitInstanceLocal((::Al::OffDiagonalData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Al::OffDiagonalData*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlcLcLOffDiagonalData_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Al::OffDiagonalData*)0x0)->GetClass();
      AlcLcLOffDiagonalData_TClassManip(theClass);
   return theClass;
   }

   static void AlcLcLOffDiagonalData_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlcLcLElementData_Dictionary();
   static void AlcLcLElementData_TClassManip(TClass*);
   static void *new_AlcLcLElementData(void *p = 0);
   static void *newArray_AlcLcLElementData(Long_t size, void *p);
   static void delete_AlcLcLElementData(void *p);
   static void deleteArray_AlcLcLElementData(void *p);
   static void destruct_AlcLcLElementData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Al::ElementData*)
   {
      ::Al::ElementData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Al::ElementData));
      static ::ROOT::TGenericClassInfo 
         instance("Al::ElementData", "AlignKernel/AlEquations.h", 45,
                  typeid(::Al::ElementData), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlcLcLElementData_Dictionary, isa_proxy, 4,
                  sizeof(::Al::ElementData) );
      instance.SetNew(&new_AlcLcLElementData);
      instance.SetNewArray(&newArray_AlcLcLElementData);
      instance.SetDelete(&delete_AlcLcLElementData);
      instance.SetDeleteArray(&deleteArray_AlcLcLElementData);
      instance.SetDestructor(&destruct_AlcLcLElementData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Al::ElementData*)
   {
      return GenerateInitInstanceLocal((::Al::ElementData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Al::ElementData*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlcLcLElementData_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Al::ElementData*)0x0)->GetClass();
      AlcLcLElementData_TClassManip(theClass);
   return theClass;
   }

   static void AlcLcLElementData_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlcLcLEquations_Dictionary();
   static void AlcLcLEquations_TClassManip(TClass*);
   static void *new_AlcLcLEquations(void *p = 0);
   static void *newArray_AlcLcLEquations(Long_t size, void *p);
   static void delete_AlcLcLEquations(void *p);
   static void deleteArray_AlcLcLEquations(void *p);
   static void destruct_AlcLcLEquations(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Al::Equations*)
   {
      ::Al::Equations *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Al::Equations));
      static ::ROOT::TGenericClassInfo 
         instance("Al::Equations", "AlignKernel/AlEquations.h", 102,
                  typeid(::Al::Equations), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlcLcLEquations_Dictionary, isa_proxy, 4,
                  sizeof(::Al::Equations) );
      instance.SetNew(&new_AlcLcLEquations);
      instance.SetNewArray(&newArray_AlcLcLEquations);
      instance.SetDelete(&delete_AlcLcLEquations);
      instance.SetDeleteArray(&deleteArray_AlcLcLEquations);
      instance.SetDestructor(&destruct_AlcLcLEquations);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Al::Equations*)
   {
      return GenerateInitInstanceLocal((::Al::Equations*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Al::Equations*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlcLcLEquations_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Al::Equations*)0x0)->GetClass();
      AlcLcLEquations_TClassManip(theClass);
   return theClass;
   }

   static void AlcLcLEquations_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlcLcLOTMonoLayerAlignModuleData_Dictionary();
   static void AlcLcLOTMonoLayerAlignModuleData_TClassManip(TClass*);
   static void *new_AlcLcLOTMonoLayerAlignModuleData(void *p = 0);
   static void *newArray_AlcLcLOTMonoLayerAlignModuleData(Long_t size, void *p);
   static void delete_AlcLcLOTMonoLayerAlignModuleData(void *p);
   static void deleteArray_AlcLcLOTMonoLayerAlignModuleData(void *p);
   static void destruct_AlcLcLOTMonoLayerAlignModuleData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Al::OTMonoLayerAlignModuleData*)
   {
      ::Al::OTMonoLayerAlignModuleData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Al::OTMonoLayerAlignModuleData));
      static ::ROOT::TGenericClassInfo 
         instance("Al::OTMonoLayerAlignModuleData", "AlignKernel/OTMonoLayerAlignData.h", 11,
                  typeid(::Al::OTMonoLayerAlignModuleData), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlcLcLOTMonoLayerAlignModuleData_Dictionary, isa_proxy, 4,
                  sizeof(::Al::OTMonoLayerAlignModuleData) );
      instance.SetNew(&new_AlcLcLOTMonoLayerAlignModuleData);
      instance.SetNewArray(&newArray_AlcLcLOTMonoLayerAlignModuleData);
      instance.SetDelete(&delete_AlcLcLOTMonoLayerAlignModuleData);
      instance.SetDeleteArray(&deleteArray_AlcLcLOTMonoLayerAlignModuleData);
      instance.SetDestructor(&destruct_AlcLcLOTMonoLayerAlignModuleData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Al::OTMonoLayerAlignModuleData*)
   {
      return GenerateInitInstanceLocal((::Al::OTMonoLayerAlignModuleData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Al::OTMonoLayerAlignModuleData*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlcLcLOTMonoLayerAlignModuleData_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Al::OTMonoLayerAlignModuleData*)0x0)->GetClass();
      AlcLcLOTMonoLayerAlignModuleData_TClassManip(theClass);
   return theClass;
   }

   static void AlcLcLOTMonoLayerAlignModuleData_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AlcLcLOTMonoLayerAlignData_Dictionary();
   static void AlcLcLOTMonoLayerAlignData_TClassManip(TClass*);
   static void *new_AlcLcLOTMonoLayerAlignData(void *p = 0);
   static void *newArray_AlcLcLOTMonoLayerAlignData(Long_t size, void *p);
   static void delete_AlcLcLOTMonoLayerAlignData(void *p);
   static void deleteArray_AlcLcLOTMonoLayerAlignData(void *p);
   static void destruct_AlcLcLOTMonoLayerAlignData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Al::OTMonoLayerAlignData*)
   {
      ::Al::OTMonoLayerAlignData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Al::OTMonoLayerAlignData));
      static ::ROOT::TGenericClassInfo 
         instance("Al::OTMonoLayerAlignData", "AlignKernel/OTMonoLayerAlignData.h", 57,
                  typeid(::Al::OTMonoLayerAlignData), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AlcLcLOTMonoLayerAlignData_Dictionary, isa_proxy, 4,
                  sizeof(::Al::OTMonoLayerAlignData) );
      instance.SetNew(&new_AlcLcLOTMonoLayerAlignData);
      instance.SetNewArray(&newArray_AlcLcLOTMonoLayerAlignData);
      instance.SetDelete(&delete_AlcLcLOTMonoLayerAlignData);
      instance.SetDeleteArray(&deleteArray_AlcLcLOTMonoLayerAlignData);
      instance.SetDestructor(&destruct_AlcLcLOTMonoLayerAlignData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Al::OTMonoLayerAlignData*)
   {
      return GenerateInitInstanceLocal((::Al::OTMonoLayerAlignData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Al::OTMonoLayerAlignData*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AlcLcLOTMonoLayerAlignData_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Al::OTMonoLayerAlignData*)0x0)->GetClass();
      AlcLcLOTMonoLayerAlignData_TClassManip(theClass);
   return theClass;
   }

   static void AlcLcLOTMonoLayerAlignData_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlMat(void *p) {
      return  p ? new(p) ::AlMat : new ::AlMat;
   }
   static void *newArray_AlMat(Long_t nElements, void *p) {
      return p ? new(p) ::AlMat[nElements] : new ::AlMat[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlMat(void *p) {
      delete ((::AlMat*)p);
   }
   static void deleteArray_AlMat(void *p) {
      delete [] ((::AlMat*)p);
   }
   static void destruct_AlMat(void *p) {
      typedef ::AlMat current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AlMat

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlSymMat(void *p) {
      return  p ? new(p) ::AlSymMat : new ::AlSymMat;
   }
   static void *newArray_AlSymMat(Long_t nElements, void *p) {
      return p ? new(p) ::AlSymMat[nElements] : new ::AlSymMat[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlSymMat(void *p) {
      delete ((::AlSymMat*)p);
   }
   static void deleteArray_AlSymMat(void *p) {
      delete [] ((::AlSymMat*)p);
   }
   static void destruct_AlSymMat(void *p) {
      typedef ::AlSymMat current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AlSymMat

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlVec(void *p) {
      return  p ? new(p) ::AlVec : new ::AlVec;
   }
   static void *newArray_AlVec(Long_t nElements, void *p) {
      return p ? new(p) ::AlVec[nElements] : new ::AlVec[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlVec(void *p) {
      delete ((::AlVec*)p);
   }
   static void deleteArray_AlVec(void *p) {
      delete [] ((::AlVec*)p);
   }
   static void destruct_AlVec(void *p) {
      typedef ::AlVec current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AlVec

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::MatRepStd<double,6,5> : new ::ROOT::Math::MatRepStd<double,6,5>;
   }
   static void *newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::MatRepStd<double,6,5>[nElements] : new ::ROOT::Math::MatRepStd<double,6,5>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p) {
      delete ((::ROOT::Math::MatRepStd<double,6,5>*)p);
   }
   static void deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p) {
      delete [] ((::ROOT::Math::MatRepStd<double,6,5>*)p);
   }
   static void destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gR(void *p) {
      typedef ::ROOT::Math::MatRepStd<double,6,5> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ROOT::Math::MatRepStd<double,6,5>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::MatRepStd<double,6,3> : new ::ROOT::Math::MatRepStd<double,6,3>;
   }
   static void *newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::MatRepStd<double,6,3>[nElements] : new ::ROOT::Math::MatRepStd<double,6,3>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p) {
      delete ((::ROOT::Math::MatRepStd<double,6,3>*)p);
   }
   static void deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p) {
      delete [] ((::ROOT::Math::MatRepStd<double,6,3>*)p);
   }
   static void destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gR(void *p) {
      typedef ::ROOT::Math::MatRepStd<double,6,3> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ROOT::Math::MatRepStd<double,6,3>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::MatRepStd<double,6,1> : new ::ROOT::Math::MatRepStd<double,6,1>;
   }
   static void *newArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::MatRepStd<double,6,1>[nElements] : new ::ROOT::Math::MatRepStd<double,6,1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p) {
      delete ((::ROOT::Math::MatRepStd<double,6,1>*)p);
   }
   static void deleteArray_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p) {
      delete [] ((::ROOT::Math::MatRepStd<double,6,1>*)p);
   }
   static void destruct_ROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gR(void *p) {
      typedef ::ROOT::Math::MatRepStd<double,6,1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ROOT::Math::MatRepStd<double,6,1>

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> > : new ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >;
   }
   static void *newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >[nElements] : new ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p) {
      delete ((::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >*)p);
   }
   static void deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p) {
      delete [] ((::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >*)p);
   }
   static void destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO5cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO5gRsPgR(void *p) {
      typedef ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> > : new ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >;
   }
   static void *newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >[nElements] : new ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p) {
      delete ((::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >*)p);
   }
   static void deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p) {
      delete [] ((::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >*)p);
   }
   static void destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO3cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO3gRsPgR(void *p) {
      typedef ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> > : new ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >;
   }
   static void *newArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >[nElements] : new ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p) {
      delete ((::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >*)p);
   }
   static void deleteArray_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p) {
      delete [] ((::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >*)p);
   }
   static void destruct_ROOTcLcLMathcLcLSMatrixlEdoublecO6cO1cOROOTcLcLMathcLcLMatRepStdlEdoublecO6cO1gRsPgR(void *p) {
      typedef ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlcLcLOffDiagonalData(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::OffDiagonalData : new ::Al::OffDiagonalData;
   }
   static void *newArray_AlcLcLOffDiagonalData(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::OffDiagonalData[nElements] : new ::Al::OffDiagonalData[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlcLcLOffDiagonalData(void *p) {
      delete ((::Al::OffDiagonalData*)p);
   }
   static void deleteArray_AlcLcLOffDiagonalData(void *p) {
      delete [] ((::Al::OffDiagonalData*)p);
   }
   static void destruct_AlcLcLOffDiagonalData(void *p) {
      typedef ::Al::OffDiagonalData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Al::OffDiagonalData

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlcLcLElementData(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::ElementData : new ::Al::ElementData;
   }
   static void *newArray_AlcLcLElementData(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::ElementData[nElements] : new ::Al::ElementData[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlcLcLElementData(void *p) {
      delete ((::Al::ElementData*)p);
   }
   static void deleteArray_AlcLcLElementData(void *p) {
      delete [] ((::Al::ElementData*)p);
   }
   static void destruct_AlcLcLElementData(void *p) {
      typedef ::Al::ElementData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Al::ElementData

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlcLcLEquations(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::Equations : new ::Al::Equations;
   }
   static void *newArray_AlcLcLEquations(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::Equations[nElements] : new ::Al::Equations[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlcLcLEquations(void *p) {
      delete ((::Al::Equations*)p);
   }
   static void deleteArray_AlcLcLEquations(void *p) {
      delete [] ((::Al::Equations*)p);
   }
   static void destruct_AlcLcLEquations(void *p) {
      typedef ::Al::Equations current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Al::Equations

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlcLcLOTMonoLayerAlignModuleData(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::OTMonoLayerAlignModuleData : new ::Al::OTMonoLayerAlignModuleData;
   }
   static void *newArray_AlcLcLOTMonoLayerAlignModuleData(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::OTMonoLayerAlignModuleData[nElements] : new ::Al::OTMonoLayerAlignModuleData[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlcLcLOTMonoLayerAlignModuleData(void *p) {
      delete ((::Al::OTMonoLayerAlignModuleData*)p);
   }
   static void deleteArray_AlcLcLOTMonoLayerAlignModuleData(void *p) {
      delete [] ((::Al::OTMonoLayerAlignModuleData*)p);
   }
   static void destruct_AlcLcLOTMonoLayerAlignModuleData(void *p) {
      typedef ::Al::OTMonoLayerAlignModuleData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Al::OTMonoLayerAlignModuleData

namespace ROOT {
   // Wrappers around operator new
   static void *new_AlcLcLOTMonoLayerAlignData(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::OTMonoLayerAlignData : new ::Al::OTMonoLayerAlignData;
   }
   static void *newArray_AlcLcLOTMonoLayerAlignData(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Al::OTMonoLayerAlignData[nElements] : new ::Al::OTMonoLayerAlignData[nElements];
   }
   // Wrapper around operator delete
   static void delete_AlcLcLOTMonoLayerAlignData(void *p) {
      delete ((::Al::OTMonoLayerAlignData*)p);
   }
   static void deleteArray_AlcLcLOTMonoLayerAlignData(void *p) {
      delete [] ((::Al::OTMonoLayerAlignData*)p);
   }
   static void destruct_AlcLcLOTMonoLayerAlignData(void *p) {
      typedef ::Al::OTMonoLayerAlignData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Al::OTMonoLayerAlignData

namespace ROOT {
   static TClass *maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR_Dictionary();
   static void maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR_TClassManip(TClass*);
   static void *new_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p = 0);
   static void *newArray_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(Long_t size, void *p);
   static void delete_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p);
   static void deleteArray_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p);
   static void destruct_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<unsigned long,Al::OffDiagonalData>*)
   {
      map<unsigned long,Al::OffDiagonalData> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<unsigned long,Al::OffDiagonalData>));
      static ::ROOT::TGenericClassInfo 
         instance("map<unsigned long,Al::OffDiagonalData>", -2, "map", 99,
                  typeid(map<unsigned long,Al::OffDiagonalData>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR_Dictionary, isa_proxy, 4,
                  sizeof(map<unsigned long,Al::OffDiagonalData>) );
      instance.SetNew(&new_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR);
      instance.SetNewArray(&newArray_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR);
      instance.SetDelete(&delete_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR);
      instance.SetDeleteArray(&deleteArray_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR);
      instance.SetDestructor(&destruct_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<unsigned long,Al::OffDiagonalData> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<unsigned long,Al::OffDiagonalData>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<unsigned long,Al::OffDiagonalData>*)0x0)->GetClass();
      maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR_TClassManip(theClass);
   return theClass;
   }

   static void maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<unsigned long,Al::OffDiagonalData> : new map<unsigned long,Al::OffDiagonalData>;
   }
   static void *newArray_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<unsigned long,Al::OffDiagonalData>[nElements] : new map<unsigned long,Al::OffDiagonalData>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p) {
      delete ((map<unsigned long,Al::OffDiagonalData>*)p);
   }
   static void deleteArray_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p) {
      delete [] ((map<unsigned long,Al::OffDiagonalData>*)p);
   }
   static void destruct_maplEunsignedsPlongcOAlcLcLOffDiagonalDatagR(void *p) {
      typedef map<unsigned long,Al::OffDiagonalData> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<unsigned long,Al::OffDiagonalData>

namespace {
  void TriggerDictionaryInitialization_AlignKernelDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignKernel",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Alignment/AlignKernel/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "AlignKernelDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$AlignKernel/AlMat.h")))  AlMat;
class __attribute__((annotate("$clingAutoload$AlignKernel/AlSymMat.h")))  AlSymMat;
class __attribute__((annotate("$clingAutoload$AlignKernel/AlVec.h")))  AlVec;
namespace Al{class __attribute__((annotate("$clingAutoload$AlignKernel/AlEquations.h")))  OffDiagonalData;}
namespace std{template <typename _Tp = void> struct __attribute__((annotate("$clingAutoload$bits/stl_function.h")))  __attribute__((annotate("$clingAutoload$string")))  less;
}
namespace std{template <typename _T1, typename _T2> struct __attribute__((annotate("$clingAutoload$bits/stl_pair.h")))  __attribute__((annotate("$clingAutoload$string")))  pair;
}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace Al{class __attribute__((annotate("$clingAutoload$AlignKernel/AlEquations.h")))  ElementData;}
namespace Al{class __attribute__((annotate("$clingAutoload$AlignKernel/AlEquations.h")))  Equations;}
namespace Al{class __attribute__((annotate("$clingAutoload$AlignKernel/OTMonoLayerAlignData.h")))  OTMonoLayerAlignModuleData;}
namespace Al{class __attribute__((annotate("$clingAutoload$AlignKernel/OTMonoLayerAlignData.h")))  OTMonoLayerAlignData;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "AlignKernelDict dictionary payload"
#ifdef __MINGW32__
  #undef __MINGW32__
#endif
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations AlignKernel_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "AlignKernel"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v3r10"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// $Id: AlignKernelDict.h,v 1.1 2009-07-06 12:40:02 wouter Exp $
#ifndef DICT_SOLVKERNELDICT_H 
#define DICT_SOLVKERNELDICT_H 1

// this line is necessary to add ROOT::SMatrices to the dictionary
#define G__DICTIONARY

#include "AlignKernel/AlMat.h"
#include "AlignKernel/AlSymMat.h"
#include "AlignKernel/AlVec.h"
#include "AlignKernel/AlEquations.h"
#include "AlignKernel/OTMonoLayerAlignData.h"

#endif // DICT_SOLVKERNELDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Al::ElementData", payloadCode, "@",
"Al::Equations", payloadCode, "@",
"Al::OTMonoLayerAlignData", payloadCode, "@",
"Al::OTMonoLayerAlignModuleData", payloadCode, "@",
"Al::OffDiagonalData", payloadCode, "@",
"AlMat", payloadCode, "@",
"AlSymMat", payloadCode, "@",
"AlVec", payloadCode, "@",
"ROOT::Math::MatRepStd<double,6,1>", payloadCode, "@",
"ROOT::Math::MatRepStd<double,6,3>", payloadCode, "@",
"ROOT::Math::MatRepStd<double,6,5>", payloadCode, "@",
"ROOT::Math::SMatrix<double,6,1,ROOT::Math::MatRepStd<double,6,1> >", payloadCode, "@",
"ROOT::Math::SMatrix<double,6,3,ROOT::Math::MatRepStd<double,6,3> >", payloadCode, "@",
"ROOT::Math::SMatrix<double,6,5,ROOT::Math::MatRepStd<double,6,5> >", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("AlignKernelDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_AlignKernelDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_AlignKernelDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_AlignKernelDict() {
  TriggerDictionaryInitialization_AlignKernelDict_Impl();
}
