# CMake generated Testfile for 
# Source directory: /public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/AlignmentSys
# Build directory: /public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/AlignmentSys
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(AlignmentSys.configurables "/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake/xenv" "--xml" "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/config/Alignment-build.xenv" "python" "-m" "GaudiTesting.Run" "--skip-return-code" "77" "--report" "ctest" "--common-tmpdir" "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/AlignmentSys/tests_tmp" "--workdir" "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/AlignmentSys/tests/qmtest" "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/AlignmentSys/tests/qmtest/alignmentsys.qms/configurables.qmt")
set_tests_properties(AlignmentSys.configurables PROPERTIES  LABELS "AlignmentSys;QMTest" SKIP_RETURN_CODE "77" WORKING_DIRECTORY "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/AlignmentSys/tests/qmtest")
