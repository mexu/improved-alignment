# CMake generated Testfile for 
# Source directory: /public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment
# Build directory: /public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Alignment/AlignKernel")
subdirs("Alignment/AlignEvent")
subdirs("Alignment/AlignmentInterfaces")
subdirs("Alignment/AlignSolvTools")
subdirs("Tr/TrackKernel")
subdirs("Alignment/AlignTrTools")
subdirs("Alignment/AlignmentDBVisualisationTool")
subdirs("Alignment/AlignmentMonitoring")
subdirs("Alignment/AlignmentTools")
subdirs("Alignment/TAlignment")
subdirs("Alignment/VeloAlignment")
subdirs("Tr/TrackFitter")
subdirs("Alignment/Escher")
subdirs("Alignment/MisAligner")
subdirs("Alignment/TVerticalAlignment")
subdirs("Calibration/OTCalibration")
subdirs("AlignmentSys")
subdirs("Calibration/Pi0Calibration")
