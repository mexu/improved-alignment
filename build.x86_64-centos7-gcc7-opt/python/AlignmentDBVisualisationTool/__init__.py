
import os, sys
__path__ = [d for d in [os.path.join(d, 'AlignmentDBVisualisationTool') for d in sys.path if d]
            if (d.startswith('/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt') or
                d.startswith('/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment')) and
               (os.path.exists(d) or 'python.zip' in d)]
if os.path.exists('/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignmentDBVisualisationTool/python/AlignmentDBVisualisationTool/__init__.py'):
    execfile('/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Alignment/AlignmentDBVisualisationTool/python/AlignmentDBVisualisationTool/__init__.py')
