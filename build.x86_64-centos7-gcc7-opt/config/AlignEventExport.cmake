# Generated by GaudiProjectConfig.cmake (with CMake 3.11.0)

if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" LESS 2.5)
   message(FATAL_ERROR "CMake >= 2.6.0 required")
endif()
cmake_policy(PUSH)
cmake_policy(VERSION 2.6)

# Compute the installation prefix relative to this file.
get_filename_component(_IMPORT_PREFIX "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)

add_library(AlignEvent SHARED IMPORTED)
set_target_properties(AlignEvent PROPERTIES
  REQUIRED_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/Alignment/AlignKernel;${CMAKE_SOURCE_DIR}/;${LCG_releases_base}/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include;${LCG_releases_base}/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include;${LCG_releases_base}/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp;${LCG_releases_base}/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include;${LCG_releases_base}/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/include;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include;${LCG_releases_base}/LCG_93/vdt/0.3.9/x86_64-centos7-gcc7-opt/include;${LCG_releases_base}/LCG_93/cppgsl/b07383ea/x86_64-centos7-gcc7-opt"
  REQUIRED_LIBRARIES "${LCG_releases_base}/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/lib/libgsl.so;${LCG_releases_base}/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/lib/libgslcblas.so;AlignKernel;${LCG_releases_base}/LCG_93/lapack/3.5.0/x86_64-centos7-gcc7-opt/lib/libLAPACK.a;${LCG_releases_base}/LCG_93/blas/20110419/x86_64-centos7-gcc7-opt/lib/libBLAS.a;gfortran;GaudiAlgLib;GaudiUtilsLib;GaudiKernel;dl;${LCG_releases_base}/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/lib/libboost_filesystem.so;${LCG_releases_base}/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/lib/libboost_thread.so;${LCG_releases_base}/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/lib/libboost_system.so;${LCG_releases_base}/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/lib/libboost_regex.so;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libCore.so;${LCG_releases_base}/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/lib/libtbb.so;GaudiPluginService;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libRIO.so;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libHist.so;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libXMLIO.so;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libThread.so;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libMatrix.so;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libMathCore.so;GaudiGSLLib;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-Cast-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-Evaluator-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-Exceptions-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-GenericFunctions-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-Geometry-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-Random-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-RandomObjects-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-RefCount-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-Vector-2.4.0.1.so;${LCG_releases_base}/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/lib/libCLHEP-Matrix-2.4.0.1.so;LHCbKernel;${LCG_releases_base}/LCG_93/vdt/0.3.9/x86_64-centos7-gcc7-opt/lib/libvdt.so;GaudiObjDescLib;LHCbMathLib;${LCG_releases_base}/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/lib/libGenVector.so;VectorClassLib;LHCbFastMath"
  IMPORTED_SONAME "libAlignEvent.so"
  IMPORTED_LOCATION "${_IMPORT_PREFIX}/lib/libAlignEvent.so"
  )

set(Alignment/AlignEvent_DEPENDENCIES Alignment/AlignKernel;GaudiObjDesc;Kernel/LHCbKernel)

set(Alignment/AlignEvent_VERSION v1r3)

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
cmake_policy(POP)
