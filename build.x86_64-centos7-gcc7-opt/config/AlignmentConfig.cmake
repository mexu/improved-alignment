# File automatically generated: DO NOT EDIT.
set(Alignment_heptools_version 93)
set(Alignment_heptools_system x86_64-centos7-gcc7)

set(Alignment_PLATFORM x86_64-centos7-gcc7-opt)

set(Alignment_VERSION master)
set(Alignment_VERSION_MAJOR 999)
set(Alignment_VERSION_MINOR 999)
set(Alignment_VERSION_PATCH 0)

set(Alignment_USES Phys;v30r0)
set(Alignment_DATA AppConfig;FieldMap;ParamFiles)

list(INSERT CMAKE_MODULE_PATH 0 ${Alignment_DIR}/cmake)
include(AlignmentPlatformConfig)
