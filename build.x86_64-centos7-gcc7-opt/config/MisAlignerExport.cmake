# Generated by GaudiProjectConfig.cmake (with CMake 3.11.0)

if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" LESS 2.5)
   message(FATAL_ERROR "CMake >= 2.6.0 required")
endif()
cmake_policy(PUSH)
cmake_policy(VERSION 2.6)

# Compute the installation prefix relative to this file.
get_filename_component(_IMPORT_PREFIX "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)

add_library(MisAlignAlg MODULE IMPORTED)

set(Alignment/MisAligner_DEPENDENCIES Det/MuonDet;Det/OTDet;Det/STDet;Det/VeloDet;GaudiAlg;GaudiCoreSvc)

set(Alignment/MisAligner_VERSION v2r4)

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
cmake_policy(POP)
