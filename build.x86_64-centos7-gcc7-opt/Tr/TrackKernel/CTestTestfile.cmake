# CMake generated Testfile for 
# Source directory: /public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Tr/TrackKernel
# Build directory: /public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Tr/TrackKernel
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(TrackKernel.test_traj "/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake/xenv" "--xml" "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/config/Alignment-build.xenv" "test_traj.exe")
set_tests_properties(TrackKernel.test_traj PROPERTIES  LABELS "TrackKernel" WORKING_DIRECTORY ".")
