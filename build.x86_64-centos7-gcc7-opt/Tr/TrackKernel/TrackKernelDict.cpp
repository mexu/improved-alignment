// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME TrackKernelDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Tr/TrackKernel/src/TrackKernelDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *LHCbcLcLStateTraj_Dictionary();
   static void LHCbcLcLStateTraj_TClassManip(TClass*);
   static void delete_LHCbcLcLStateTraj(void *p);
   static void deleteArray_LHCbcLcLStateTraj(void *p);
   static void destruct_LHCbcLcLStateTraj(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LHCb::StateTraj*)
   {
      ::LHCb::StateTraj *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LHCb::StateTraj));
      static ::ROOT::TGenericClassInfo 
         instance("LHCb::StateTraj", "TrackKernel/StateTraj.h", 36,
                  typeid(::LHCb::StateTraj), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &LHCbcLcLStateTraj_Dictionary, isa_proxy, 4,
                  sizeof(::LHCb::StateTraj) );
      instance.SetDelete(&delete_LHCbcLcLStateTraj);
      instance.SetDeleteArray(&deleteArray_LHCbcLcLStateTraj);
      instance.SetDestructor(&destruct_LHCbcLcLStateTraj);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LHCb::StateTraj*)
   {
      return GenerateInitInstanceLocal((::LHCb::StateTraj*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LHCb::StateTraj*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LHCbcLcLStateTraj_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LHCb::StateTraj*)0x0)->GetClass();
      LHCbcLcLStateTraj_TClassManip(theClass);
   return theClass;
   }

   static void LHCbcLcLStateTraj_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *LHCbcLcLZTrajectorylEdoublegR_Dictionary();
   static void LHCbcLcLZTrajectorylEdoublegR_TClassManip(TClass*);
   static void delete_LHCbcLcLZTrajectorylEdoublegR(void *p);
   static void deleteArray_LHCbcLcLZTrajectorylEdoublegR(void *p);
   static void destruct_LHCbcLcLZTrajectorylEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LHCb::ZTrajectory<double>*)
   {
      ::LHCb::ZTrajectory<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LHCb::ZTrajectory<double>));
      static ::ROOT::TGenericClassInfo 
         instance("LHCb::ZTrajectory<double>", "TrackKernel/ZTrajectory.h", 38,
                  typeid(::LHCb::ZTrajectory<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &LHCbcLcLZTrajectorylEdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(::LHCb::ZTrajectory<double>) );
      instance.SetDelete(&delete_LHCbcLcLZTrajectorylEdoublegR);
      instance.SetDeleteArray(&deleteArray_LHCbcLcLZTrajectorylEdoublegR);
      instance.SetDestructor(&destruct_LHCbcLcLZTrajectorylEdoublegR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LHCb::ZTrajectory<double>*)
   {
      return GenerateInitInstanceLocal((::LHCb::ZTrajectory<double>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LHCb::ZTrajectory<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LHCbcLcLZTrajectorylEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LHCb::ZTrajectory<double>*)0x0)->GetClass();
      LHCbcLcLZTrajectorylEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void LHCbcLcLZTrajectorylEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *LHCbcLcLTrackTraj_Dictionary();
   static void LHCbcLcLTrackTraj_TClassManip(TClass*);
   static void delete_LHCbcLcLTrackTraj(void *p);
   static void deleteArray_LHCbcLcLTrackTraj(void *p);
   static void destruct_LHCbcLcLTrackTraj(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LHCb::TrackTraj*)
   {
      ::LHCb::TrackTraj *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LHCb::TrackTraj));
      static ::ROOT::TGenericClassInfo 
         instance("LHCb::TrackTraj", "TrackKernel/TrackTraj.h", 25,
                  typeid(::LHCb::TrackTraj), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &LHCbcLcLTrackTraj_Dictionary, isa_proxy, 4,
                  sizeof(::LHCb::TrackTraj) );
      instance.SetDelete(&delete_LHCbcLcLTrackTraj);
      instance.SetDeleteArray(&deleteArray_LHCbcLcLTrackTraj);
      instance.SetDestructor(&destruct_LHCbcLcLTrackTraj);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LHCb::TrackTraj*)
   {
      return GenerateInitInstanceLocal((::LHCb::TrackTraj*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LHCb::TrackTraj*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LHCbcLcLTrackTraj_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LHCb::TrackTraj*)0x0)->GetClass();
      LHCbcLcLTrackTraj_TClassManip(theClass);
   return theClass;
   }

   static void LHCbcLcLTrackTraj_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *LHCbcLcLTrackStateVertex_Dictionary();
   static void LHCbcLcLTrackStateVertex_TClassManip(TClass*);
   static void *new_LHCbcLcLTrackStateVertex(void *p = 0);
   static void *newArray_LHCbcLcLTrackStateVertex(Long_t size, void *p);
   static void delete_LHCbcLcLTrackStateVertex(void *p);
   static void deleteArray_LHCbcLcLTrackStateVertex(void *p);
   static void destruct_LHCbcLcLTrackStateVertex(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LHCb::TrackStateVertex*)
   {
      ::LHCb::TrackStateVertex *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LHCb::TrackStateVertex));
      static ::ROOT::TGenericClassInfo 
         instance("LHCb::TrackStateVertex", "TrackKernel/TrackStateVertex.h", 27,
                  typeid(::LHCb::TrackStateVertex), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &LHCbcLcLTrackStateVertex_Dictionary, isa_proxy, 4,
                  sizeof(::LHCb::TrackStateVertex) );
      instance.SetNew(&new_LHCbcLcLTrackStateVertex);
      instance.SetNewArray(&newArray_LHCbcLcLTrackStateVertex);
      instance.SetDelete(&delete_LHCbcLcLTrackStateVertex);
      instance.SetDeleteArray(&deleteArray_LHCbcLcLTrackStateVertex);
      instance.SetDestructor(&destruct_LHCbcLcLTrackStateVertex);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LHCb::TrackStateVertex*)
   {
      return GenerateInitInstanceLocal((::LHCb::TrackStateVertex*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LHCb::TrackStateVertex*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LHCbcLcLTrackStateVertex_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LHCb::TrackStateVertex*)0x0)->GetClass();
      LHCbcLcLTrackStateVertex_TClassManip(theClass);
   return theClass;
   }

   static void LHCbcLcLTrackStateVertex_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_LHCbcLcLStateTraj(void *p) {
      delete ((::LHCb::StateTraj*)p);
   }
   static void deleteArray_LHCbcLcLStateTraj(void *p) {
      delete [] ((::LHCb::StateTraj*)p);
   }
   static void destruct_LHCbcLcLStateTraj(void *p) {
      typedef ::LHCb::StateTraj current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LHCb::StateTraj

namespace ROOT {
   // Wrapper around operator delete
   static void delete_LHCbcLcLZTrajectorylEdoublegR(void *p) {
      delete ((::LHCb::ZTrajectory<double>*)p);
   }
   static void deleteArray_LHCbcLcLZTrajectorylEdoublegR(void *p) {
      delete [] ((::LHCb::ZTrajectory<double>*)p);
   }
   static void destruct_LHCbcLcLZTrajectorylEdoublegR(void *p) {
      typedef ::LHCb::ZTrajectory<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LHCb::ZTrajectory<double>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_LHCbcLcLTrackTraj(void *p) {
      delete ((::LHCb::TrackTraj*)p);
   }
   static void deleteArray_LHCbcLcLTrackTraj(void *p) {
      delete [] ((::LHCb::TrackTraj*)p);
   }
   static void destruct_LHCbcLcLTrackTraj(void *p) {
      typedef ::LHCb::TrackTraj current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LHCb::TrackTraj

namespace ROOT {
   // Wrappers around operator new
   static void *new_LHCbcLcLTrackStateVertex(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::LHCb::TrackStateVertex : new ::LHCb::TrackStateVertex;
   }
   static void *newArray_LHCbcLcLTrackStateVertex(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::LHCb::TrackStateVertex[nElements] : new ::LHCb::TrackStateVertex[nElements];
   }
   // Wrapper around operator delete
   static void delete_LHCbcLcLTrackStateVertex(void *p) {
      delete ((::LHCb::TrackStateVertex*)p);
   }
   static void deleteArray_LHCbcLcLTrackStateVertex(void *p) {
      delete [] ((::LHCb::TrackStateVertex*)p);
   }
   static void destruct_LHCbcLcLTrackStateVertex(void *p) {
      typedef ::LHCb::TrackStateVertex current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LHCb::TrackStateVertex

namespace {
  void TriggerDictionaryInitialization_TrackKernelDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Tr/TrackKernel",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/cppgsl/b07383ea/x86_64-centos7-gcc7-opt",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/vdt/0.3.9/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Tr/TrackKernel/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "TrackKernelDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace LHCb{class __attribute__((annotate("$clingAutoload$TrackKernel/StateTraj.h")))  StateTraj;}
namespace LHCb{template <typename FTYPE = double> class __attribute__((annotate("$clingAutoload$TrackKernel/ZTrajectory.h")))  __attribute__((annotate("$clingAutoload$TrackKernel/StateZTraj.h")))  ZTrajectory;
}
namespace LHCb{class __attribute__((annotate("$clingAutoload$TrackKernel/TrackTraj.h")))  TrackTraj;}
namespace LHCb{class __attribute__((annotate("$clingAutoload$TrackKernel/TrackStateVertex.h")))  TrackStateVertex;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "TrackKernelDict dictionary payload"
#ifdef __MINGW32__
  #undef __MINGW32__
#endif
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations TrackKernel_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "TrackKernel"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v3r1"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// $Id: TrackKernelDict.h,v 1.1 2009-07-08 13:33:45 wouter Exp $
#ifndef TRACKFITEVENT_LCGDICT_H
#define TRACKFITEVENT_LCGDICT_ 1

// Additional classes to be added to automatically generated lcgdict

// begin include files
#include "TrackKernel/StateTraj.h"
#include "TrackKernel/StateZTraj.h"
#include "TrackKernel/TrackTraj.h"
#include "TrackKernel/ZTrajectory.h"
#include "TrackKernel/TrackStateVertex.h"

// end include files

namespace {
  struct _Instantiations {
    // begin instantiations
    // end instantiations
  };
}

#endif // TRACKFITEVENT_LCGDICT_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"", payloadCode, "@",
"LHCb::StateTraj", payloadCode, "@",
"LHCb::StateZTraj::(anonymous)", payloadCode, "@",
"LHCb::TrackStateVertex", payloadCode, "@",
"LHCb::TrackTraj", payloadCode, "@",
"LHCb::ZTrajectory<double>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TrackKernelDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TrackKernelDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TrackKernelDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TrackKernelDict() {
  TriggerDictionaryInitialization_TrackKernelDict_Impl();
}
