#Mon Jun 15 10:24:44 2020"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.GaudiHandles import *
from GaudiKernel.DataObjectHandleBase import DataObjectHandleBase
from GaudiKernel.Proxy.Configurable import *

class TrackEventFitter( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'Cardinality' : 0, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'FilterCircularDependencies' : True, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'TracksInContainer' : DataObjectHandleBase("Rec/Track/Best"), # DataObjectHandleBase
    'TracksOutContainer' : DataObjectHandleBase("Rec/Track/Best"), # DataObjectHandleBase
    'SkipFailedFitAtInput' : True, # bool
    'MaxChi2DoF' : 999999.00, # float
    'BatchSize' : 50, # int
    'Fitter' : PrivateToolHandle('TrackMasterFitter/Fitter'), # GaudiHandle
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'FilterCircularDependencies' : """ filter out circular data dependencies [Algorithm] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'TracksInContainer' : """  [Gaudi::Functional::details::DataHandleMixin<std::tuple<KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> > >,std::tuple<KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> > >,Gaudi::Functional::Traits::use_<> >] """,
    'TracksOutContainer' : """  [Gaudi::Functional::details::DataHandleMixin<std::tuple<KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> > >,std::tuple<KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> > >,Gaudi::Functional::Traits::use_<> >] """,
    'SkipFailedFitAtInput' : """  [TrackEventFitter] """,
    'MaxChi2DoF' : """ Max chi2 per track when output is a new container [TrackEventFitter] """,
    'BatchSize' : """ Size of batches [TrackEventFitter] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TrackEventFitter, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrackFitter'
  def getType( self ):
      return 'TrackEventFitter'
  pass # class TrackEventFitter

class TrackKalmanFilter( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'ForceBiDirectionalFit' : True, # bool
    'ForceSmooth' : False, # bool
    'DoF' : 5, # int
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TrackKalmanFilter, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrackFitter'
  def getType( self ):
      return 'TrackKalmanFilter'
  pass # class TrackKalmanFilter

class TrackMasterFitter( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'FitUpstream' : True, # bool
    'AddDefaultReferenceNodes' : True, # bool
    'StateAtBeamLine' : True, # bool
    'NumberFitIterations' : 10, # int
    'Chi2Outliers' : 9.0000000, # float
    'MaxNumberOutliers' : 2, # int
    'UseSeedStateErrors' : False, # bool
    'UseClassicalSmoother' : False, # bool
    'FillExtraInfo' : True, # bool
    'ErrorX' : 20.000000, # float
    'ErrorY' : 20.000000, # float
    'ErrorTx' : 0.10000000, # float
    'ErrorTy' : 0.10000000, # float
    'ErrorQoP' : [ 0.0000000 , 0.010000000 ], # list
    'MakeNodes' : False, # bool
    'MakeMeasurements' : False, # bool
    'UpdateTransport' : True, # bool
    'MaxUpdateTransports' : 10, # int
    'UpdateMaterial' : False, # bool
    'UpdateReferenceInOutlierIterations' : True, # bool
    'MinMomentumELossCorr' : 10.000000, # float
    'ApplyMaterialCorrections' : True, # bool
    'ApplyEnergyLossCorr' : True, # bool
    'MaxDeltaChiSqConverged' : 0.010000000, # float
    'TransverseMomentumForScattering' : 400.00000, # float
    'MomentumForScattering' : -1.0000000, # float
    'MinMomentumForScattering' : 100.00000, # float
    'MaxMomentumForScattering' : 500000.00, # float
    'MinNumVeloRHitsForOutlierRemoval' : 3, # int
    'MinNumVeloPhiHitsForOutlierRemoval' : 3, # int
    'MinNumTTHitsForOutlierRemoval' : 3, # int
    'MinNumTHitsForOutlierRemoval' : 6, # int
    'MinNumMuonHitsForOutlierRemoval' : 4, # int
    'Projector' : PrivateToolHandle('TrackProjectorSelector'), # GaudiHandle
    'MeasProvider' : PrivateToolHandle('MeasurementProvider'), # GaudiHandle
    'NodeFitter' : PrivateToolHandle('TrackKalmanFilter'), # GaudiHandle
    'Extrapolator' : PrivateToolHandle('TrackMasterExtrapolator'), # GaudiHandle
    'VeloExtrapolator' : PrivateToolHandle('TrackLinearExtrapolator'), # GaudiHandle
    'MaterialLocator' : PrivateToolHandle('DetailedMaterialLocator'), # GaudiHandle
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
    'FitUpstream' : """ switch between upstream/downstream fit [TrackMasterFitter] """,
    'AddDefaultReferenceNodes' : """ add default reference nodes [TrackMasterFitter] """,
    'StateAtBeamLine' : """ add state closest to the beam-line [TrackMasterFitter] """,
    'NumberFitIterations' : """ number of fit iterations to perform [TrackMasterFitter] """,
    'Chi2Outliers' : """ chi2 of outliers to be removed [TrackMasterFitter] """,
    'MaxNumberOutliers' : """ max number of outliers to be removed [TrackMasterFitter] """,
    'UseSeedStateErrors' : """ use errors of the seed state [TrackMasterFitter] """,
    'UseClassicalSmoother' : """ Use classical smoother [TrackMasterFitter] """,
    'FillExtraInfo' : """ Fill the extra info [TrackMasterFitter] """,
    'ErrorX' : """ Seed error on x [TrackMasterFitter] """,
    'ErrorY' : """ Seed error on y [TrackMasterFitter] """,
    'ErrorTx' : """ Seed error on slope x [TrackMasterFitter] """,
    'ErrorTy' : """ Seed error on slope y [TrackMasterFitter] """,
    'ErrorQoP' : """ Seed error on QoP [TrackMasterFitter] """,
    'MakeNodes' : """  [TrackMasterFitter] """,
    'MakeMeasurements' : """  [TrackMasterFitter] """,
    'UpdateTransport' : """ Update the transport matrices between iterations [TrackMasterFitter] """,
    'MaxUpdateTransports' : """ Update transport only n-times during iterations [TrackMasterFitter] """,
    'UpdateMaterial' : """ Update material corrections between iterations [TrackMasterFitter] """,
    'UpdateReferenceInOutlierIterations' : """ Update projection in iterations in which outliers are removed [TrackMasterFitter] """,
    'MinMomentumELossCorr' : """ Minimum momentum used in correction for energy loss [TrackMasterFitter] """,
    'ApplyMaterialCorrections' : """ Apply material corrections [TrackMasterFitter] """,
    'ApplyEnergyLossCorr' : """ Apply energy loss corrections [TrackMasterFitter] """,
    'MaxDeltaChiSqConverged' : """ Maximum change in chisquare for converged fit [TrackMasterFitter] """,
    'TransverseMomentumForScattering' : """ transverse momentum used for scattering if track has no good momentum estimate [TrackMasterFitter] """,
    'MomentumForScattering' : """ momentum used for scattering in e.g. magnet off data [TrackMasterFitter] """,
    'MinMomentumForScattering' : """ Minimum momentum used for scattering [TrackMasterFitter] """,
    'MaxMomentumForScattering' : """ Maximum momentum used for scattering [TrackMasterFitter] """,
    'MinNumVeloRHitsForOutlierRemoval' : """ Minimum number of VeloR hits [TrackMasterFitter] """,
    'MinNumVeloPhiHitsForOutlierRemoval' : """ Minimum number of VeloPhi hits [TrackMasterFitter] """,
    'MinNumTTHitsForOutlierRemoval' : """ Minimum number of TT hits [TrackMasterFitter] """,
    'MinNumTHitsForOutlierRemoval' : """ Minimum number of T hits [TrackMasterFitter] """,
    'MinNumMuonHitsForOutlierRemoval' : """ Minimum number of Muon hits [TrackMasterFitter] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TrackMasterFitter, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrackFitter'
  def getType( self ):
      return 'TrackMasterFitter'
  pass # class TrackMasterFitter

class TrackVectorFitter( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'AddDefaultReferenceNodes' : True, # bool
    'ApplyEnergyLossCorr' : True, # bool
    'ApplyMaterialCorrections' : True, # bool
    'Chi2Outliers' : 9.0000000, # float
    'FillExtraInfo' : True, # bool
    'MakeMeasurements' : False, # bool
    'MakeNodes' : False, # bool
    'MagneticFieldSvc' : 'MagneticFieldSvc', # str
    'MaxDeltaChiSqConverged' : 0.010000000, # float
    'MaxMomentumForScattering' : 500000.00, # float
    'MemManagerStorageIncrements' : 4096, # int
    'MinMomentumELossCorr' : 10.000000, # float
    'MinMomentumForScattering' : 100.00000, # float
    'MinNumMuonHitsForOutlierRemoval' : 4, # int
    'MinNumTHitsForOutlierRemoval' : 6, # int
    'MinNumTTHitsForOutlierRemoval' : 3, # int
    'MinNumVeloPhiHitsForOutlierRemoval' : 3, # int
    'MinNumVeloRHitsForOutlierRemoval' : 3, # int
    'NumberFitIterations' : 10, # int
    'MaxNumberOutliers' : 2, # int
    'MomentumForScattering' : -1.0000000, # float
    'TransverseMomentumForScattering' : 400.00000, # float
    'StateAtBeamLine' : True, # bool
    'UpdateMaterial' : False, # bool
    'FitUpstream' : True, # bool
    'UseSeedStateErrors' : False, # bool
    'Extrapolator' : PrivateToolHandle('TrackMasterExtrapolator'), # GaudiHandle
    'VeloExtrapolator' : PrivateToolHandle('TrackLinearExtrapolator'), # GaudiHandle
    'MeasProvider' : PrivateToolHandle('MeasurementProvider'), # GaudiHandle
    'MaterialLocator' : PrivateToolHandle('DetailedMaterialLocator'), # GaudiHandle
    'Projector' : PrivateToolHandle('TrackProjectorSelector'), # GaudiHandle
    'ProjectorGeneric' : PrivateToolHandle('TrackProjector'), # GaudiHandle
  }
  _propertyDocDct = { 
    'ExtraInputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'ExtraOutputs' : """  [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
    'AddDefaultReferenceNodes' : """  [TrackVectorFitter] """,
    'ApplyEnergyLossCorr' : """  [TrackVectorFitter] """,
    'ApplyMaterialCorrections' : """  [TrackVectorFitter] """,
    'Chi2Outliers' : """  [TrackVectorFitter] """,
    'FillExtraInfo' : """  [TrackVectorFitter] """,
    'MakeMeasurements' : """  [TrackVectorFitter] """,
    'MakeNodes' : """  [TrackVectorFitter] """,
    'MagneticFieldSvc' : """  [TrackVectorFitter] """,
    'MaxDeltaChiSqConverged' : """  [TrackVectorFitter] """,
    'MaxMomentumForScattering' : """  [TrackVectorFitter] """,
    'MemManagerStorageIncrements' : """  [TrackVectorFitter] """,
    'MinMomentumELossCorr' : """  [TrackVectorFitter] """,
    'MinMomentumForScattering' : """  [TrackVectorFitter] """,
    'MinNumMuonHitsForOutlierRemoval' : """  [TrackVectorFitter] """,
    'MinNumTHitsForOutlierRemoval' : """  [TrackVectorFitter] """,
    'MinNumTTHitsForOutlierRemoval' : """  [TrackVectorFitter] """,
    'MinNumVeloPhiHitsForOutlierRemoval' : """  [TrackVectorFitter] """,
    'MinNumVeloRHitsForOutlierRemoval' : """  [TrackVectorFitter] """,
    'NumberFitIterations' : """  [TrackVectorFitter] """,
    'MaxNumberOutliers' : """  [TrackVectorFitter] """,
    'MomentumForScattering' : """  [TrackVectorFitter] """,
    'TransverseMomentumForScattering' : """  [TrackVectorFitter] """,
    'StateAtBeamLine' : """  [TrackVectorFitter] """,
    'UpdateMaterial' : """  [TrackVectorFitter] """,
    'FitUpstream' : """  [TrackVectorFitter] """,
    'UseSeedStateErrors' : """  [TrackVectorFitter] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TrackVectorFitter, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrackFitter'
  def getType( self ):
      return 'TrackVectorFitter'
  pass # class TrackVectorFitter
