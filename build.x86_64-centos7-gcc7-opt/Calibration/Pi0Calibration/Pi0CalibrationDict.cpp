// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME Pi0CalibrationDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Calibration/Pi0Calibration/dict/Pi0CalibrationDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile_Dictionary();
   static void CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile_TClassManip(TClass*);
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile(void *p);
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile(void *p);
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Calibration::Pi0Calibration::Pi0CalibrationFile*)
   {
      ::Calibration::Pi0Calibration::Pi0CalibrationFile *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Calibration::Pi0Calibration::Pi0CalibrationFile));
      static ::ROOT::TGenericClassInfo 
         instance("Calibration::Pi0Calibration::Pi0CalibrationFile", "Pi0Calibration/Pi0CalibrationFile.h", 45,
                  typeid(::Calibration::Pi0Calibration::Pi0CalibrationFile), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile_Dictionary, isa_proxy, 4,
                  sizeof(::Calibration::Pi0Calibration::Pi0CalibrationFile) );
      instance.SetDelete(&delete_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile);
      instance.SetDeleteArray(&deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile);
      instance.SetDestructor(&destruct_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Calibration::Pi0Calibration::Pi0CalibrationFile*)
   {
      return GenerateInitInstanceLocal((::Calibration::Pi0Calibration::Pi0CalibrationFile*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0CalibrationFile*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0CalibrationFile*)0x0)->GetClass();
      CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile_TClassManip(theClass);
   return theClass;
   }

   static void CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap_Dictionary();
   static void CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap_TClassManip(TClass*);
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap(void *p);
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap(void *p);
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Calibration::Pi0Calibration::Pi0LambdaMap*)
   {
      ::Calibration::Pi0Calibration::Pi0LambdaMap *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Calibration::Pi0Calibration::Pi0LambdaMap));
      static ::ROOT::TGenericClassInfo 
         instance("Calibration::Pi0Calibration::Pi0LambdaMap", "Pi0Calibration/Pi0LambdaMap.h", 30,
                  typeid(::Calibration::Pi0Calibration::Pi0LambdaMap), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap_Dictionary, isa_proxy, 4,
                  sizeof(::Calibration::Pi0Calibration::Pi0LambdaMap) );
      instance.SetDelete(&delete_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap);
      instance.SetDeleteArray(&deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap);
      instance.SetDestructor(&destruct_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Calibration::Pi0Calibration::Pi0LambdaMap*)
   {
      return GenerateInitInstanceLocal((::Calibration::Pi0Calibration::Pi0LambdaMap*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0LambdaMap*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0LambdaMap*)0x0)->GetClass();
      CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap_TClassManip(theClass);
   return theClass;
   }

   static void CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0MassFiller_Dictionary();
   static void CalibrationcLcLPi0CalibrationcLcLPi0MassFiller_TClassManip(TClass*);
   static void *new_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p = 0);
   static void *newArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(Long_t size, void *p);
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p);
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p);
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Calibration::Pi0Calibration::Pi0MassFiller*)
   {
      ::Calibration::Pi0Calibration::Pi0MassFiller *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Calibration::Pi0Calibration::Pi0MassFiller));
      static ::ROOT::TGenericClassInfo 
         instance("Calibration::Pi0Calibration::Pi0MassFiller", "Pi0Calibration/Pi0MassFiller.h", 50,
                  typeid(::Calibration::Pi0Calibration::Pi0MassFiller), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CalibrationcLcLPi0CalibrationcLcLPi0MassFiller_Dictionary, isa_proxy, 4,
                  sizeof(::Calibration::Pi0Calibration::Pi0MassFiller) );
      instance.SetNew(&new_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller);
      instance.SetNewArray(&newArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller);
      instance.SetDelete(&delete_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller);
      instance.SetDeleteArray(&deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller);
      instance.SetDestructor(&destruct_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Calibration::Pi0Calibration::Pi0MassFiller*)
   {
      return GenerateInitInstanceLocal((::Calibration::Pi0Calibration::Pi0MassFiller*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0MassFiller*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0MassFiller_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0MassFiller*)0x0)->GetClass();
      CalibrationcLcLPi0CalibrationcLcLPi0MassFiller_TClassManip(theClass);
   return theClass;
   }

   static void CalibrationcLcLPi0CalibrationcLcLPi0MassFiller_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0MassFitter_Dictionary();
   static void CalibrationcLcLPi0CalibrationcLcLPi0MassFitter_TClassManip(TClass*);
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter(void *p);
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter(void *p);
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Calibration::Pi0Calibration::Pi0MassFitter*)
   {
      ::Calibration::Pi0Calibration::Pi0MassFitter *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Calibration::Pi0Calibration::Pi0MassFitter));
      static ::ROOT::TGenericClassInfo 
         instance("Calibration::Pi0Calibration::Pi0MassFitter", "Pi0Calibration/Pi0MassFitter.h", 35,
                  typeid(::Calibration::Pi0Calibration::Pi0MassFitter), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CalibrationcLcLPi0CalibrationcLcLPi0MassFitter_Dictionary, isa_proxy, 4,
                  sizeof(::Calibration::Pi0Calibration::Pi0MassFitter) );
      instance.SetDelete(&delete_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter);
      instance.SetDeleteArray(&deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter);
      instance.SetDestructor(&destruct_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Calibration::Pi0Calibration::Pi0MassFitter*)
   {
      return GenerateInitInstanceLocal((::Calibration::Pi0Calibration::Pi0MassFitter*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0MassFitter*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CalibrationcLcLPi0CalibrationcLcLPi0MassFitter_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Calibration::Pi0Calibration::Pi0MassFitter*)0x0)->GetClass();
      CalibrationcLcLPi0CalibrationcLcLPi0MassFitter_TClassManip(theClass);
   return theClass;
   }

   static void CalibrationcLcLPi0CalibrationcLcLPi0MassFitter_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile(void *p) {
      delete ((::Calibration::Pi0Calibration::Pi0CalibrationFile*)p);
   }
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile(void *p) {
      delete [] ((::Calibration::Pi0Calibration::Pi0CalibrationFile*)p);
   }
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0CalibrationFile(void *p) {
      typedef ::Calibration::Pi0Calibration::Pi0CalibrationFile current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Calibration::Pi0Calibration::Pi0CalibrationFile

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap(void *p) {
      delete ((::Calibration::Pi0Calibration::Pi0LambdaMap*)p);
   }
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap(void *p) {
      delete [] ((::Calibration::Pi0Calibration::Pi0LambdaMap*)p);
   }
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0LambdaMap(void *p) {
      typedef ::Calibration::Pi0Calibration::Pi0LambdaMap current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Calibration::Pi0Calibration::Pi0LambdaMap

namespace ROOT {
   // Wrappers around operator new
   static void *new_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Calibration::Pi0Calibration::Pi0MassFiller : new ::Calibration::Pi0Calibration::Pi0MassFiller;
   }
   static void *newArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Calibration::Pi0Calibration::Pi0MassFiller[nElements] : new ::Calibration::Pi0Calibration::Pi0MassFiller[nElements];
   }
   // Wrapper around operator delete
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p) {
      delete ((::Calibration::Pi0Calibration::Pi0MassFiller*)p);
   }
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p) {
      delete [] ((::Calibration::Pi0Calibration::Pi0MassFiller*)p);
   }
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0MassFiller(void *p) {
      typedef ::Calibration::Pi0Calibration::Pi0MassFiller current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Calibration::Pi0Calibration::Pi0MassFiller

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter(void *p) {
      delete ((::Calibration::Pi0Calibration::Pi0MassFitter*)p);
   }
   static void deleteArray_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter(void *p) {
      delete [] ((::Calibration::Pi0Calibration::Pi0MassFitter*)p);
   }
   static void destruct_CalibrationcLcLPi0CalibrationcLcLPi0MassFitter(void *p) {
      typedef ::Calibration::Pi0Calibration::Pi0MassFitter current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Calibration::Pi0Calibration::Pi0MassFitter

namespace {
  void TriggerDictionaryInitialization_Pi0CalibrationDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Calibration/Pi0Calibration",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Python/2.7.13/x86_64-centos7-gcc7-opt/include/python2.7",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/cppgsl/b07383ea/x86_64-centos7-gcc7-opt",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/vdt/0.3.9/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/clhep/2.4.0.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/GSL/2.1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/rangev3/0.3.0/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/AIDA/3.2.1/x86_64-centos7-gcc7-opt/src/cpp",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/tbb/2018_U1/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/ROOT/6.12.06/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_93/Boost/1.66.0/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/Tr/TrackKernel",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r0/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v30r2/InstallArea/x86_64-centos7-gcc7-opt/include",
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/ROOT/6.12.06-51921/x86_64-centos7-gcc7-opt/include",
"/public1/lhcb/mlxu/angular_coefficiency_refit/Detector_Align/Alignment/build.x86_64-centos7-gcc7-opt/Calibration/Pi0Calibration/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "Pi0CalibrationDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace Calibration{namespace Pi0Calibration{class __attribute__((annotate("$clingAutoload$Pi0Calibration/Pi0CalibrationFile.h")))  Pi0CalibrationFile;}}
namespace Calibration{namespace Pi0Calibration{class __attribute__((annotate("$clingAutoload$Pi0Calibration/Pi0LambdaMap.h")))  Pi0LambdaMap;}}
namespace Calibration{namespace Pi0Calibration{class __attribute__((annotate("$clingAutoload$Pi0Calibration/Pi0MassFiller.h")))  Pi0MassFiller;}}
namespace Calibration{namespace Pi0Calibration{class __attribute__((annotate("$clingAutoload$Pi0Calibration/Pi0MassFitter.h")))  Pi0MassFitter;}}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "Pi0CalibrationDict dictionary payload"
#ifdef __MINGW32__
  #undef __MINGW32__
#endif
#ifdef _Instantiations
  #undef _Instantiations
#endif

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef _Instantiations
  #define _Instantiations Pi0Calibration_Instantiations
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef unix
  #define unix 1
#endif
#ifndef f2cFortran
  #define f2cFortran 1
#endif
#ifndef linux
  #define linux 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef PACKAGE_NAME
  #define PACKAGE_NAME "Pi0Calibration"
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "v1r0"
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef DICT_PI0CALIBRATIONDICT_H 
#define DICT_PI0CALIBRATIONDICT_H 1

// Include files
#include "Pi0Calibration/MMapVector.h"  
#include "Pi0Calibration/Pi0CalibrationFile.h"  
#include "Pi0Calibration/Pi0LambdaMap.h"  
#include "Pi0Calibration/Pi0MassFiller.h"  
#include "Pi0Calibration/Pi0MassFitter.h"

#endif // DICT_PI0CALIBRATIONDICT_H


#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Calibration::Pi0Calibration::Pi0CalibrationFile", payloadCode, "@",
"Calibration::Pi0Calibration::Pi0LambdaMap", payloadCode, "@",
"Calibration::Pi0Calibration::Pi0MassFiller", payloadCode, "@",
"Calibration::Pi0Calibration::Pi0MassFitter", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("Pi0CalibrationDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_Pi0CalibrationDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_Pi0CalibrationDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_Pi0CalibrationDict() {
  TriggerDictionaryInitialization_Pi0CalibrationDict_Impl();
}
