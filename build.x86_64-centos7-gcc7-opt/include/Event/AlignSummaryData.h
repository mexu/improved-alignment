
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

#ifndef AlignEvent_AlignSummaryData_H
#define AlignEvent_AlignSummaryData_H 1

// Include files
#include "AlignKernel/AlEquations.h"
#include "GaudiKernel/DataObject.h"
#include <ostream>

// Forward declarations

namespace LHCb
{

  // Forward declarations
  
  // Class ID definition
  static const CLID CLID_AlignSummaryData = 10213;
  
  // Namespace for locations in TDS
  namespace AlignSummaryDataLocation {
    static const std::string Default = "FST/Alignment/SummaryData";
  }
  

  /** @class AlignSummaryData AlignSummaryData.h
   *
   * 
   *
   * @author 
   * created Mon Jun 15 10:13:46 2020
   *
   */

  class AlignSummaryData: public DataObject
  {
  public:

    /// constructor from AlEquations object
    AlignSummaryData(const Al::Equations& eq) : m_equations(eq) {}
  
    /// constructor
    AlignSummaryData(const size_t& size,
                     const Gaudi::Time& time) : m_equations(size,
                                               time) {}
  
    /// Default Constructor
    AlignSummaryData() : m_equations() {}
  
    /// Default Destructor
    virtual ~AlignSummaryData() {}
  
    // Retrieve pointer to class definition structure
    const CLID& clID() const override;
    static const CLID& classID();
  
    /// 
    void add(const AlignSummaryData& rhs);
  
    /// needs also function with capital because GaudyPthon.Parallel uses that
    void Add(const AlignSummaryData& rhs);
  
    /// printOut method to Gaudi message stream
    virtual std::ostream& fillStream(std::ostream& os) const override;
  
    /// Retrieve const  
    const Al::Equations& equations() const;
  
    /// Retrieve  
    Al::Equations& equations();
  
    /// Update  
    void setEquations(const Al::Equations& value);
  
  protected:

  private:

    Al::Equations m_equations; ///< 
  
  }; // class AlignSummaryData

  inline std::ostream& operator<< (std::ostream& str, const AlignSummaryData& obj)
  {
    return obj.fillStream(str);
  }
  
} // namespace LHCb;

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::AlignSummaryData::clID() const
{
  return LHCb::AlignSummaryData::classID();
}

inline const CLID& LHCb::AlignSummaryData::classID()
{
  return CLID_AlignSummaryData;
}

inline const Al::Equations& LHCb::AlignSummaryData::equations() const 
{
  return m_equations;
}

inline Al::Equations& LHCb::AlignSummaryData::equations() 
{
  return m_equations;
}

inline void LHCb::AlignSummaryData::setEquations(const Al::Equations& value) 
{
  m_equations = value;
}

inline void LHCb::AlignSummaryData::Add(const AlignSummaryData& rhs) 
{

     add(rhs) ;
      
}



#endif ///AlignEvent_AlignSummaryData_H
